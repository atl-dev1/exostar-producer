package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;

public class ExoPocHdrRefDesc {

	private Integer pocRefDescHdrId;
	private String recordId;
	private String refDesc;
	private Timestamp createDate;

	public Integer getPocRefDescHdrId() {
		return pocRefDescHdrId;
	}

	public void setPocRefDescHdrId(Integer pocRefDescHdrId) {
		this.pocRefDescHdrId = pocRefDescHdrId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefDesc() {
		return refDesc;
	}

	public void setRefDesc(String refDesc) {
		this.refDesc = refDesc;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
}
