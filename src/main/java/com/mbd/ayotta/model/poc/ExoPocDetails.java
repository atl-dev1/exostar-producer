package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

public class ExoPocDetails {
	
	Logger logger = Logger.getLogger(ExoPocDetails.class);
	private ExoPocLineDetails dtl;
	private ExoPocLinePartInfo api;
	private ExoPocLineRefInfoDets rfd;
	private ExoPocLineRefDescDets dsd;
	private ExoPocLineEventDets evt;
	private ExoPocLinePartyInfoDets nmd;
	private ExoPocLineScheduleInfo sch;
	
	// saving POC data in EXO_POC_LINE_DETAIL table.
	public static ExoPocLineDetails getDtl(String str, ExoPocOrderHdr exoPocOrderHdr){
		ExoPocLineDetails dtl = new ExoPocLineDetails();
		ArrayList<String> list = new ArrayList<String>();
		//dtl.setExoPocOrderHdr(exoPocOrderHdr);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				dtl.setRecordId(strVal);
				break;
			case 1:
				dtl.setLineId(strVal);
				break;
			case 2:
				dtl.setChangeType(strVal);
				break;
			case 3:
				dtl.setTotalLineQty(parseStrToDouble(strVal));
				break;
			case 4:
				dtl.setQtyLeftToReciv(strVal);
				break;
			case 5:
				dtl.setUom(strVal);
				break;
			case 6:
				dtl.setBasisUnitPrice(strVal);
				break;
			case 7:
				Double unp = parseStrToDouble(strVal);
				unp = unp == null ? 0 :unp;
				dtl.setUnitPrice(unp);
				break;
			case 8:
				dtl.setPriceBasisQty(parseStrToDouble(strVal));
				break;
			case 9:
				dtl.setPriceBasisUom(strVal);
				break;
			case 10:
				dtl.setBuyerPartNo(strVal);
				break;
			case 11:
				dtl.setPartNoDesc(strVal);
				break;
			case 12:
				dtl.setSupplierPartNo(strVal);
				break;
			case 13:
				dtl.setManufctPartNo(strVal);
				break;
			case 14:
				dtl.setDrawingRevNo(strVal);
				break;
			case 15:
				dtl.setEnggChangelevel(strVal);
				break;
			case 16:
				dtl.setSerialNo(strVal);
				break;
			case 17:
				dtl.setCustOrdNo(strVal);
				break;
			case 18:
				dtl.setExtSysRefNo(strVal);
				break;
			case 19:
				dtl.setCustRefNo(strVal);
				break;
			case 20:
				dtl.setExtSysRefLineNo(strVal);
				break;
			case 21:
				dtl.setOtherProdQual1(strVal);
				break;
			case 22:
				dtl.setOtherProdCode1(strVal);
				break;
			case 23:
				dtl.setOtherProdQual2(strVal);
				break;
			case 24:
				dtl.setOtherProdCode2(strVal);
				break;
			case 25:
				dtl.setPoLineSeqNo(parseStrToInteger(strVal));
				break;
			case 26:
				dtl.setContractPostion(strVal);
				break;
			case 27:
				dtl.setTransactnCatType(strVal);
				break;
			case 28:
				dtl.setCustContract(strVal);
				break;
			case 29:
				dtl.seteInvoiceAllowed(strVal);
				break;
			case 30:
				dtl.setShipAllowed(strVal);
				break;
			case 31:
				dtl.setPricingDesc(strVal);
				break;
			case 32:
				dtl.setDpas(strVal);
				break;
			case 33:
				dtl.setChangeDetails(strVal);
				break;
			case 34:
				dtl.setChangeDetailDesc(strVal);
				break;
			case 35:
				dtl.setPrimeContract(strVal);
				break;
			case 36:
				dtl.setWorkOrdNo(strVal);
				break;
			case 37:
				dtl.setInternalOrdNo(strVal);
				break;
			case 38:
				dtl.setLineItemSumAllot(strVal);
				break;
			case 39:
				dtl.setSpl(strVal);
				break;
			case 40:
				dtl.setTaxType(strVal);
				break;
			case 41:
				dtl.setTaxAmount(parseStrToDouble(strVal));
				break;
			case 42:
				dtl.setTaxPercent(parseStrToDouble(strVal));
				break;
			case 43:
				dtl.setReasonTaxExempt(strVal);
				break;
			case 44:
				dtl.setTaxId(strVal);
				break;
			case 45:
				dtl.setTaxLocQualifier(strVal);
				break;
			case 46:
				dtl.setTaxLocation(strVal);
				break;
			case 47:
				dtl.setReserveDtl47(strVal);
				break;
			case 48:
				dtl.setReserveDtl48(strVal);
				break;
			case 49:
				dtl.setReserveDtl49(strVal);
				break;
			case 50:
				dtl.setReserveDtl50(strVal);
				break;
			case 51:
				dtl.setReserveDtl51(strVal);
				break;
			case 52:
				dtl.setReserveDtl52(strVal);
				break;
			case 53:
				dtl.setReserveDtl53(strVal);
				break;
			case 54:
				dtl.setReserveDtl54(strVal);
				break;
			case 55:
				dtl.setReserveDtl55(strVal);
				break;
			case 56:
				dtl.setReserveDtl56(strVal);
				break;
			case 57:
				dtl.setLineTotal(parseStrToDouble(strVal));
				break;
			case 58:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				dtl.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPocOrderHdr.getExoPocLineDetails().add(dtl);
		return dtl;
	}
	
	// saving POC data in EXO_POC_LINE_PART_INFO table.
	public static ExoPocLinePartInfo getApi(String str, ExoPocLineDetails exoPocLineDetails){
		ExoPocLinePartInfo api = new ExoPocLinePartInfo();
		ArrayList<String> list = new ArrayList<String>();
		//api.setExoPocLineDetails(exoPocLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				api.setRecordId(strVal);
				break;
			case 1:
				api.setAddPartInfo(strVal);
				break;
			case 2:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				api.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPocLineDetails.getExoPocLinePartInfo().add(api);
		return api;
	}
	
	// saving POC data in EXO_POC_LINE_REF_INFO_DETS table.
	public static ExoPocLineRefInfoDets getRfd(String str, ExoPocLineDetails exoPocLineDetails){
		ExoPocLineRefInfoDets rfd = new ExoPocLineRefInfoDets();
		ArrayList<String> list = new ArrayList<String>();
		//rfd.setExoPocLineDetails(exoPocLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				rfd.setRecordId(strVal);
				break;
			case 1:
				rfd.setRefCode(strVal);
				break;
			case 2:
				rfd.setRefTitle(strVal);
				break;
			case 3:
				rfd.setAddRefTitle(strVal);
				break;
			case 4:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				rfd.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPocLineDetails.getExoPocLineRefInfoDets().add(rfd);
		return rfd;
	}
	
	// saving POC data in EXO_POC_LINE_REF_DESC_DETS table.
	public static ExoPocLineRefDescDets getDsd(String str,ExoPocLineDetails exoPocLineDetails){
		ExoPocLineRefDescDets dsd = new ExoPocLineRefDescDets();
		ArrayList<String> list = new ArrayList<String>();
		//dsd.setExoPocLineDetails(exoPocLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				dsd.setRecordId(strVal);
				break;
			case 1:
				dsd.setRefDesc(strVal);
				break;
			case 2:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				dsd.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPocLineDetails.getExoPocLineRefDescDets().add(dsd);
		return dsd;
	}
	
	// saving POC data in EXO_POC_LINE_EVENT_DETS table.
	public static ExoPocLineEventDets getEvt(String str,ExoPocLineDetails exoPocLineDetails){
		ExoPocLineEventDets evt = new ExoPocLineEventDets();
		ArrayList<String> list = new ArrayList<String>();
		//evt.setExoPocLineDetails(exoPocLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				evt.setRecordId(strVal);
				break;
			case 1:
				evt.setEvent(strVal);
				break;
			case 2:
				evt.setDateReq(strVal);
				break;
			case 3:
				evt.setPayAmount(strVal);
				break;
			case 4:
				evt.setEventDesc(strVal);
				break;
			case 5:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				evt.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPocLineDetails.getExoPocLineEventDets().add(evt);
		return evt;
	}
	
	// saving POC data in EXO_PO_LINE_PARTY_DETS table. 	
	public static ExoPocLinePartyInfoDets getNmd(String str,ExoPocLineDetails exoPocLineDetails){
		ExoPocLinePartyInfoDets nmd = new ExoPocLinePartyInfoDets();
		ArrayList<String> list = new ArrayList<String>();
		//nmd.setExoPocLineDetails(exoPocLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				nmd.setRecordId(strVal);
				break;
			case 1:
				nmd.setNameAddrQualifier(strVal);
				break;
			case 2:
				nmd.setName1(strVal);
				break;
			case 3:
				nmd.setName2(strVal);
				break;
			case 4:
				nmd.setName3(strVal);
				break;
			case 5:
				nmd.setIdCode(strVal);
				break;
			case 6:
				nmd.setAddress1(strVal);
				break;
			case 7:
				nmd.setAddress2(strVal);
				break;
			case 8:
				nmd.setAddress3(strVal);
				break;
			case 9:
				nmd.setAddress4(strVal);
				break;
			case 10:
				nmd.setCity(strVal);
				break;
			case 11:
				nmd.setStateCode(strVal);
				break;
			case 12:
				nmd.setZip(strVal);
				break;
			case 13:
				nmd.setCountryCode(strVal);
				break;
			case 14:
				nmd.setContact(strVal);
				break;
			case 15:
				nmd.setEmail(strVal);
				break;
			case 16:
				nmd.setFax(strVal);
				break;
			case 17:
				nmd.setTelephone(strVal);
				break;
			case 18:
				nmd.setReserveNMH18(strVal);
				break;
			case 19:
				nmd.setReserveNMH19(strVal);
				break;
			case 20:
				nmd.setReserveNMH20(strVal);
				break;
			case 21:
				nmd.setReserveNMH21(strVal);
				break;
			case 22:
				nmd.setReserveNMH22(strVal);
				break;
			case 23:
				nmd.setReserveNMH23(strVal);
				break;
			case 24:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				nmd.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPocLineDetails.getExoPocLinePartyInfoDets().add(nmd);
		return nmd;
	}
	
	// saving POC data in EXO_POC_LINE_SCHEDULE_INFO table.
	public static ExoPocLineScheduleInfo getSch(String str,ExoPocLineDetails exoPocLineDetails) throws ParseException{
		ExoPocLineScheduleInfo sch = new ExoPocLineScheduleInfo();
		ArrayList<String> list = new ArrayList<String>();
		//sch.setExoPocLineDetails(exoPocLineDetails);
		list = splitString(str);
		int index = 0;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		for (String strVal : list){
			switch (index){
			case 0:
				sch.setRecordId(strVal);
				break;
			case 1:
				sch.setScheduleId(parseStrToInteger(strVal));
				break;
			case 2:
				sch.setReqScheduleQty(parseStrToDouble(strVal));
				break;
			case 3:
				sch.setUom(strVal);
				break;
			case 4:
				sch.setReqDelDate(stringToDate(strVal));
				break;
			case 5:
				sch.setContrctualDelDate(stringToDate(strVal));
				break;
			case 6:
				sch.setOtherSchedDate(stringToDate(strVal));
				break;
			case 7:
				sch.setRoutingSeqCode(strVal);
				break;
			case 8:
				sch.setModeOfTrans(strVal);
				break;
			case 9:
				sch.setRouting(strVal);
				break;
			case 10:
				sch.setReserveSch10(strVal);
				break;
			case 11:
				sch.setReserveSch11(strVal);
				break;
			case 12:
				sch.setReserveSch12(strVal);
				break;
			case 13:
				sch.setReserveSch13(strVal);
				break;
			case 14:
				sch.setReserveSch14(strVal);
				break;
			case 15:
				sch.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPocLineDetails.getExoPocLineScheduleInfo().add(sch);
		return sch;
	}
	
	
	public static ArrayList<String> splitString(String str){
		
		ArrayList<String> splitList = new ArrayList<String>();
		for(String retVal : str.split("\\|" , -1)){
			splitList.add(retVal);
		}
		return splitList;
	}
	
	public static Double parseStrToDouble(String str){
		if(!str.equals("")){
			return Double.parseDouble(str);
		}else{
			return null;
		}
		
	}
	
	public static Integer parseStrToInteger(String str){
		if(!str.equals("")){
			return Integer.parseInt(str);
		}else{
			return null;
		}
		
	}
	
	private static Date stringToDate(String str){
		Locale locale= null;
		if (locale == null)
			locale = Locale.UK;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", locale);
		try {
			if (str != null && !str.equals("")){
				return new Date(sdf.parse(str).getTime());
			}
		} catch (ParseException e) {
			 e.printStackTrace();
		}
		return null;
	}
	
	private static Date stringToTime(String str){
		Locale locale= null;
		if (locale == null)
			locale = Locale.UK;
		SimpleDateFormat sdf = new SimpleDateFormat("hhmmss", locale);
		try {
			if (str != null){
				return new Date(sdf.parse(str).getTime());
			}
		} catch (ParseException e) {
			 e.printStackTrace();
		}
		return null;
	}

	public ExoPocLineDetails getDtl() {
		return dtl;
	}

	public void setDtl(ExoPocLineDetails dtl) {
		this.dtl = dtl;
	}

	public ExoPocLineRefInfoDets getRfd() {
		return rfd;
	}

	public void setRfd(ExoPocLineRefInfoDets rfd) {
		this.rfd = rfd;
	}

	public ExoPocLineRefDescDets getDsd() {
		return dsd;
	}

	public void setDsd(ExoPocLineRefDescDets dsd) {
		this.dsd = dsd;
	}

	public ExoPocLineEventDets getEvt() {
		return evt;
	}

	public void setEvt(ExoPocLineEventDets evt) {
		this.evt = evt;
	}

	public ExoPocLinePartyInfoDets getNmd() {
		return nmd;
	}

	public void setNmd(ExoPocLinePartyInfoDets nmd) {
		this.nmd = nmd;
	}

	public ExoPocLineScheduleInfo getSch() {
		return sch;
	}

	public void setSch(ExoPocLineScheduleInfo sch) {
		this.sch = sch;
	}

	public ExoPocLinePartInfo getApi() {
		return api;
	}

	public void setApi(ExoPocLinePartInfo api) {
		this.api = api;
	}
}
