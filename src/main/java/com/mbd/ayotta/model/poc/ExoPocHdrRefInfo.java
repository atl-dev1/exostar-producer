package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ExoPocHdrRefInfo {

	private Integer poRefInfoHdrId;
	private String recordId;
	private String refCode;
	private String refTitle;
	private String addRefTitle;
	private Timestamp createDate;
	//private ExoPocOrderHdr exoPocOrderHdr = new ExoPocOrderHdr();
	private ArrayList<ExoPocHdrRefDesc> exoPocHdrRefDesc = new ArrayList<ExoPocHdrRefDesc>();

	public Integer getPoRefInfoHdrId() {
		return poRefInfoHdrId;
	}

	public void setPoRefInfoHdrId(Integer poRefInfoHdrId) {
		this.poRefInfoHdrId = poRefInfoHdrId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public String getRefTitle() {
		return refTitle;
	}

	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}

	public String getAddRefTitle() {
		return addRefTitle;
	}

	public void setAddRefTitle(String addRefTitle) {
		this.addRefTitle = addRefTitle;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public ArrayList<ExoPocHdrRefDesc> getExoPocHdrRefDesc() {
		return exoPocHdrRefDesc;
	}

	public void setExoPocHdrRefDesc(ArrayList<ExoPocHdrRefDesc> exoPocHdrRefDesc) {
		this.exoPocHdrRefDesc = exoPocHdrRefDesc;
	}
}
