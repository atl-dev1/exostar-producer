package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ExoPocTrxControl {

	private Integer trxControlId;
	private String recordId;
	private String funcIdCode;
	private String trxCtrlNo;
	private String senderId;
	private String recieverId;
	private String ffVersionCtrlNo;
	private String bbsId;
	private Date trxDate;
	private String trxTime;
	private String reserveCtl9;
	private String reserveCtl10;
	private String reserveCtl11;
	private String reserveCtl12;
	private String reserveCtl13;
	private Timestamp createDate;
	private ArrayList<ExoPocOrderHdr> exoPocOrderHdrs = new ArrayList<ExoPocOrderHdr>();

	public Integer getTrxControlId() {
		return trxControlId;
	}

	public void setTrxControlId(Integer trxControlId) {
		this.trxControlId = trxControlId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getFuncIdCode() {
		return funcIdCode;
	}

	public void setFuncIdCode(String funcIdCode) {
		this.funcIdCode = funcIdCode;
	}

	public String getTrxCtrlNo() {
		return trxCtrlNo;
	}

	public void setTrxCtrlNo(String trxCtrlNo) {
		this.trxCtrlNo = trxCtrlNo;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getRecieverId() {
		return recieverId;
	}

	public void setRecieverId(String recieverId) {
		this.recieverId = recieverId;
	}

	public String getFfVersionCtrlNo() {
		return ffVersionCtrlNo;
	}

	public void setFfVersionCtrlNo(String ffVersionCtrlNo) {
		this.ffVersionCtrlNo = ffVersionCtrlNo;
	}

	public String getBbsId() {
		return bbsId;
	}

	public void setBbsId(String bbsId) {
		this.bbsId = bbsId;
	}

	public Date getTrxDate() {
		return trxDate;
	}

	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}

	public String getReserveCtl9() {
		return reserveCtl9;
	}

	public void setReserveCtl9(String reserveCtl9) {
		this.reserveCtl9 = reserveCtl9;
	}

	public String getReserveCtl10() {
		return reserveCtl10;
	}

	public void setReserveCtl10(String reserveCtl10) {
		this.reserveCtl10 = reserveCtl10;
	}

	public String getReserveCtl11() {
		return reserveCtl11;
	}

	public void setReserveCtl11(String reserveCtl11) {
		this.reserveCtl11 = reserveCtl11;
	}

	public String getReserveCtl12() {
		return reserveCtl12;
	}

	public void setReserveCtl12(String reserveCtl12) {
		this.reserveCtl12 = reserveCtl12;
	}

	public String getReserveCtl13() {
		return reserveCtl13;
	}

	public void setReserveCtl13(String reserveCtl13) {
		this.reserveCtl13 = reserveCtl13;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public String getTrxTime() {
		return trxTime;
	}

	public void setTrxTime(String trxTime) {
		this.trxTime = trxTime;
	}

	public ArrayList<ExoPocOrderHdr> getExoPocOrderHdrs() {
		return exoPocOrderHdrs;
	}

	public void setExoPocOrderHdrs(ArrayList<ExoPocOrderHdr> exoPocOrderHdrs) {
		this.exoPocOrderHdrs = exoPocOrderHdrs;
	}
}
