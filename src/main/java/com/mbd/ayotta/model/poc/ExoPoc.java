package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.utils.DateUtil;


public class ExoPoc extends AbstractAction{
	
	private ExoPocTrxControl ctl;
	private ExoPocOrderHdr hdr;
	private ExoPocAttHdr att;
	private ExoPocHdrRefInfo rfh;
	private ExoPocHdrRefDesc dsh;
	private ExoPocHdrPartyInfo nmh;
	
	private List<ExoPocDetails> exoPocDetailsList = new ArrayList<ExoPocDetails>();
	
	// saving POC data in EXO_POC_TRX_CONTROL table.
	public static ExoPocTrxControl getCtl(String str){
		ExoPocTrxControl ctl = new ExoPocTrxControl();
		ArrayList<String> ctlList = new ArrayList<String>();
		ctlList = splitString(str);
		int index = 0;
		for (String strVal : ctlList){
			switch (index){
			case 0:
				ctl.setRecordId(strVal);
				break;
			case 1:
				ctl.setFuncIdCode(strVal);
				break;
			case 2:
				ctl.setTrxCtrlNo(strVal);
				break;
			case 3:
				ctl.setSenderId(strVal);
				break;
			case 4:
				ctl.setRecieverId(strVal);
				break;
			case 5:
				ctl.setFfVersionCtrlNo(strVal);
				break;
			case 6:
				ctl.setBbsId(strVal);
				break;
			case 7:
				ctl.setTrxDate(DateUtil.stringToDate(strVal));
				break;
			case 8:
				ctl.setTrxTime(strVal);
				break;
			case 9:
				ctl.setReserveCtl9(strVal);
				break;
			case 10:
				ctl.setReserveCtl10(strVal);
				break;
			case 11:
				ctl.setReserveCtl11(strVal);
				break;
			case 12:
				ctl.setReserveCtl12(strVal);
				break;
			case 13:
				ctl.setReserveCtl13(strVal);
				break;
			case 14:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				ctl.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		return ctl;
	}
	
	// saving POC data in EXO_POC_HEADER table.
	public static ExoPocOrderHdr getHdr(String str, ExoPocTrxControl exoPocTrxCtrl){
		ExoPocOrderHdr hdr = new ExoPocOrderHdr();
		ArrayList<String> list = new ArrayList<String>();
		list = splitString(str);
		//hdr.setExoPocTrxControl(exoPocTrxCtrl);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				hdr.setRecordId(strVal);
				break;
			case 1:
				hdr.setPurpose(strVal);
				break;
			case 2:
				hdr.setBuyerAccount(strVal);
				break;
			case 3:
				hdr.setPoType(strVal);
				break;
			case 4:
				hdr.setPoNo(strVal);
				break;
			case 5:
				hdr.setReleaseNo(strVal);
				break;
			case 6:
				hdr.setChangeOrdSeq(strVal);
				break;
			case 7:
				hdr.setOrderDate(DateUtil.stringToDate(strVal));
				break;
			case 8:
				hdr.setChangeOrderDate(DateUtil.stringToDate(strVal));
				break;
			case 9:
				hdr.setRequestedResponse(strVal);
				break;
			case 10:
				hdr.setShipPayMethod(strVal);
				break;
			case 11:
				hdr.setFobLocation(strVal);
				break;
			case 12:
				hdr.setFobDesc(strVal);
				break;
			case 13:
				hdr.setPaymentTerms(strVal);
				break;
			case 14:
				hdr.setDiscountPercent(parseStrToDouble(strVal));
				break;
			case 15:
				hdr.setDiscountDaysDue(parseStrToInteger(strVal));
				break;
			case 16:
				hdr.setNetDaysDue(parseStrToInteger(strVal));
				break;
			case 17:
				hdr.setContractType(strVal);
				break;
			case 18:
				hdr.setContractEffDate(DateUtil.stringToDate(strVal));
				break;
			case 19:
				hdr.setContractExpDate(DateUtil.stringToDate(strVal));
				break;
			case 20:
				hdr.setTermExpire(DateUtil.stringToDate(strVal));
				break;
			case 21:
				hdr.setCurrencyParty(strVal);
				break;
			case 22:
				hdr.setCurrencyCode(strVal);
				break;
			case 23:
				hdr.setForeignCurrType(strVal);
				break;
			case 24:
				hdr.setForeignCurrRate(strVal);
				break;
			case 25:
				hdr.setOriginatingCompId(strVal);
				break;
			case 26:
				hdr.setReasonForChange(strVal);
				break;
			case 27:
				hdr.setPoReference(strVal);
				break;
			case 28:
				hdr.setStrategicAggNo(strVal);
				break;
			case 29:
				hdr.setRoutingSeqCode(strVal);
				break;
			case 30:
				hdr.setChangeDetails(strVal);
				break;
			case 31:
				hdr.setChangeDetailsDesc(strVal);
				break;
			case 32:
				hdr.setModeOfTrans(strVal);
				break;
			case 33:
				hdr.setRouting(strVal);
				break;
			case 34:
				hdr.setBlanketAuthLimit(parseStrToDouble(strVal));
				break;
			case 35:
				hdr.setTotalPoSumAllot(strVal);
				break;
			case 36:
				hdr.setVendorOrderNo(strVal);
				break;
			case 37:
				hdr.setLegacyStatus(strVal);
				break;
			case 38:
				hdr.setNoOfLines(parseStrToInteger(strVal));
				break;
			case 39:
				hdr.setTotalOrdAmount(parseStrToDouble(strVal));
				break;
			case 40:
				hdr.setTotalOrdQty(parseStrToDouble(strVal));
				break;
			case 41:
				hdr.setTermDesc(strVal);
				break;
			case 42:
				hdr.setReserveHdr42(strVal);
				break;
			case 43:
				hdr.setReserveHdr43(strVal);
				break;
			case 44:
				hdr.setReserveHdr44(strVal);
				break;
			case 45:
				hdr.setReserveHdr45(strVal);
				break;
			case 46:
				hdr.setReserveHdr46(strVal);
				break;
			case 47:
				hdr.setReserveHdr47(strVal);
				break;
			case 48:
				hdr.setReserveHdr48(strVal);
				break;
			case 49:
				hdr.setReserveHdr49(strVal);
				break;
			case 50:
				hdr.setReserveHdr50(strVal);
				break;
			case 51:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				hdr.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPocTrxCtrl.getExoPocOrderHdrs().add(hdr);
		return hdr;
	}
	
	// saving POC data in EXO_POC_HEADER_ATT table.
	public static ExoPocAttHdr getAtt(String str, ExoPocOrderHdr exoPocOrderHdr){
		ExoPocAttHdr att = new ExoPocAttHdr();
		ArrayList<String> list = new ArrayList<String>();
		//att.setExoPocOrderHdr(exoPocOrderHdr);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				att.setRecordId(strVal);
				break;
			case 1:
				att.setFileName(strVal);
				break;
			case 2:
				att.setAttachLoc(strVal);
				break;
			case 3:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				att.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPocOrderHdr.getExoPocAttHdr().add(att);
		return att;
	}
	
	// saving POC data in EXO_POC_HEADER_REF table.
	public static ExoPocHdrRefInfo getRfh(String str, ExoPocOrderHdr exoPocOrderHdr){
		ExoPocHdrRefInfo rfh = new ExoPocHdrRefInfo();
		ArrayList<String> list = new ArrayList<String>();
		//rfh.setExoPocOrderHdr(exoPocOrderHdr);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				rfh.setRecordId(strVal);
				break;
			case 1:
				rfh.setRefCode(strVal);
				break;
			case 2:
				rfh.setRefTitle(strVal);
				break;
			case 3:
				rfh.setAddRefTitle(strVal);
				break;
			case 4:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				rfh.setCreateDate(timestamp);
				break;

			}
			index++;
		}
		exoPocOrderHdr.getExoPocHdrRefInfo().add(rfh);
		return rfh;
	}
	
	// saving POC data in EXO_POC_HEADER_REF_DESC table.
	public static ExoPocHdrRefDesc getDsh(String str, ExoPocHdrRefInfo exoPocHdrRefInfo){
		ExoPocHdrRefDesc dsh = new ExoPocHdrRefDesc();
		ArrayList<String> list = new ArrayList<String>();
		//dsh.setExoPocHdrRefInfo(exoPocHdrRefInfo);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				dsh.setRecordId(strVal);
				break;
			case 1:
				dsh.setRefDesc(strVal);
				break;
			case 2:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				dsh.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPocHdrRefInfo.getExoPocHdrRefDesc().add(dsh);
		return dsh;
	}
	
	// saving POC data in EXO_POC_HEADER_PARTY_INFO table.
	public static ExoPocHdrPartyInfo getNmh(String str, ExoPocOrderHdr exoPocOrderHdr){
		ExoPocHdrPartyInfo nmh = new ExoPocHdrPartyInfo();
		ArrayList<String> list = new ArrayList<String>();
		//nmh.setExoPocOrderHdr(exoPocOrderHdr);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				nmh.setRecordId(strVal);
				break;
			case 1:
				nmh.setNameAddrQualifier(strVal);
				break;
			case 2:
				nmh.setName1(strVal);
				break;
			case 3:
				nmh.setName2(strVal);
				break;
			case 4:
				nmh.setName3(strVal);
				break;
			case 5:
				nmh.setIdCode(strVal);
				break;
			case 6:
				nmh.setAddress1(strVal);
				break;
			case 7:
				nmh.setAddress2(strVal);
				break;
			case 8:
				nmh.setAddress3(strVal);
				break;
			case 9:
				nmh.setAddress4(strVal);
				break;
			case 10:
				nmh.setCity(strVal);
				break;
			case 11:
				nmh.setStateCode(strVal);
				break;
			case 12:
				nmh.setZip(strVal);
				break;
			case 13:
				nmh.setCountryCode(strVal);
				break;
			case 14:
				nmh.setContact(strVal);
				break;
			case 15:
				nmh.setEmail(strVal);
				break;
			case 16:
				nmh.setFax(strVal);
				break;
			case 17:
				nmh.setTelephone(strVal);
				break;
			case 18:
				nmh.setReserveNMH18(strVal);
				break;
			case 19:
				nmh.setReserveNMH19(strVal);
				break;
			case 20:
				nmh.setReserveNMH20(strVal);
				break;
			case 21:
				nmh.setReserveNMH21(strVal);
				break;
			case 22:
				nmh.setReserveNMH22(strVal);
				break;
			case 23:
				nmh.setReserveNMH23(strVal);
				break;
			case 24:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				nmh.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPocOrderHdr.getExoPocHdrPartyInfo().add(nmh);
		return nmh;
	}
	
	// saving POC data in EXO_POC_LINE_DETAIL table.
	public static ExoPocLineDetails getDtl(String str){
		ExoPocLineDetails dtl = new ExoPocLineDetails();
		ArrayList<String> list = new ArrayList<String>();
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				dtl.setRecordId(strVal);
				break;
			case 1:
				dtl.setLineId(strVal);
				break;
			case 2:
				dtl.setTotalLineQty(parseStrToDouble(strVal));
				break;
			case 3:
				dtl.setUom(strVal);
				break;
			case 4:
				dtl.setBasisUnitPrice(strVal);
				break;
			case 5:
				dtl.setUnitPrice(parseStrToDouble(strVal));
				break;
			case 6:
				dtl.setPriceBasisQty(parseStrToDouble(strVal));
				break;
			case 7:
				dtl.setPriceBasisUom(strVal);
				break;
			case 8:
				dtl.setBuyerPartNo(strVal);
				break;
			case 9:
				dtl.setPartNoDesc(strVal);
				break;
			case 10:
				dtl.setSupplierPartNo(strVal);
				break;
			case 11:
				dtl.setManufctPartNo(strVal);
				break;
			case 12:
				dtl.setDrawingRevNo(strVal);
				break;
			case 13:
				dtl.setEnggChangelevel(strVal);
				break;
			case 14:
				dtl.setSerialNo(strVal);
				break;
			case 15:
				dtl.setCustOrdNo(strVal);
				break;
			case 16:
				dtl.setExtSysRefNo(strVal);
				break;
			case 17:
				dtl.setCustRefNo(strVal);
				break;
			case 18:
				dtl.setExtSysRefLineNo(strVal);
				break;
			case 19:
				dtl.setOtherProdQual1(strVal);
				break;
			case 20:
				dtl.setOtherProdCode1(strVal);
				break;
			case 21:
				dtl.setOtherProdQual2(strVal);
				break;
			case 22:
				dtl.setOtherProdCode2(strVal);
				break;
			case 23:
				dtl.setPoLineSeqNo(parseStrToInteger(strVal));
				break;
			case 24:
				dtl.setContractPostion(strVal);
				break;
			case 25:
				dtl.setTransactnCatType(strVal);
				break;
			case 26:
				dtl.setCustContract(strVal);
				break;
			case 27:
				dtl.seteInvoiceAllowed(strVal);
				break;
			case 28:
				dtl.setShipAllowed(strVal);
				break;
			case 29:
				dtl.setPricingDesc(strVal);
				break;
			case 30:
				dtl.setDpas(strVal);
				break;
			case 31:
				dtl.setPrimeContract(strVal);
				break;
			case 32:
				dtl.setWorkOrdNo(strVal);
				break;
			case 33:
				dtl.setInternalOrdNo(strVal);
				break;
			case 34:
				dtl.setLineItemSumAllot(strVal);
				break;
			case 35:
				dtl.setSpl(strVal);
				break;
			case 36:
				dtl.setTaxType(strVal);
				break;
			case 37:
				dtl.setTaxAmount(parseStrToDouble(strVal));
				break;
			case 38:
				dtl.setTaxPercent(parseStrToDouble(strVal));
				break;
			case 39:
				dtl.setReasonTaxExempt(strVal);
				break;
			case 40:
				dtl.setTaxId(strVal);
				break;
			case 41:
				dtl.setTaxLocQualifier(strVal);
				break;
			case 42:
				dtl.setTaxLocation(strVal);
				break;
			case 43:
				dtl.setReserveDtl47(strVal);
				break;
			case 44:
				dtl.setReserveDtl48(strVal);
				break;
			case 45:
				dtl.setReserveDtl49(strVal);
				break;
			case 46:
				dtl.setReserveDtl50(strVal);
				break;
			case 47:
				dtl.setReserveDtl51(strVal);
				break;
			case 48:
				dtl.setReserveDtl52(strVal);
				break;
			case 49:
				dtl.setReserveDtl53(strVal);
				break;
			case 50:
				dtl.setReserveDtl54(strVal);
				break;
			case 51:
				dtl.setReserveDtl55(strVal);
				break;
			case 52:
				dtl.setReserveDtl56(strVal);
				break;
			case 53:
				dtl.setLineTotal(parseStrToDouble(strVal));
				break;
			case 54:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				dtl.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		return dtl;
	}
	
	public ExoPocTrxControl getCtl() {
		return ctl;
	}

	public void setCtl(ExoPocTrxControl ctl) {
		this.ctl = ctl;
	}

	public ExoPocOrderHdr getHdr() {
		return hdr;
	}

	public void setHdr(ExoPocOrderHdr hdr) {
		this.hdr = hdr;
	}

	public ExoPocAttHdr getAtt() {
		return att;
	}

	public void setAtt(ExoPocAttHdr att) {
		this.att = att;
	}

	public ExoPocHdrRefInfo getRfh() {
		return rfh;
	}

	public void setRfh(ExoPocHdrRefInfo rfh) {
		this.rfh = rfh;
	}

	public ExoPocHdrRefDesc getDsh() {
		return dsh;
	}

	public void setDsh(ExoPocHdrRefDesc dsh) {
		this.dsh = dsh;
	}


	public ExoPocHdrPartyInfo getNmh() {
		return nmh;
	}

	public void setNmh(ExoPocHdrPartyInfo nmh) {
		this.nmh = nmh;
	}

	public List<ExoPocDetails> getExoPocDetailsList() {
		return exoPocDetailsList;
	}

	public void setExoPocDetailsList(List<ExoPocDetails> exoPocDetailsList) {
		this.exoPocDetailsList = exoPocDetailsList;
	}
	
	
	
}
