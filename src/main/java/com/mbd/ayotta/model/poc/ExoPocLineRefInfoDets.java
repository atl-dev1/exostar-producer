package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;

public class ExoPocLineRefInfoDets {

	private Integer pocRefInfoDetsId;
	private String recordId;
	private String refCode;
	private String refTitle;
	private String addRefTitle;
	private Timestamp createDate;
	//private ExoPocLineDetails exoPocLineDetails = new ExoPocLineDetails();

	public Integer getPocRefInfoDetsId() {
		return pocRefInfoDetsId;
	}

	public void setPocRefInfoDetsId(Integer pocRefInfoDetsId) {
		this.pocRefInfoDetsId = pocRefInfoDetsId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public String getRefTitle() {
		return refTitle;
	}

	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}

	public String getAddRefTitle() {
		return addRefTitle;
	}

	public void setAddRefTitle(String addRefTitle) {
		this.addRefTitle = addRefTitle;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	/*public ExoPocLineDetails getExoPocLineDetails() {
		return exoPocLineDetails;
	}

	public void setExoPocLineDetails(ExoPocLineDetails exoPocLineDetails) {
		this.exoPocLineDetails = exoPocLineDetails;
	}*/

}
