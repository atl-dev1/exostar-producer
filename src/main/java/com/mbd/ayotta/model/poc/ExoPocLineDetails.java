package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;
import java.util.ArrayList;

public class ExoPocLineDetails {

	private Integer pocLineDetailId;
	private String recordId;
	private String lineId;
	private String changeType;
	private Double totalLineQty;
	private String qtyLeftToReciv;
	private String uom;
	private String basisUnitPrice;
	private Double unitPrice;
	private Double priceBasisQty;
	private String priceBasisUom;
	private String buyerPartNo;
	private String partNoDesc;
	private String supplierPartNo;
	private String manufctPartNo;
	private String drawingRevNo;
	private String enggChangelevel;
	private String serialNo;
	private String custOrdNo;
	private String extSysRefNo;
	private String custRefNo;
	private String extSysRefLineNo;
	private String otherProdQual1;
	private String otherProdCode1;
	private String otherProdQual2;
	private String otherProdCode2;
	private Integer poLineSeqNo;
	private String contractPostion;
	private String transactnCatType;
	private String custContract;
	private String eInvoiceAllowed;
	private String shipAllowed;
	private String pricingDesc;
	private String dpas;
	private String changeDetails;
	private String changeDetailDesc;
	private String primeContract;
	private String workOrdNo;
	private String internalOrdNo;
	private String lineItemSumAllot;
	private String spl;
	private String taxType;
	private Double taxAmount;
	private Double taxPercent;
	private String reasonTaxExempt;
	private String taxId;
	private String taxLocQualifier;
	private String taxLocation;
	private String reserveDtl47;
	private String reserveDtl48;
	private String reserveDtl49;
	private String reserveDtl50;
	private String reserveDtl51;
	private String reserveDtl52;
	private String reserveDtl53;
	private String reserveDtl54;
	private String reserveDtl55;
	private String reserveDtl56;
	private Double lineTotal;
	private Timestamp createDate;
	//private ExoPocOrderHdr exoPocOrderHdr = new ExoPocOrderHdr();
	private ArrayList<ExoPocLineEventDets> exoPocLineEventDets = new ArrayList<ExoPocLineEventDets>();
	private ArrayList<ExoPocLinePartInfo> exoPocLinePartInfo = new ArrayList<ExoPocLinePartInfo>();
	private ArrayList<ExoPocLinePartyInfoDets> exoPocLinePartyInfoDets = new ArrayList<ExoPocLinePartyInfoDets>();
	private ArrayList<ExoPocLineRefDescDets> exoPocLineRefDescDets = new ArrayList<ExoPocLineRefDescDets>();
	private ArrayList<ExoPocLineRefInfoDets> exoPocLineRefInfoDets = new ArrayList<ExoPocLineRefInfoDets>();
	private ArrayList<ExoPocLineScheduleInfo> exoPocLineScheduleInfo = new ArrayList<ExoPocLineScheduleInfo>();

	public Integer getPocLineDetailId() {
		return pocLineDetailId;
	}

	public void setPocLineDetailId(Integer pocLineDetailId) {
		this.pocLineDetailId = pocLineDetailId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getLineId() {
		return lineId;
	}

	public void setLineId(String lineId) {
		this.lineId = lineId;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public Double getTotalLineQty() {
		return totalLineQty;
	}

	public void setTotalLineQty(Double totalLineQty) {
		this.totalLineQty = totalLineQty;
	}

	public String getQtyLeftToReciv() {
		return qtyLeftToReciv;
	}

	public void setQtyLeftToReciv(String qtyLeftToReciv) {
		this.qtyLeftToReciv = qtyLeftToReciv;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getBasisUnitPrice() {
		return basisUnitPrice;
	}

	public void setBasisUnitPrice(String basisUnitPrice) {
		this.basisUnitPrice = basisUnitPrice;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getPriceBasisQty() {
		return priceBasisQty;
	}

	public void setPriceBasisQty(Double priceBasisQty) {
		this.priceBasisQty = priceBasisQty;
	}

	public String getPriceBasisUom() {
		return priceBasisUom;
	}

	public void setPriceBasisUom(String priceBasisUom) {
		this.priceBasisUom = priceBasisUom;
	}

	public String getBuyerPartNo() {
		return buyerPartNo;
	}

	public void setBuyerPartNo(String buyerPartNo) {
		this.buyerPartNo = buyerPartNo;
	}

	public String getPartNoDesc() {
		return partNoDesc;
	}

	public void setPartNoDesc(String partNoDesc) {
		this.partNoDesc = partNoDesc;
	}

	public String getSupplierPartNo() {
		return supplierPartNo;
	}

	public void setSupplierPartNo(String supplierPartNo) {
		this.supplierPartNo = supplierPartNo;
	}

	public String getManufctPartNo() {
		return manufctPartNo;
	}

	public void setManufctPartNo(String manufctPartNo) {
		this.manufctPartNo = manufctPartNo;
	}

	public String getDrawingRevNo() {
		return drawingRevNo;
	}

	public void setDrawingRevNo(String drawingRevNo) {
		this.drawingRevNo = drawingRevNo;
	}

	public String getEnggChangelevel() {
		return enggChangelevel;
	}

	public void setEnggChangelevel(String enggChangelevel) {
		this.enggChangelevel = enggChangelevel;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getCustOrdNo() {
		return custOrdNo;
	}

	public void setCustOrdNo(String custOrdNo) {
		this.custOrdNo = custOrdNo;
	}

	public String getExtSysRefNo() {
		return extSysRefNo;
	}

	public void setExtSysRefNo(String extSysRefNo) {
		this.extSysRefNo = extSysRefNo;
	}

	public String getCustRefNo() {
		return custRefNo;
	}

	public void setCustRefNo(String custRefNo) {
		this.custRefNo = custRefNo;
	}

	public String getExtSysRefLineNo() {
		return extSysRefLineNo;
	}

	public void setExtSysRefLineNo(String extSysRefLineNo) {
		this.extSysRefLineNo = extSysRefLineNo;
	}

	public String getOtherProdQual1() {
		return otherProdQual1;
	}

	public void setOtherProdQual1(String otherProdQual1) {
		this.otherProdQual1 = otherProdQual1;
	}

	public String getOtherProdCode1() {
		return otherProdCode1;
	}

	public void setOtherProdCode1(String otherProdCode1) {
		this.otherProdCode1 = otherProdCode1;
	}

	public String getOtherProdQual2() {
		return otherProdQual2;
	}

	public void setOtherProdQual2(String otherProdQual2) {
		this.otherProdQual2 = otherProdQual2;
	}

	public String getOtherProdCode2() {
		return otherProdCode2;
	}

	public void setOtherProdCode2(String otherProdCode2) {
		this.otherProdCode2 = otherProdCode2;
	}

	public Integer getPoLineSeqNo() {
		return poLineSeqNo;
	}

	public void setPoLineSeqNo(Integer poLineSeqNo) {
		this.poLineSeqNo = poLineSeqNo;
	}

	public String getContractPostion() {
		return contractPostion;
	}

	public void setContractPostion(String contractPostion) {
		this.contractPostion = contractPostion;
	}

	public String getTransactnCatType() {
		return transactnCatType;
	}

	public void setTransactnCatType(String transactnCatType) {
		this.transactnCatType = transactnCatType;
	}

	public String getCustContract() {
		return custContract;
	}

	public void setCustContract(String custContract) {
		this.custContract = custContract;
	}

	public String geteInvoiceAllowed() {
		return eInvoiceAllowed;
	}

	public void seteInvoiceAllowed(String eInvoiceAllowed) {
		this.eInvoiceAllowed = eInvoiceAllowed;
	}

	public String getShipAllowed() {
		return shipAllowed;
	}

	public void setShipAllowed(String shipAllowed) {
		this.shipAllowed = shipAllowed;
	}

	public String getPricingDesc() {
		return pricingDesc;
	}

	public void setPricingDesc(String pricingDesc) {
		this.pricingDesc = pricingDesc;
	}

	public String getDpas() {
		return dpas;
	}

	public void setDpas(String dpas) {
		this.dpas = dpas;
	}

	public String getChangeDetails() {
		return changeDetails;
	}

	public void setChangeDetails(String changeDetails) {
		this.changeDetails = changeDetails;
	}

	public String getChangeDetailDesc() {
		return changeDetailDesc;
	}

	public void setChangeDetailDesc(String changeDetailDesc) {
		this.changeDetailDesc = changeDetailDesc;
	}

	public String getPrimeContract() {
		return primeContract;
	}

	public void setPrimeContract(String primeContract) {
		this.primeContract = primeContract;
	}

	public String getWorkOrdNo() {
		return workOrdNo;
	}

	public void setWorkOrdNo(String workOrdNo) {
		this.workOrdNo = workOrdNo;
	}

	public String getInternalOrdNo() {
		return internalOrdNo;
	}

	public void setInternalOrdNo(String internalOrdNo) {
		this.internalOrdNo = internalOrdNo;
	}

	public String getLineItemSumAllot() {
		return lineItemSumAllot;
	}

	public void setLineItemSumAllot(String lineItemSumAllot) {
		this.lineItemSumAllot = lineItemSumAllot;
	}

	public String getSpl() {
		return spl;
	}

	public void setSpl(String spl) {
		this.spl = spl;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public Double getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(Double taxPercent) {
		this.taxPercent = taxPercent;
	}

	public String getReasonTaxExempt() {
		return reasonTaxExempt;
	}

	public void setReasonTaxExempt(String reasonTaxExempt) {
		this.reasonTaxExempt = reasonTaxExempt;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getTaxLocQualifier() {
		return taxLocQualifier;
	}

	public void setTaxLocQualifier(String taxLocQualifier) {
		this.taxLocQualifier = taxLocQualifier;
	}

	public String getTaxLocation() {
		return taxLocation;
	}

	public void setTaxLocation(String taxLocation) {
		this.taxLocation = taxLocation;
	}

	public String getReserveDtl47() {
		return reserveDtl47;
	}

	public void setReserveDtl47(String reserveDtl47) {
		this.reserveDtl47 = reserveDtl47;
	}

	public String getReserveDtl48() {
		return reserveDtl48;
	}

	public void setReserveDtl48(String reserveDtl48) {
		this.reserveDtl48 = reserveDtl48;
	}

	public String getReserveDtl49() {
		return reserveDtl49;
	}

	public void setReserveDtl49(String reserveDtl49) {
		this.reserveDtl49 = reserveDtl49;
	}

	public String getReserveDtl50() {
		return reserveDtl50;
	}

	public void setReserveDtl50(String reserveDtl50) {
		this.reserveDtl50 = reserveDtl50;
	}

	public String getReserveDtl51() {
		return reserveDtl51;
	}

	public void setReserveDtl51(String reserveDtl51) {
		this.reserveDtl51 = reserveDtl51;
	}

	public String getReserveDtl52() {
		return reserveDtl52;
	}

	public void setReserveDtl52(String reserveDtl52) {
		this.reserveDtl52 = reserveDtl52;
	}

	public String getReserveDtl53() {
		return reserveDtl53;
	}

	public void setReserveDtl53(String reserveDtl53) {
		this.reserveDtl53 = reserveDtl53;
	}

	public String getReserveDtl54() {
		return reserveDtl54;
	}

	public void setReserveDtl54(String reserveDtl54) {
		this.reserveDtl54 = reserveDtl54;
	}

	public String getReserveDtl55() {
		return reserveDtl55;
	}

	public void setReserveDtl55(String reserveDtl55) {
		this.reserveDtl55 = reserveDtl55;
	}

	public String getReserveDtl56() {
		return reserveDtl56;
	}

	public void setReserveDtl56(String reserveDtl56) {
		this.reserveDtl56 = reserveDtl56;
	}

	public Double getLineTotal() {
		return lineTotal;
	}

	public void setLineTotal(Double lineTotal) {
		this.lineTotal = lineTotal;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	/*public ExoPocOrderHdr getExoPocOrderHdr() {
		return exoPocOrderHdr;
	}

	public void setExoPocOrderHdr(ExoPocOrderHdr exoPocOrderHdr) {
		this.exoPocOrderHdr = exoPocOrderHdr;
	}*/

	public ArrayList<ExoPocLineEventDets> getExoPocLineEventDets() {
		return exoPocLineEventDets;
	}

	public void setExoPocLineEventDets(
			ArrayList<ExoPocLineEventDets> exoPocLineEventDets) {
		this.exoPocLineEventDets = exoPocLineEventDets;
	}

	public ArrayList<ExoPocLinePartInfo> getExoPocLinePartInfo() {
		return exoPocLinePartInfo;
	}

	public void setExoPocLinePartInfo(
			ArrayList<ExoPocLinePartInfo> exoPocLinePartInfo) {
		this.exoPocLinePartInfo = exoPocLinePartInfo;
	}

	public ArrayList<ExoPocLinePartyInfoDets> getExoPocLinePartyInfoDets() {
		return exoPocLinePartyInfoDets;
	}

	public void setExoPocLinePartyInfoDets(
			ArrayList<ExoPocLinePartyInfoDets> exoPocLinePartyInfoDets) {
		this.exoPocLinePartyInfoDets = exoPocLinePartyInfoDets;
	}

	public ArrayList<ExoPocLineRefDescDets> getExoPocLineRefDescDets() {
		return exoPocLineRefDescDets;
	}

	public void setExoPocLineRefDescDets(
			ArrayList<ExoPocLineRefDescDets> exoPocLineRefDescDets) {
		this.exoPocLineRefDescDets = exoPocLineRefDescDets;
	}

	public ArrayList<ExoPocLineRefInfoDets> getExoPocLineRefInfoDets() {
		return exoPocLineRefInfoDets;
	}

	public void setExoPocLineRefInfoDets(
			ArrayList<ExoPocLineRefInfoDets> exoPocLineRefInfoDets) {
		this.exoPocLineRefInfoDets = exoPocLineRefInfoDets;
	}

	public ArrayList<ExoPocLineScheduleInfo> getExoPocLineScheduleInfo() {
		return exoPocLineScheduleInfo;
	}

	public void setExoPocLineScheduleInfo(
			ArrayList<ExoPocLineScheduleInfo> exoPocLineScheduleInfo) {
		this.exoPocLineScheduleInfo = exoPocLineScheduleInfo;
	}

}
