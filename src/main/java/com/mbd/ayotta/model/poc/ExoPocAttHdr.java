package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;

public class ExoPocAttHdr {

	private Integer poAttachHdrId;
	private String recordId;
	private String fileName;
	private String AttachLoc;
	private Timestamp createDate;
	//private ExoPocOrderHdr exoPocOrderHdr = new ExoPocOrderHdr();

	public Integer getPocAttachHdrId() {
		return poAttachHdrId;
	}

	public void setPocAttachHdrId(Integer poAttachHdrId) {
		this.poAttachHdrId = poAttachHdrId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAttachLoc() {
		return AttachLoc;
	}

	public void setAttachLoc(String attachLoc) {
		AttachLoc = attachLoc;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	/*public ExoPocOrderHdr getExoPocOrderHdr() {
		return exoPocOrderHdr;
	}

	public void setExoPocOrderHdr(ExoPocOrderHdr exoPocOrderHdr) {
		this.exoPocOrderHdr = exoPocOrderHdr;
	}*/

	public Integer getPoAttachHdrId() {
		return poAttachHdrId;
	}

	public void setPoAttachHdrId(Integer poAttachHdrId) {
		this.poAttachHdrId = poAttachHdrId;
	}
}
