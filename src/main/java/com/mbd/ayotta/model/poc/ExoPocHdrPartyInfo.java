package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;

public class ExoPocHdrPartyInfo {

	private Integer pocPartyInfoHdrId;
	private String recordId;
	private String nameAddrQualifier;
	private String name1;
	private String name2;
	private String name3;
	private String idCode;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String city;
	private String stateCode;
	private String zip;
	private String countryCode;
	private String contact;
	private String email;
	private String fax;
	private String telephone;
	private String reserveNMH18;
	private String reserveNMH19;
	private String reserveNMH20;
	private String reserveNMH21;
	private String reserveNMH22;
	private String reserveNMH23;
	private Timestamp createDate;
	//private ExoPocOrderHdr exoPocOrderHdr = new ExoPocOrderHdr();

	public Integer getPocPartyInfoHdrId() {
		return pocPartyInfoHdrId;
	}

	public void setPocPartyInfoHdrId(Integer pocPartyInfoHdrId) {
		this.pocPartyInfoHdrId = pocPartyInfoHdrId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getNameAddrQualifier() {
		return nameAddrQualifier;
	}

	public void setNameAddrQualifier(String nameAddrQualifier) {
		this.nameAddrQualifier = nameAddrQualifier;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public String getIdCode() {
		return idCode;
	}

	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	/*public ExoPocOrderHdr getExoPocOrderHdr() {
		return exoPocOrderHdr;
	}

	public void setExoPocOrderHdr(ExoPocOrderHdr exoPocOrderHdr) {
		this.exoPocOrderHdr = exoPocOrderHdr;
	}*/

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getReserveNMH18() {
		return reserveNMH18;
	}

	public void setReserveNMH18(String reserveNMH18) {
		this.reserveNMH18 = reserveNMH18;
	}

	public String getReserveNMH19() {
		return reserveNMH19;
	}

	public void setReserveNMH19(String reserveNMH19) {
		this.reserveNMH19 = reserveNMH19;
	}

	public String getReserveNMH20() {
		return reserveNMH20;
	}

	public void setReserveNMH20(String reserveNMH20) {
		this.reserveNMH20 = reserveNMH20;
	}

	public String getReserveNMH21() {
		return reserveNMH21;
	}

	public void setReserveNMH21(String reserveNMH21) {
		this.reserveNMH21 = reserveNMH21;
	}

	public String getReserveNMH22() {
		return reserveNMH22;
	}

	public void setReserveNMH22(String reserveNMH22) {
		this.reserveNMH22 = reserveNMH22;
	}

	public String getReserveNMH23() {
		return reserveNMH23;
	}

	public void setReserveNMH23(String reserveNMH23) {
		this.reserveNMH23 = reserveNMH23;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
}
