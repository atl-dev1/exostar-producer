package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;

public class ExoPocLineRefDescDets {

	private Integer pocRefDescDetsId;
	private String recordId;
	private String refDesc;
	private Timestamp createDate;
	//private ExoPocLineDetails exoPocLineDetails = new ExoPocLineDetails();

	public Integer getPocRefDescDetsId() {
		return pocRefDescDetsId;
	}

	public void setPocRefDescDetsId(Integer pocRefDescDetsId) {
		this.pocRefDescDetsId = pocRefDescDetsId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefDesc() {
		return refDesc;
	}

	public void setRefDesc(String refDesc) {
		this.refDesc = refDesc;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	/*public ExoPocLineDetails getExoPocLineDetails() {
		return exoPocLineDetails;
	}

	public void setExoPocLineDetails(ExoPocLineDetails exoPocLineDetails) {
		this.exoPocLineDetails = exoPocLineDetails;
	}*/

}
