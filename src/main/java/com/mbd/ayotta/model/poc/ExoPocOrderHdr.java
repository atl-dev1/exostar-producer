package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class ExoPocOrderHdr {

	private Integer pocOrderHdrId;
	private String recordId;
	private String purpose;
	private String buyerAccount;
	private String poType;
	private String poNo;
	private String releaseNo;
	private String changeOrdSeq;
	private Date orderDate;
	private Date changeOrderDate;
	private String requestedResponse;
	private String shipPayMethod;
	private String fobLocation;
	private String fobDesc;
	private String paymentTerms;
	private Double discountPercent;
	private Integer discountDaysDue;
	private Integer netDaysDue;
	private String contractType;
	private Date contractEffDate;
	private Date contractExpDate;
	private Date termExpire;
	private String currencyParty;
	private String currencyCode;
	private String foreignCurrType;
	private String foreignCurrRate;
	private String originatingCompId;
	private String reasonForChange;
	private String poReference;
	private String strategicAggNo;
	private String routingSeqCode;
	private String changeDetails;
	private String changeDetailsDesc;
	private String modeOfTrans;
	private String routing;
	private Double blanketAuthLimit;
	private String totalPoSumAllot;
	private String vendorOrderNo;
	private String legacyStatus;
	private Integer noOfLines;
	private Double totalOrdAmount;
	private Double totalOrdQty;
	private String termDesc;
	private String reserveHdr42;
	private String reserveHdr43;
	private String reserveHdr44;
	private String reserveHdr45;
	private String reserveHdr46;
	private String reserveHdr47;
	private String reserveHdr48;
	private String reserveHdr49;
	private String reserveHdr50;
	private Timestamp createDate;
	private Integer origExoPoHdrId;
	//private ExoPocTrxControl exoPocTrxControl = new ExoPocTrxControl();
	private ArrayList<ExoPocAttHdr> exoPocAttHdr = new ArrayList<ExoPocAttHdr>();
	private ArrayList<ExoPocHdrPartyInfo> exoPocHdrPartyInfo = new ArrayList<ExoPocHdrPartyInfo>();
	private ArrayList<ExoPocHdrRefInfo> exoPocHdrRefInfo = new ArrayList<ExoPocHdrRefInfo>();
	private ArrayList<ExoPocLineDetails> exoPocLineDetails = new ArrayList<ExoPocLineDetails>();

	public Integer getPocOrderHdrId() {
		return pocOrderHdrId;
	}

	public void setPocOrderHdrId(Integer pocOrderHdrId) {
		this.pocOrderHdrId = pocOrderHdrId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getBuyerAccount() {
		return buyerAccount;
	}

	public void setBuyerAccount(String buyerAccount) {
		this.buyerAccount = buyerAccount;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getReleaseNo() {
		return releaseNo;
	}

	public void setReleaseNo(String releaseNo) {
		this.releaseNo = releaseNo;
	}

	public String getChangeOrdSeq() {
		return changeOrdSeq;
	}

	public void setChangeOrdSeq(String changeOrdSeq) {
		this.changeOrdSeq = changeOrdSeq;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Date getChangeOrderDate() {
		return changeOrderDate;
	}

	public void setChangeOrderDate(Date changeOrderDate) {
		this.changeOrderDate = changeOrderDate;
	}

	public String getRequestedResponse() {
		return requestedResponse;
	}

	public void setRequestedResponse(String requestedResponse) {
		this.requestedResponse = requestedResponse;
	}

	public String getShipPayMethod() {
		return shipPayMethod;
	}

	public void setShipPayMethod(String shipPayMethod) {
		this.shipPayMethod = shipPayMethod;
	}

	public String getFobLocation() {
		return fobLocation;
	}

	public void setFobLocation(String fobLocation) {
		this.fobLocation = fobLocation;
	}

	public String getFobDesc() {
		return fobDesc;
	}

	public void setFobDesc(String fobDesc) {
		this.fobDesc = fobDesc;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Double getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}

	public Integer getDiscountDaysDue() {
		return discountDaysDue;
	}

	public void setDiscountDaysDue(Integer discountDaysDue) {
		this.discountDaysDue = discountDaysDue;
	}

	public Integer getNetDaysDue() {
		return netDaysDue;
	}

	public void setNetDaysDue(Integer netDaysDue) {
		this.netDaysDue = netDaysDue;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public Date getContractEffDate() {
		return contractEffDate;
	}

	public void setContractEffDate(Date contractEffDate) {
		this.contractEffDate = contractEffDate;
	}

	public Date getContractExpDate() {
		return contractExpDate;
	}

	public void setContractExpDate(Date contractExpDate) {
		this.contractExpDate = contractExpDate;
	}

	public Date getTermExpire() {
		return termExpire;
	}

	public void setTermExpire(Date termExpire) {
		this.termExpire = termExpire;
	}

	public String getCurrencyParty() {
		return currencyParty;
	}

	public void setCurrencyParty(String currencyParty) {
		this.currencyParty = currencyParty;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getForeignCurrType() {
		return foreignCurrType;
	}

	public void setForeignCurrType(String foreignCurrType) {
		this.foreignCurrType = foreignCurrType;
	}

	public String getForeignCurrRate() {
		return foreignCurrRate;
	}

	public void setForeignCurrRate(String foreignCurrRate) {
		this.foreignCurrRate = foreignCurrRate;
	}

	public String getOriginatingCompId() {
		return originatingCompId;
	}

	public void setOriginatingCompId(String originatingCompId) {
		this.originatingCompId = originatingCompId;
	}

	public String getReasonForChange() {
		return reasonForChange;
	}

	public void setReasonForChange(String reasonForChange) {
		this.reasonForChange = reasonForChange;
	}

	public String getPoReference() {
		return poReference;
	}

	public void setPoReference(String poReference) {
		this.poReference = poReference;
	}

	public String getStrategicAggNo() {
		return strategicAggNo;
	}

	public void setStrategicAggNo(String strategicAggNo) {
		this.strategicAggNo = strategicAggNo;
	}

	public String getRoutingSeqCode() {
		return routingSeqCode;
	}

	public void setRoutingSeqCode(String routingSeqCode) {
		this.routingSeqCode = routingSeqCode;
	}

	public String getChangeDetails() {
		return changeDetails;
	}

	public void setChangeDetails(String changeDetails) {
		this.changeDetails = changeDetails;
	}

	public String getChangeDetailsDesc() {
		return changeDetailsDesc;
	}

	public void setChangeDetailsDesc(String changeDetailsDesc) {
		this.changeDetailsDesc = changeDetailsDesc;
	}

	public String getModeOfTrans() {
		return modeOfTrans;
	}

	public void setModeOfTrans(String modeOfTrans) {
		this.modeOfTrans = modeOfTrans;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public Double getBlanketAuthLimit() {
		return blanketAuthLimit;
	}

	public void setBlanketAuthLimit(Double blanketAuthLimit) {
		this.blanketAuthLimit = blanketAuthLimit;
	}

	public String getTotalPoSumAllot() {
		return totalPoSumAllot;
	}

	public void setTotalPoSumAllot(String totalPoSumAllot) {
		this.totalPoSumAllot = totalPoSumAllot;
	}

	public String getVendorOrderNo() {
		return vendorOrderNo;
	}

	public void setVendorOrderNo(String vendorOrderNo) {
		this.vendorOrderNo = vendorOrderNo;
	}

	public String getLegacyStatus() {
		return legacyStatus;
	}

	public void setLegacyStatus(String legacyStatus) {
		this.legacyStatus = legacyStatus;
	}

	public Integer getNoOfLines() {
		return noOfLines;
	}

	public void setNoOfLines(Integer noOfLines) {
		this.noOfLines = noOfLines;
	}

	public Double getTotalOrdAmount() {
		return totalOrdAmount;
	}

	public void setTotalOrdAmount(Double totalOrdAmount) {
		this.totalOrdAmount = totalOrdAmount;
	}

	public Double getTotalOrdQty() {
		return totalOrdQty;
	}

	public void setTotalOrdQty(Double totalOrdQty) {
		this.totalOrdQty = totalOrdQty;
	}

	public String getTermDesc() {
		return termDesc;
	}

	public void setTermDesc(String termDesc) {
		this.termDesc = termDesc;
	}

	public String getReserveHdr42() {
		return reserveHdr42;
	}

	public void setReserveHdr42(String reserveHdr42) {
		this.reserveHdr42 = reserveHdr42;
	}

	public String getReserveHdr43() {
		return reserveHdr43;
	}

	public void setReserveHdr43(String reserveHdr43) {
		this.reserveHdr43 = reserveHdr43;
	}

	public String getReserveHdr44() {
		return reserveHdr44;
	}

	public void setReserveHdr44(String reserveHdr44) {
		this.reserveHdr44 = reserveHdr44;
	}

	public String getReserveHdr45() {
		return reserveHdr45;
	}

	public void setReserveHdr45(String reserveHdr45) {
		this.reserveHdr45 = reserveHdr45;
	}

	public String getReserveHdr46() {
		return reserveHdr46;
	}

	public void setReserveHdr46(String reserveHdr46) {
		this.reserveHdr46 = reserveHdr46;
	}

	public String getReserveHdr47() {
		return reserveHdr47;
	}

	public void setReserveHdr47(String reserveHdr47) {
		this.reserveHdr47 = reserveHdr47;
	}

	public String getReserveHdr48() {
		return reserveHdr48;
	}

	public void setReserveHdr48(String reserveHdr48) {
		this.reserveHdr48 = reserveHdr48;
	}

	public String getReserveHdr49() {
		return reserveHdr49;
	}

	public void setReserveHdr49(String reserveHdr49) {
		this.reserveHdr49 = reserveHdr49;
	}

	public String getReserveHdr50() {
		return reserveHdr50;
	}

	public void setReserveHdr50(String reserveHdr50) {
		this.reserveHdr50 = reserveHdr50;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Integer getOrigExoPoHdrId() {
		return origExoPoHdrId;
	}

	public void setOrigExoPoHdrId(Integer origExoPoHdrId) {
		this.origExoPoHdrId = origExoPoHdrId;
	}

	public ArrayList<ExoPocAttHdr> getExoPocAttHdr() {
		return exoPocAttHdr;
	}

	public void setExoPocAttHdr(ArrayList<ExoPocAttHdr> exoPocAttHdr) {
		this.exoPocAttHdr = exoPocAttHdr;
	}

	public ArrayList<ExoPocHdrPartyInfo> getExoPocHdrPartyInfo() {
		return exoPocHdrPartyInfo;
	}

	public void setExoPocHdrPartyInfo(
			ArrayList<ExoPocHdrPartyInfo> exoPocHdrPartyInfo) {
		this.exoPocHdrPartyInfo = exoPocHdrPartyInfo;
	}

	public ArrayList<ExoPocHdrRefInfo> getExoPocHdrRefInfo() {
		return exoPocHdrRefInfo;
	}

	public void setExoPocHdrRefInfo(ArrayList<ExoPocHdrRefInfo> exoPocHdrRefInfo) {
		this.exoPocHdrRefInfo = exoPocHdrRefInfo;
	}

	public ArrayList<ExoPocLineDetails> getExoPocLineDetails() {
		return exoPocLineDetails;
	}

	public void setExoPocLineDetails(
			ArrayList<ExoPocLineDetails> exoPocLineDetails) {
		this.exoPocLineDetails = exoPocLineDetails;
	}

}
