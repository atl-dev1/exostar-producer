package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;

public class ExoPocLineEventDets {

	private Integer pocLineEventDetsId;
	private String recordId;
	private String event;
	private String dateReq;
	private String payAmount;
	private String eventDesc;
	private Timestamp createDate;
	//private ExoPocLineDetails exoPocLineDetails = new ExoPocLineDetails();

	public Integer getPocLineEventDetsId() {
		return pocLineEventDetsId;
	}

	public void setPocLineEventDetsId(Integer pocLineEventDetsId) {
		this.pocLineEventDetsId = pocLineEventDetsId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getDateReq() {
		return dateReq;
	}

	public void setDateReq(String dateReq) {
		this.dateReq = dateReq;
	}

	public String getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	/*public ExoPocLineDetails getExoPocLineDetails() {
		return exoPocLineDetails;
	}

	public void setExoPocLineDetails(ExoPocLineDetails exoPocLineDetails) {
		this.exoPocLineDetails = exoPocLineDetails;
	}*/

}
