package com.mbd.ayotta.model.poc;

import java.sql.Timestamp;

public class ExoPocLinePartInfo {

	private Integer pocLinePartInfoId;
	private String recordId;
	private String addPartInfo;
	private Timestamp createDate;
	//private ExoPocLineDetails exoPocLineDetails = new ExoPocLineDetails();

	public Integer getPocLinePartInfoId() {
		return pocLinePartInfoId;
	}

	public void setPocLinePartInfoId(Integer pocLinePartInfoId) {
		this.pocLinePartInfoId = pocLinePartInfoId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getAddPartInfo() {
		return addPartInfo;
	}

	public void setAddPartInfo(String addPartInfo) {
		this.addPartInfo = addPartInfo;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	/*public ExoPocLineDetails getExoPocLineDetails() {
		return exoPocLineDetails;
	}

	public void setExoPocLineDetails(ExoPocLineDetails exoPocLineDetails) {
		this.exoPocLineDetails = exoPocLineDetails;
	}*/

}
