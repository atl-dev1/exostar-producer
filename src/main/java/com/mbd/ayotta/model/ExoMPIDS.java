package com.mbd.ayotta.model;


public class ExoMPIDS {
	
	private static String buyerMPID;
	private static String supplierMPID;
	private static String exostarMPID;
	
	public static String getBuyerMPID() {
		return buyerMPID;
	}
	public  void setBuyerMPID(String buyerMPID) {
		ExoMPIDS.buyerMPID = buyerMPID;
	}
	public static String getSupplierMPID() {
		return supplierMPID;
	}
	public  void setSupplierMPID(String supplierMPID) {
		ExoMPIDS.supplierMPID = supplierMPID;
	}
	public  static String getExostarMPID() {
		return exostarMPID;
	}
	public  void setExostarMPID(String exostarMPID) {
		ExoMPIDS.exostarMPID = exostarMPID;
	}
	
	

	
}
