package com.mbd.ayotta.model;

public class ExoTrig {

	private String trigFeed;
	private String trigPKey;
	private String trigDate;

	
	public ExoTrig() {
	}

	public String getTrigFeed() {
		return trigFeed;
	}

	public void setTrigFeed(String trigFeed) {
		this.trigFeed = trigFeed;
	}

	public String getTrigPKey() {
		return trigPKey;
	}

	public void setTrigPKey(String trigPKey) {
		this.trigPKey = trigPKey;
	}

	public String getTrigDate() {
		return trigDate;
	}

	public void setTrigDate(String trigDate) {
		this.trigDate = trigDate;
	}
	
	@Override
	public String toString() {
		return "ETATrig ["
				+ (trigFeed != null ? "trigFeed=" + trigFeed + ", " : "")
				+ (trigPKey != null ? "trigPKey=" + trigPKey + ", " : "")
				+ (trigDate != null ? "trigDate=" + trigDate : "") + "]";
	}

}
