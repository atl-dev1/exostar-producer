package com.mbd.ayotta.model.po;

import java.sql.Timestamp;

public class ExoPoLineEventDets {

	private Integer poLineEventDetsId;
	private String recordId;
	private String event;
	private String dateReq;
	private String payAmount;
	private String eventDesc;
	private Timestamp createDate;

	public Integer getPoLineEventDetsId() {
		return poLineEventDetsId;
	}

	public void setPoLineEventDetsId(Integer poLineEventDetsId) {
		this.poLineEventDetsId = poLineEventDetsId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getDateReq() {
		return dateReq;
	}

	public void setDateReq(String dateReq) {
		this.dateReq = dateReq;
	}

	public String getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(String payAmount) {
		this.payAmount = payAmount;
	}

	public String getEventDesc() {
		return eventDesc;
	}

	public void setEventDesc(String eventDesc) {
		this.eventDesc = eventDesc;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}
