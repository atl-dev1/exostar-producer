package com.mbd.ayotta.model.po;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.Logger;

public class ExoPoDetails {
	
	Logger logger = Logger.getLogger(ExoPoDetails.class);
	private ExoPoLineDetails dtl;
	private ExoPoLinePartInfo api;
	private ExoPoLineRefInfoDets rfd;
	private ExoPoLineRefDescDets dsd;
	private ExoPoLineEventDets evt;
	private ExoPoLinePartyInfoDets nmd;
	private ExoPoLineScheduleInfo sch;
	
	// saving PO data in EXO_PO_LINE_DETAIL table.
	public static ExoPoLineDetails getDtl(String str, ExoPoOrderHdr exoPoOrderHdr){
		ExoPoLineDetails dtl = new ExoPoLineDetails();
		ArrayList<String> list = new ArrayList<String>();
		//dtl.setExoPoOrderHdr(exoPoOrderHdr);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				dtl.setRecordId(strVal);
				break;
			case 1:
				dtl.setLineId(strVal);
				break;
			case 2:
				dtl.setTotalLineQty(parseStrToDouble(strVal));
				break;
			case 3:
				dtl.setUom(strVal);
				break;
			case 4:
				dtl.setBasisUnitPrice(strVal);
				break;
			case 5:
				Double unp = parseStrToDouble(strVal);
				unp = unp == null ? 0 :unp;
				dtl.setUnitPrice(unp);
				break;
			case 6:
				dtl.setPriceBasisQty(parseStrToDouble(strVal));
				break;
			case 7:
				dtl.setPriceBasisUom(strVal);
				break;
			case 8:
				dtl.setBuyerPartNo(strVal);
				break;
			case 9:
				dtl.setPartNoDesc(strVal);
				break;
			case 10:
				dtl.setSupplierPartNo(strVal);
				break;
			case 11:
				dtl.setManufctPartNo(strVal);
				break;
			case 12:
				dtl.setDrawingRevNo(strVal);
				break;
			case 13:
				dtl.setEnggChangelevel(strVal);
				break;
			case 14:
				dtl.setSerialNo(strVal);
				break;
			case 15:
				dtl.setCustOrdNo(strVal);
				break;
			case 16:
				dtl.setExtSysRefNo(strVal);
				break;
			case 17:
				dtl.setCustRefNo(strVal);
				break;
			case 18:
				dtl.setExtSysRefLineNo(strVal);
				break;
			case 19:
				dtl.setOtherProdQual1(strVal);
				break;
			case 20:
				dtl.setOtherProdCode1(strVal);
				break;
			case 21:
				dtl.setOtherProdQual2(strVal);
				break;
			case 22:
				dtl.setOtherProdCode2(strVal);
				break;
			case 23:
				dtl.setPoLineSeqNo(parseStrToInteger(strVal));
				break;
			case 24:
				dtl.setContractPostion(strVal);
				break;
			case 25:
				dtl.setTransactnCatType(strVal);
				break;
			case 26:
				dtl.setCustContract(strVal);
				break;
			case 27:
				dtl.seteInvoiceAllowed(strVal);
				break;
			case 28:
				dtl.setShipAllowed(strVal);
				break;
			case 29:
				dtl.setPricingDesc(strVal);
				break;
			case 30:
				dtl.setDpas(strVal);
				break;
			case 31:
				dtl.setPrimeContract(strVal);
				break;
			case 32:
				dtl.setWorkOrdNo(strVal);
				break;
			case 33:
				dtl.setInternalOrdNo(strVal);
				break;
			case 34:
				dtl.setLineItemSumAllot(strVal);
				break;
			case 35:
				dtl.setSpl(strVal);
				break;
			case 36:
				dtl.setTaxType(strVal);
				break;
			case 37:
				dtl.setTaxAmount(parseStrToDouble(strVal));
				break;
			case 38:
				dtl.setTaxPercent(parseStrToDouble(strVal));
				break;
			case 39:
				dtl.setReasonTaxExempt(strVal);
				break;
			case 40:
				dtl.setTaxId(strVal);
				break;
			case 41:
				dtl.setTaxLocQualifier(strVal);
				break;
			case 42:
				dtl.setTaxLocation(strVal);
				break;
			case 43:
				dtl.setReserveDtl43(strVal);
				break;
			case 44:
				dtl.setReserveDtl44(strVal);
				break;
			case 45:
				dtl.setReserveDtl45(strVal);
				break;
			case 46:
				dtl.setReserveDtl46(strVal);
				break;
			case 47:
				dtl.setReserveDtl47(strVal);
				break;
			case 48:
				dtl.setReserveDtl48(strVal);
				break;
			case 49:
				dtl.setReserveDtl49(strVal);
				break;
			case 50:
				dtl.setReserveDtl50(strVal);
				break;
			case 51:
				dtl.setReserveDtl51(strVal);
				break;
			case 52:
				dtl.setReserveDtl52(strVal);
				break;
			case 53:
				dtl.setLineTotal(parseStrToDouble(strVal));
				break;
			case 54:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				dtl.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPoOrderHdr.getExoPoLineDetails().add(dtl);
		return dtl;
	}
	
	// saving PO data in EXO_PO_LINE_PART_INFO table.
	public static ExoPoLinePartInfo getApi(String str, ExoPoLineDetails exoPoLineDetails){
		ExoPoLinePartInfo api = new ExoPoLinePartInfo();
		ArrayList<String> list = new ArrayList<String>();
		//api.setExoPoLineDetails(exoPoLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				api.setRecordId(strVal);
				break;
			case 1:
				api.setAddPartInfo(strVal);
				break;
			case 2:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				api.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPoLineDetails.getExoPoLinePartInfo().add(api);
		return api;
	}
	
	// saving PO data in EXO_PO_LINE_REF_INFO_DETS table.
	public static ExoPoLineRefInfoDets getRfd(String str, ExoPoLineDetails exoPoLineDetails){
		ExoPoLineRefInfoDets rfd = new ExoPoLineRefInfoDets();
		ArrayList<String> list = new ArrayList<String>();
		//rfd.setExoPoLineDetails(exoPoLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				rfd.setRecordId(strVal);
				break;
			case 1:
				rfd.setRefCode(strVal);
				break;
			case 2:
				rfd.setRefTitle(strVal);
				break;
			case 3:
				rfd.setAddRefTitle(strVal);
				break;
			case 4:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				rfd.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPoLineDetails.getExoPoLineRefInfoDets().add(rfd);
		return rfd;
	}
	
	// saving PO data in EXO_PO_LINE_REF_DESC_DETS table.
	public static ExoPoLineRefDescDets getDsd(String str,ExoPoLineDetails exoPoLineDetails){
		ExoPoLineRefDescDets dsd = new ExoPoLineRefDescDets();
		ArrayList<String> list = new ArrayList<String>();
		//dsd.setExoPoLineDetails(exoPoLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				dsd.setRecordId(strVal);
				break;
			case 1:
				dsd.setRefDesc(strVal);
				break;
			case 2:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				dsd.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPoLineDetails.getExoPoLineRefDescDets().add(dsd);
		return dsd;
	}
	
	// saving PO data in EXO_PO_LINE_EVENT_DETS table.
	public static ExoPoLineEventDets getEvt(String str,ExoPoLineDetails exoPoLineDetails){
		ExoPoLineEventDets evt = new ExoPoLineEventDets();
		ArrayList<String> list = new ArrayList<String>();
		//evt.setExoPoLineDetails(exoPoLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				evt.setRecordId(strVal);
				break;
			case 1:
				evt.setEvent(strVal);
				break;
			case 2:
				evt.setDateReq(strVal);
				break;
			case 3:
				evt.setPayAmount(strVal);
				break;
			case 4:
				evt.setEventDesc(strVal);
				break;
			case 5:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				evt.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPoLineDetails.getExoPoLineEventDets().add(evt);
		return evt;
	}
	
	// saving PO data in EXO_PO_LINE_PARTY_DETS table. 	
	public static ExoPoLinePartyInfoDets getNmd(String str,ExoPoLineDetails exoPoLineDetails){
		ExoPoLinePartyInfoDets nmd = new ExoPoLinePartyInfoDets();
		ArrayList<String> list = new ArrayList<String>();
		//nmd.setExoPoLineDetails(exoPoLineDetails);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				nmd.setRecordId(strVal);
				break;
			case 1:
				nmd.setNameAddrQualifier(strVal);
				break;
			case 2:
				nmd.setName1(strVal);
				break;
			case 3:
				nmd.setName2(strVal);
				break;
			case 4:
				nmd.setName3(strVal);
				break;
			case 5:
				nmd.setIdCode(strVal);
				break;
			case 6:
				nmd.setAddress1(strVal);
				break;
			case 7:
				nmd.setAddress2(strVal);
				break;
			case 8:
				nmd.setAddress3(strVal);
				break;
			case 9:
				nmd.setAddress4(strVal);
				break;
			case 10:
				nmd.setCity(strVal);
				break;
			case 11:
				nmd.setStateCode(strVal);
				break;
			case 12:
				nmd.setZip(strVal);
				break;
			case 13:
				nmd.setCountryCode(strVal);
				break;
			case 14:
				nmd.setContact(strVal);
				break;
			case 15:
				nmd.setEmail(strVal);
				break;
			case 16:
				nmd.setFax(strVal);
				break;
			case 17:
				nmd.setTelephone(strVal);
				break;
			case 18:
				nmd.setReserveNMH18(strVal);
				break;
			case 19:
				nmd.setReserveNMH19(strVal);
				break;
			case 20:
				nmd.setReserveNMH20(strVal);
				break;
			case 21:
				nmd.setReserveNMH21(strVal);
				break;
			case 22:
				nmd.setReserveNMH22(strVal);
				break;
			case 23:
				nmd.setReserveNMH23(strVal);
				break;
			case 24:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				nmd.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPoLineDetails.getExoPoLinePartyInfoDets().add(nmd);
		return nmd;
	}
	
	// saving PO data in EXO_PO_LINE_SCHEDULE_INFO table.
	public static ExoPoLineScheduleInfo getSch(String str,ExoPoLineDetails exoPoLineDetails){
		ExoPoLineScheduleInfo sch = new ExoPoLineScheduleInfo();
		ArrayList<String> list = new ArrayList<String>();
		//sch.setExoPoLineDetails(exoPoLineDetails);
		list = splitString(str);
		int index = 0;
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		for (String strVal : list){
			switch (index){
			case 0:
				sch.setRecordId(strVal);
				break;
			case 1:
				sch.setScheduleId(parseStrToInteger(strVal));
				break;
			case 2:
				sch.setReqScheduleQty(parseStrToDouble(strVal));
				break;
			case 3:
				sch.setUom(strVal);
				break;
			case 4:
				sch.setReqDelDate(stringToDate(strVal));
				break;
			case 5:
				sch.setContrctualDelDate(stringToDate(strVal));
				break;
			case 6:
				sch.setOtherSchedDate(stringToDate(strVal));
				break;
			case 7:
				sch.setRoutingSeqCode(strVal);
				break;
			case 8:
				sch.setModeOfTrans(strVal);
				break;
			case 9:
				sch.setRouting(strVal);
				break;
			case 10:
				sch.setReserveSch10(strVal);
				break;
			case 11:
				sch.setReserveSch11(strVal);
				break;
			case 12:
				sch.setReserveSch12(strVal);
				break;
			case 13:
				sch.setReserveSch13(strVal);
				break;
			case 14:
				sch.setReserveSch14(strVal);
				break;
			case 15:
				sch.setCreateDate(timestamp);
				break;
			}	
			index++;
		}
		exoPoLineDetails.getExoPoLineScheduleInfo().add(sch);
		return sch;
	}
	
	
	public static ArrayList<String> splitString(String str){
		
		ArrayList<String> splitList = new ArrayList<String>();
		for(String retVal : str.split("\\|" , -1)){
			splitList.add(retVal);
		}
		return splitList;
	}
	
	public static Double parseStrToDouble(String str){
		if(!str.equals("")){
			return Double.parseDouble(str);
		}else{
			return null;
		}
		
	}
	
	public static Integer parseStrToInteger(String str){
		if(!str.equals("")){
			return Integer.parseInt(str);
		}else{
			return null;
		}
		
	}
	
	private static Date stringToDate(String str){
		Locale locale= null;
		if (locale == null)
			locale = Locale.UK;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", locale);
		try {
			if (str != null && !str.equals("")){
				return new Date(sdf.parse(str).getTime());
			}
		} catch (ParseException e) {
			 e.printStackTrace();
		}
		return null;
	}
	
	private static Date stringToTime(String str){
		Locale locale= null;
		if (locale == null)
			locale = Locale.UK;
		SimpleDateFormat sdf = new SimpleDateFormat("hhmmss", locale);
		try {
			if (str != null){
				return new Date(sdf.parse(str).getTime());
			}
		} catch (ParseException e) {
			 e.printStackTrace();
		}
		return null;
	}

	public ExoPoLineDetails getDtl() {
		return dtl;
	}

	public void setDtl(ExoPoLineDetails dtl) {
		this.dtl = dtl;
	}

	public ExoPoLineRefInfoDets getRfd() {
		return rfd;
	}

	public void setRfd(ExoPoLineRefInfoDets rfd) {
		this.rfd = rfd;
	}

	public ExoPoLineRefDescDets getDsd() {
		return dsd;
	}

	public void setDsd(ExoPoLineRefDescDets dsd) {
		this.dsd = dsd;
	}

	public ExoPoLineEventDets getEvt() {
		return evt;
	}

	public void setEvt(ExoPoLineEventDets evt) {
		this.evt = evt;
	}

	public ExoPoLinePartyInfoDets getNmd() {
		return nmd;
	}

	public void setNmd(ExoPoLinePartyInfoDets nmd) {
		this.nmd = nmd;
	}

	public ExoPoLineScheduleInfo getSch() {
		return sch;
	}

	public void setSch(ExoPoLineScheduleInfo sch) {
		this.sch = sch;
	}

	public ExoPoLinePartInfo getApi() {
		return api;
	}

	public void setApi(ExoPoLinePartInfo api) {
		this.api = api;
	}
}
