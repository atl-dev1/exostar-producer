package com.mbd.ayotta.model.po;

import java.sql.Timestamp;

public class ExoPoLinePartInfo {

	private Integer poLinePartInfoId;
	private String recordId;
	private String addPartInfo;
	private Timestamp createDate;

	public Integer getPoLinePartInfoId() {
		return poLinePartInfoId;
	}

	public void setPoLinePartInfoId(Integer poLinePartInfoId) {
		this.poLinePartInfoId = poLinePartInfoId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getAddPartInfo() {
		return addPartInfo;
	}

	public void setAddPartInfo(String addPartInfo) {
		this.addPartInfo = addPartInfo;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}
