package com.mbd.ayotta.model.po;

import java.sql.Timestamp;

public class ExoPoLineRefDescDets {

	private Integer poRefDescDetsId;
	private String recordId;
	private String refDesc;
	private Timestamp createDate;

	public Integer getPoRefDescDetsId() {
		return poRefDescDetsId;
	}

	public void setPoRefDescDetsId(Integer poRefDescDetsId) {
		this.poRefDescDetsId = poRefDescDetsId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefDesc() {
		return refDesc;
	}

	public void setRefDesc(String refDesc) {
		this.refDesc = refDesc;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}
