package com.mbd.ayotta.model.po;

import java.sql.Timestamp;

public class ExoPoAttHdr {

	private Integer poAttachHdrId;
	private String recordId;
	private String fileName;
	private String AttachLoc;
	private Timestamp createDate;

	public Integer getPoAttachHdrId() {
		return poAttachHdrId;
	}

	public void setPoAttachHdrId(Integer poAttachHdrId) {
		this.poAttachHdrId = poAttachHdrId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getAttachLoc() {
		return AttachLoc;
	}

	public void setAttachLoc(String attachLoc) {
		AttachLoc = attachLoc;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
}
