package com.mbd.ayotta.model.po;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.log4j.Logger;

import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.utils.DateUtil;


public class ExoPo extends AbstractAction{
	
	Logger logger = Logger.getLogger(ExoPo.class);
	private ExoPoTrxControl ctl;
	private ExoPoOrderHdr hdr;
	private ExoPoAttHdr att;
	private ExoPoHdrRefInfo rfh;
	private ExoPoHdrRefDesc dsh;
	private ExoPoHdrPartyInfo nmh;
	private List<ExoPoDetails> exoPoDetailsList = new ArrayList<ExoPoDetails>();
	
	
	// saving PO data in EXO_PO_TRX_CONTROL table.
	public static ExoPoTrxControl getCtl(String str){
		ExoPoTrxControl ctl = new ExoPoTrxControl();
		ArrayList<String> ctlList = new ArrayList<String>();
		ctlList = splitString(str);
		int index = 0;
		for (String strVal : ctlList){
			switch (index){
			case 0:
				ctl.setRecordId(strVal);
				break;
			case 1:
				ctl.setFuncIdCode(strVal);
				break;
			case 2:
				ctl.setTrxCtrlNo(strVal);
				break;
			case 3:
				ctl.setSenderId(strVal);
				break;
			case 4:
				ctl.setRecieverId(strVal);
				break;
			case 5:
				ctl.setFfVersionCtrlNo(strVal);
				break;
			case 6:
				ctl.setBbsId(strVal);
				break;
			case 7:
				ctl.setTrxDate(DateUtil.stringToDate(strVal));
				break;
			case 8:
				ctl.setTrxTime(strVal);
				break;
			case 9:
				ctl.setReserveCtl9(strVal);
				break;
			case 10:
				ctl.setReserveCtl10(strVal);
				break;
			case 11:
				ctl.setReserveCtl11(strVal);
				break;
			case 12:
				ctl.setReserveCtl12(strVal);
				break;
			case 13:
				ctl.setReserveCtl13(strVal);
				break;
			case 14:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				ctl.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		return ctl;
	}
	
	// saving PO data in EXO_PO_HEADER table.
	public static ExoPoOrderHdr getHdr(String str, ExoPoTrxControl exoPoTrxCtrl){
		ExoPoOrderHdr hdr = new ExoPoOrderHdr();
		ArrayList<String> list = new ArrayList<String>();
		list = splitString(str);
		//hdr.setExoPoTrxControl(exoPoTrxCtrl);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				hdr.setRecordId(strVal);
				break;
			case 1:
				hdr.setPurpose(strVal);
				break;
			case 2:
				hdr.setBuyerAccount(strVal);
				break;
			case 3:
				hdr.setPoType(strVal);
				break;
			case 4:
				hdr.setPoNo(strVal);
				break;
			case 5:
				hdr.setReleaseNo(strVal);
				break;
			case 6:
				hdr.setOrderDate(DateUtil.stringToDate(strVal));
				break;
			case 7:
				hdr.setRequestedResponse(strVal);
				break;
			case 8:
				hdr.setShipPayMethod(strVal);
				break;
			case 9:
				hdr.setFobLocation(strVal);
				break;
			case 10:
				hdr.setFobDesc(strVal);
				break;
			case 11:
				hdr.setPaymentTerms(strVal);
				break;
			case 12:
				hdr.setDiscountPercent(parseStrToDouble(strVal));
				break;
			case 13:
				hdr.setDiscountDaysDue(parseStrToInteger(strVal));
				break;
			case 14:
				hdr.setNetDaysDue(parseStrToInteger(strVal));
				break;
			case 15:
				hdr.setContractType(strVal);
				break;
			case 16:
				hdr.setContractEffDate(DateUtil.stringToDate(strVal));
				break;
			case 17:
				hdr.setContractExpDate(DateUtil.stringToDate(strVal));
				break;
			case 18:
				hdr.setTermExpire(DateUtil.stringToDate(strVal));
				break;
			case 19:
				hdr.setCurrencyParty(strVal);
				break;
			case 20:
				hdr.setCurrencyCode(strVal);
				break;
			case 21:
				hdr.setForeignCurrType(strVal);
				break;
			case 22:
				hdr.setForeignCurrRate(strVal);
				break;
			case 23:
				hdr.setOriginatingCompId(strVal);
				break;
			case 24:
				hdr.setPoReference(strVal);
				break;
			case 25:
				hdr.setStrategicAggNo(strVal);
				break;
			case 26:
				hdr.setRoutingseqCode(strVal);
				break;
			case 27:
				hdr.setModeOfTrans(strVal);
				break;
			case 28:
				hdr.setRouting(strVal);
				break;
			case 29:
				hdr.setBlanketAuthLimit(parseStrToDouble(strVal));
				break;
			case 30:
				hdr.setTotalPoSumAllot(strVal);
				break;
			case 31:
				hdr.setVendorOrderNo(strVal);
				break;
			case 32:
				hdr.setNoOfLines(parseStrToInteger(strVal));
				break;
			case 33:
				hdr.setTotalOrdAmount(parseStrToDouble(strVal));
				break;
			case 34:
				hdr.setTotalOrdQty(parseStrToDouble(strVal));
				break;
			case 35:
				hdr.setTermDesc(strVal);
				break;
			case 36:
				hdr.setReserveHdr36(strVal);
				break;
			case 37:
				hdr.setReserveHdr37(strVal);
				break;
			case 38:
				hdr.setReserveHdr38(strVal);
				break;
			case 39:
				hdr.setReserveHdr39(strVal);
				break;
			case 40:
				hdr.setReserveHdr40(strVal);
				break;
			case 41:
				hdr.setReserveHdr41(strVal);
				break;
			case 42:
				hdr.setReserveHdr42(strVal);
				break;
			case 43:
				hdr.setReserveHdr43(strVal);
				break;
			case 44:
				hdr.setReserveHdr44(strVal);
				break;
			case 45:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				hdr.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPoTrxCtrl.getExoPoOrderHdrs().add(hdr);
		return hdr;
	}
	
	// saving PO data in EXO_PO_HEADER_ATT table.
	public static ExoPoAttHdr getAtt(String str, ExoPoOrderHdr exoPoOrderHdr){
		ExoPoAttHdr att = new ExoPoAttHdr();
		ArrayList<String> list = new ArrayList<String>();
		//att.setExoPoOrderHdr(exoPoOrderHdr);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				att.setRecordId(strVal);
				break;
			case 1:
				att.setFileName(strVal);
				break;
			case 2:
				att.setAttachLoc(strVal);
				break;
			case 3:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				att.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPoOrderHdr.getExoPoAttHdr().add(att);
		return att;
	}
	
	// saving PO data in EXO_PO_HEADER_REF table.
	public static ExoPoHdrRefInfo getRfh(String str, ExoPoOrderHdr exoPoOrderHdr){
		ExoPoHdrRefInfo rfh = new ExoPoHdrRefInfo();
		ArrayList<String> list = new ArrayList<String>();
		//rfh.setExoPoOrderHdr(exoPoOrderHdr);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				rfh.setRecordId(strVal);
				break;
			case 1:
				rfh.setRefCode(strVal);
				break;
			case 2:
				rfh.setRefTitle(strVal);
				break;
			case 3:
				rfh.setAddRefTitle(strVal);
				break;
			case 4:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				rfh.setCreateDate(timestamp);
				break;

			}
			index++;
		}
		exoPoOrderHdr.getExoPoHdrRefInfo().add(rfh);
		return rfh;
	}
	
	// saving PO data in EXO_PO_HEADER_REF_DESC table.
	public static ExoPoHdrRefDesc getDsh(String str, ExoPoHdrRefInfo exoPoHdrRefInfo){
		ExoPoHdrRefDesc dsh = new ExoPoHdrRefDesc();
		ArrayList<String> list = new ArrayList<String>();
		//dsh.setExoPoHdrRefInfo(exoPoHdrRefInfo);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				dsh.setRecordId(strVal);
				break;
			case 1:
				dsh.setRefDesc(strVal);
				break;
			case 2:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				dsh.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPoHdrRefInfo.getExoPoHdrRefDesc().add(dsh);
		return dsh;
	}
	
	// saving PO data in EXO_PO_HEADER_PARTY_INFO table.
	public static ExoPoHdrPartyInfo getNmh(String str, ExoPoOrderHdr exoPoOrderHdr){
		ExoPoHdrPartyInfo nmh = new ExoPoHdrPartyInfo();
		ArrayList<String> list = new ArrayList<String>();
		//nmh.setExoPoOrderHdr(exoPoOrderHdr);
		list = splitString(str);
		int index = 0;
		for (String strVal : list){
			switch (index){
			case 0:
				nmh.setRecordId(strVal);
				break;
			case 1:
				nmh.setNameAddrQualifier(strVal);
				break;
			case 2:
				nmh.setName1(strVal);
				break;
			case 3:
				nmh.setName2(strVal);
				break;
			case 4:
				nmh.setName3(strVal);
				break;
			case 5:
				nmh.setIdCode(strVal);
				break;
			case 6:
				nmh.setAddress1(strVal);
				break;
			case 7:
				nmh.setAddress2(strVal);
				break;
			case 8:
				nmh.setAddress3(strVal);
				break;
			case 9:
				nmh.setAddress4(strVal);
				break;
			case 10:
				nmh.setCity(strVal);
				break;
			case 11:
				nmh.setStateCode(strVal);
				break;
			case 12:
				nmh.setZip(strVal);
				break;
			case 13:
				nmh.setCountryCode(strVal);
				break;
			case 14:
				nmh.setContact(strVal);
				break;
			case 15:
				nmh.setEmail(strVal);
				break;
			case 16:
				nmh.setFax(strVal);
				break;
			case 17:
				nmh.setTelephone(strVal);
				break;
			case 18:
				nmh.setReserveNMH18(strVal);
				break;
			case 19:
				nmh.setReserveNMH19(strVal);
				break;
			case 20:
				nmh.setReserveNMH20(strVal);
				break;
			case 21:
				nmh.setReserveNMH21(strVal);
				break;
			case 22:
				nmh.setReserveNMH22(strVal);
				break;
			case 23:
				nmh.setReserveNMH23(strVal);
				break;
			case 24:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				nmh.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoPoOrderHdr.getExoPoHdrPartyInfo().add(nmh);
		return nmh;
	}
	
	public ExoPoTrxControl getCtl() {
		return ctl;
	}
	
	public void setCtl(ExoPoTrxControl ctl) {
		this.ctl = ctl;
	}

	public ExoPoOrderHdr getHdr() {
		return hdr;
	}

	public void setHdr(ExoPoOrderHdr hdr) {
		this.hdr = hdr;
	}

	public ExoPoAttHdr getAtt() {
		return att;
	}

	public void setAtt(ExoPoAttHdr att) {
		this.att = att;
	}

	public ExoPoHdrRefInfo getRfh() {
		return rfh;
	}

	public void setRfh(ExoPoHdrRefInfo rfh) {
		this.rfh = rfh;
	}

	public ExoPoHdrRefDesc getDsh() {
		return dsh;
	}

	public void setDsh(ExoPoHdrRefDesc dsh) {
		this.dsh = dsh;
	}

	public ExoPoHdrPartyInfo getNmh() {
		return nmh;
	}

	public void setNmh(ExoPoHdrPartyInfo nmh) {
		this.nmh = nmh;
	}

	public List<ExoPoDetails> getExoPoDetailsList() {
		return exoPoDetailsList;
	}

	public void setExoPoDetailsList(List<ExoPoDetails> exoPoDetailsList) {
		this.exoPoDetailsList = exoPoDetailsList;
	}
}
