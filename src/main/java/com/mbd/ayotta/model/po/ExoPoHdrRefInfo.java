package com.mbd.ayotta.model.po;

import java.sql.Timestamp;
import java.util.ArrayList;

public class ExoPoHdrRefInfo {

	private Integer poRefInfoHdrId;
	private String recordId;
	private String refCode;
	private String refTitle;
	private String addRefTitle;
	private Timestamp createDate;
    private ArrayList<ExoPoHdrRefDesc> exoPoHdrRefDesc = new ArrayList<ExoPoHdrRefDesc>(); 
    
	public Integer getPoRefInfoHdrId() {
		return poRefInfoHdrId;
	}

	public void setPoRefInfoHdrId(Integer poRefInfoHdrId) {
		this.poRefInfoHdrId = poRefInfoHdrId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public String getRefTitle() {
		return refTitle;
	}

	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}

	public String getAddRefTitle() {
		return addRefTitle;
	}

	public void setAddRefTitle(String addRefTitle) {
		this.addRefTitle = addRefTitle;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public ArrayList<ExoPoHdrRefDesc> getExoPoHdrRefDesc() {
		return exoPoHdrRefDesc;
	}

	public void setExoPoHdrRefDesc(ArrayList<ExoPoHdrRefDesc> exoPoHdrRefDesc) {
		this.exoPoHdrRefDesc = exoPoHdrRefDesc;
	}

}
