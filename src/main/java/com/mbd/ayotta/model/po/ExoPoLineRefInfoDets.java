package com.mbd.ayotta.model.po;

import java.sql.Timestamp;

public class ExoPoLineRefInfoDets {

	private Integer poRefInfoDetsId;
	private String recordId;
	private String refCode;
	private String refTitle;
	private String addRefTitle;
	private Timestamp createDate;

	public Integer getPoRefInfoDetsId() {
		return poRefInfoDetsId;
	}

	public void setPoRefInfoDetsId(Integer poRefInfoDetsId) {
		this.poRefInfoDetsId = poRefInfoDetsId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public String getRefTitle() {
		return refTitle;
	}

	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}

	public String getAddRefTitle() {
		return addRefTitle;
	}

	public void setAddRefTitle(String addRefTitle) {
		this.addRefTitle = addRefTitle;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}
