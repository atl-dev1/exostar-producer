package com.mbd.ayotta.model.po;

import java.sql.Timestamp;

import org.springframework.data.annotation.Id;

public class PoMessages {

	@Id
	private String elasticSearchRef;
	private String messageStatus;
	private Timestamp elkCreationDate;
	private Timestamp elkModifiedDate;
	private String fileName;
	private String orderType;//PO|POC|POCR|FA.
	private ExoPoTrxControl trxControl;
	private ExoPayLoad exoPayLoad;
	private String vendorCode;
	private String toDunsName;
	private String poType;

	public String getElasticSearchRef() {
		return elasticSearchRef;
	}

	public void setElasticSearchRef(String elasticSearchRef) {
		this.elasticSearchRef = elasticSearchRef;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public Timestamp getElkCreationDate() {
		return elkCreationDate;
	}

	public void setElkCreationDate(Timestamp elkCreationDate) {
		this.elkCreationDate = elkCreationDate;
	}

	public Timestamp getElkModifiedDate() {
		return elkModifiedDate;
	}

	public void setElkModifiedDate(Timestamp elkModifiedDate) {
		this.elkModifiedDate = elkModifiedDate;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public ExoPoTrxControl getTrxControl() {
		return trxControl;
	}

	public void setTrxControl(ExoPoTrxControl trxControl) {
		this.trxControl = trxControl;
	}

	public ExoPayLoad getExoPayLoad() {
		return exoPayLoad;
	}

	public void setExoPayLoad(ExoPayLoad exoPayLoad) {
		this.exoPayLoad = exoPayLoad;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getToDunsName() {
		return toDunsName;
	}

	public void setToDunsName(String toDunsName) {
		this.toDunsName = toDunsName;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}
}
