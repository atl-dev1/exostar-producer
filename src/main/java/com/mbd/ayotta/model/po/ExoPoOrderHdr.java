package com.mbd.ayotta.model.po;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class ExoPoOrderHdr {

	private Integer poOrderHdrId;
	private String recordId;
	private String purpose;
	private String buyerAccount;
	private String poType;
	private String poNo;
	private String releaseNo;
	private Date orderDate;
	private String requestedResponse;
	private String shipPayMethod;
	private String fobLocation;
	private String fobDesc;
	private String paymentTerms;
	private Double discountPercent;
	private Integer discountDaysDue;
	private Integer netDaysDue;
	private String contractType;
	private Date contractEffDate;
	private Date contractExpDate;
	private Date termExpire;
	private String currencyParty;
	private String currencyCode;
	private String foreignCurrType;
	private String foreignCurrRate;
	private String originatingCompId;
	private String poReference;
	private String strategicAggNo;
	private String routingseqCode;
	private String modeOfTrans;
	private String routing;
	private Double blanketAuthLimit;
	private String totalPoSumAllot;
	private String vendorOrderNo;
	private Integer noOfLines;
	private Double totalOrdAmount;
	private Double totalOrdQty;
	private String termDesc;
	private String reserveHdr36;
	private String reserveHdr37;
	private String reserveHdr38;
	private String reserveHdr39;
	private String reserveHdr40;
	private String reserveHdr41;
	private String reserveHdr42;
	private String reserveHdr43;
	private String reserveHdr44;
	private Timestamp createDate;
	
    private ArrayList<ExoPoAttHdr> exoPoAttHdr = new ArrayList<ExoPoAttHdr>(); 
	
    private ArrayList<ExoPoHdrPartyInfo> exoPoHdrPartyInfo = new ArrayList<ExoPoHdrPartyInfo>();
	
    private ArrayList<ExoPoHdrRefInfo> exoPoHdrRefInfo = new ArrayList<ExoPoHdrRefInfo>();
	
    private ArrayList<ExoPoLineDetails> exoPoLineDetails = new ArrayList<ExoPoLineDetails>();

	public Integer getPoOrderHdrId() {
		return poOrderHdrId;
	}

	public void setPoOrderHdrId(Integer poOrderHdrId) {
		this.poOrderHdrId = poOrderHdrId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getBuyerAccount() {
		return buyerAccount;
	}

	public void setBuyerAccount(String buyerAccount) {
		this.buyerAccount = buyerAccount;
	}

	public String getPoType() {
		return poType;
	}

	public void setPoType(String poType) {
		this.poType = poType;
	}

	public String getPoNo() {
		return poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public String getReleaseNo() {
		return releaseNo;
	}

	public void setReleaseNo(String releaseNo) {
		this.releaseNo = releaseNo;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getRequestedResponse() {
		return requestedResponse;
	}

	public void setRequestedResponse(String requestedResponse) {
		this.requestedResponse = requestedResponse;
	}

	public String getShipPayMethod() {
		return shipPayMethod;
	}

	public void setShipPayMethod(String shipPayMethod) {
		this.shipPayMethod = shipPayMethod;
	}

	public String getFobLocation() {
		return fobLocation;
	}

	public void setFobLocation(String fobLocation) {
		this.fobLocation = fobLocation;
	}

	public String getFobDesc() {
		return fobDesc;
	}

	public void setFobDesc(String fobDesc) {
		this.fobDesc = fobDesc;
	}

	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public Double getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(Double discountPercent) {
		this.discountPercent = discountPercent;
	}

	public Integer getDiscountDaysDue() {
		return discountDaysDue;
	}

	public void setDiscountDaysDue(Integer discountDaysDue) {
		this.discountDaysDue = discountDaysDue;
	}

	public Integer getNetDaysDue() {
		return netDaysDue;
	}

	public void setNetDaysDue(Integer netDaysDue) {
		this.netDaysDue = netDaysDue;
	}

	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	public Date getContractEffDate() {
		return contractEffDate;
	}

	public void setContractEffDate(Date contractEffDate) {
		this.contractEffDate = contractEffDate;
	}

	public Date getContractExpDate() {
		return contractExpDate;
	}

	public void setContractExpDate(Date contractExpDate) {
		this.contractExpDate = contractExpDate;
	}

	public Date getTermExpire() {
		return termExpire;
	}

	public void setTermExpire(Date termExpire) {
		this.termExpire = termExpire;
	}

	public String getCurrencyParty() {
		return currencyParty;
	}

	public void setCurrencyParty(String currencyParty) {
		this.currencyParty = currencyParty;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getForeignCurrType() {
		return foreignCurrType;
	}

	public void setForeignCurrType(String foreignCurrType) {
		this.foreignCurrType = foreignCurrType;
	}

	public String getForeignCurrRate() {
		return foreignCurrRate;
	}

	public void setForeignCurrRate(String foreignCurrRate) {
		this.foreignCurrRate = foreignCurrRate;
	}

	public String getOriginatingCompId() {
		return originatingCompId;
	}

	public void setOriginatingCompId(String originatingCompId) {
		this.originatingCompId = originatingCompId;
	}

	public String getPoReference() {
		return poReference;
	}

	public void setPoReference(String poReference) {
		this.poReference = poReference;
	}

	public String getStrategicAggNo() {
		return strategicAggNo;
	}

	public void setStrategicAggNo(String strategicAggNo) {
		this.strategicAggNo = strategicAggNo;
	}

	public String getRoutingseqCode() {
		return routingseqCode;
	}

	public void setRoutingseqCode(String routingseqCode) {
		this.routingseqCode = routingseqCode;
	}

	public String getModeOfTrans() {
		return modeOfTrans;
	}

	public void setModeOfTrans(String modeOfTrans) {
		this.modeOfTrans = modeOfTrans;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public Double getBlanketAuthLimit() {
		return blanketAuthLimit;
	}

	public void setBlanketAuthLimit(Double blanketAuthLimit) {
		this.blanketAuthLimit = blanketAuthLimit;
	}

	public String getTotalPoSumAllot() {
		return totalPoSumAllot;
	}

	public void setTotalPoSumAllot(String totalPoSumAllot) {
		this.totalPoSumAllot = totalPoSumAllot;
	}

	public String getVendorOrderNo() {
		return vendorOrderNo;
	}

	public void setVendorOrderNo(String vendorOrderNo) {
		this.vendorOrderNo = vendorOrderNo;
	}

	public Integer getNoOfLines() {
		return noOfLines;
	}

	public void setNoOfLines(Integer noOfLines) {
		this.noOfLines = noOfLines;
	}

	public Double getTotalOrdAmount() {
		return totalOrdAmount;
	}

	public void setTotalOrdAmount(Double totalOrdAmount) {
		this.totalOrdAmount = totalOrdAmount;
	}

	public Double getTotalOrdQty() {
		return totalOrdQty;
	}

	public void setTotalOrdQty(Double totalOrdQty) {
		this.totalOrdQty = totalOrdQty;
	}

	public String getTermDesc() {
		return termDesc;
	}

	public void setTermDesc(String termDesc) {
		this.termDesc = termDesc;
	}

	public String getReserveHdr36() {
		return reserveHdr36;
	}

	public void setReserveHdr36(String reserveHdr36) {
		this.reserveHdr36 = reserveHdr36;
	}

	public String getReserveHdr37() {
		return reserveHdr37;
	}

	public void setReserveHdr37(String reserveHdr37) {
		this.reserveHdr37 = reserveHdr37;
	}

	public String getReserveHdr38() {
		return reserveHdr38;
	}

	public void setReserveHdr38(String reserveHdr38) {
		this.reserveHdr38 = reserveHdr38;
	}

	public String getReserveHdr39() {
		return reserveHdr39;
	}

	public void setReserveHdr39(String reserveHdr39) {
		this.reserveHdr39 = reserveHdr39;
	}

	public String getReserveHdr40() {
		return reserveHdr40;
	}

	public void setReserveHdr40(String reserveHdr40) {
		this.reserveHdr40 = reserveHdr40;
	}

	public String getReserveHdr41() {
		return reserveHdr41;
	}

	public void setReserveHdr41(String reserveHdr41) {
		this.reserveHdr41 = reserveHdr41;
	}

	public String getReserveHdr42() {
		return reserveHdr42;
	}

	public void setReserveHdr42(String reserveHdr42) {
		this.reserveHdr42 = reserveHdr42;
	}

	public String getReserveHdr43() {
		return reserveHdr43;
	}

	public void setReserveHdr43(String reserveHdr43) {
		this.reserveHdr43 = reserveHdr43;
	}

	public String getReserveHdr44() {
		return reserveHdr44;
	}

	public void setReserveHdr44(String reserveHdr44) {
		this.reserveHdr44 = reserveHdr44;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public ArrayList<ExoPoAttHdr> getExoPoAttHdr() {
		return exoPoAttHdr;
	}

	public void setExoPoAttHdr(ArrayList<ExoPoAttHdr> exoPoAttHdr) {
		this.exoPoAttHdr = exoPoAttHdr;
	}

	public ArrayList<ExoPoHdrPartyInfo> getExoPoHdrPartyInfo() {
		return exoPoHdrPartyInfo;
	}

	public void setExoPoHdrPartyInfo(ArrayList<ExoPoHdrPartyInfo> exoPoHdrPartyInfo) {
		this.exoPoHdrPartyInfo = exoPoHdrPartyInfo;
	}

	public ArrayList<ExoPoHdrRefInfo> getExoPoHdrRefInfo() {
		return exoPoHdrRefInfo;
	}

	public void setExoPoHdrRefInfo(ArrayList<ExoPoHdrRefInfo> exoPoHdrRefInfo) {
		this.exoPoHdrRefInfo = exoPoHdrRefInfo;
	}

	public ArrayList<ExoPoLineDetails> getExoPoLineDetails() {
		return exoPoLineDetails;
	}

	public void setExoPoLineDetails(ArrayList<ExoPoLineDetails> exoPoLineDetails) {
		this.exoPoLineDetails = exoPoLineDetails;
	}

}
