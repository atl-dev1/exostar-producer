package com.mbd.ayotta.model.po;

public class ExoPayLoad {

	private Integer id;
	private Integer poHdrId;
	private String cpo;
	private String orderData;
	private String orderType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPoHdrId() {
		return poHdrId;
	}

	public void setPoHdrId(Integer poHdrId) {
		this.poHdrId = poHdrId;
	}

	public String getCpo() {
		return cpo;
	}

	public void setCpo(String cpo) {
		this.cpo = cpo;
	}

	public String getOrderData() {
		return orderData;
	}

	public void setOrderData(String orderData) {
		this.orderData = orderData;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

}
