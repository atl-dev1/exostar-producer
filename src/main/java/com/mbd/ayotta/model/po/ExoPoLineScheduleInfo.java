package com.mbd.ayotta.model.po;

import java.sql.Timestamp;
import java.util.Date;

public class ExoPoLineScheduleInfo {

	private Integer poLineScheduleId;
	private String recordId;
	private Integer scheduleId;
	private Double reqScheduleQty;
	private String uom;
	private Date reqDelDate;
	private Date contrctualDelDate;
	private Date otherSchedDate;
	private String routingSeqCode;
	private String modeOfTrans;
	private String routing;
	private String reserveSch10;
	private String reserveSch11;
	private String reserveSch12;
	private String reserveSch13;
	private String reserveSch14;
	private Timestamp createDate;

	public Integer getPoLineScheduleId() {
		return poLineScheduleId;
	}

	public void setPoLineScheduleId(Integer poLineScheduleId) {
		this.poLineScheduleId = poLineScheduleId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Double getReqScheduleQty() {
		return reqScheduleQty;
	}

	public void setReqScheduleQty(Double reqScheduleQty) {
		this.reqScheduleQty = reqScheduleQty;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Date getReqDelDate() {
		return reqDelDate;
	}

	public void setReqDelDate(Date reqDelDate) {
		this.reqDelDate = reqDelDate;
	}

	public Date getContrctualDelDate() {
		return contrctualDelDate;
	}

	public void setContrctualDelDate(Date contrctualDelDate) {
		this.contrctualDelDate = contrctualDelDate;
	}

	public Date getOtherSchedDate() {
		return otherSchedDate;
	}

	public void setOtherSchedDate(Date otherSchedDate) {
		this.otherSchedDate = otherSchedDate;
	}

	public String getRoutingSeqCode() {
		return routingSeqCode;
	}

	public void setRoutingSeqCode(String routingSeqCode) {
		this.routingSeqCode = routingSeqCode;
	}

	public String getModeOfTrans() {
		return modeOfTrans;
	}

	public void setModeOfTrans(String modeOfTrans) {
		this.modeOfTrans = modeOfTrans;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public String getReserveSch10() {
		return reserveSch10;
	}

	public void setReserveSch10(String reserveSch10) {
		this.reserveSch10 = reserveSch10;
	}

	public String getReserveSch11() {
		return reserveSch11;
	}

	public void setReserveSch11(String reserveSch11) {
		this.reserveSch11 = reserveSch11;
	}

	public String getReserveSch12() {
		return reserveSch12;
	}

	public void setReserveSch12(String reserveSch12) {
		this.reserveSch12 = reserveSch12;
	}

	public String getReserveSch13() {
		return reserveSch13;
	}

	public void setReserveSch13(String reserveSch13) {
		this.reserveSch13 = reserveSch13;
	}

	public String getReserveSch14() {
		return reserveSch14;
	}

	public void setReserveSch14(String reserveSch14) {
		this.reserveSch14 = reserveSch14;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

}
