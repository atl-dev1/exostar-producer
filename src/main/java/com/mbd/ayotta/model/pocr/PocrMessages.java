package com.mbd.ayotta.model.pocr;

import java.sql.Timestamp;

import org.springframework.data.annotation.Id;

import com.mbd.ayotta.model.po.ExoPayLoad;

public class PocrMessages {

	@Id
	private String elasticSearchRef;
	private String messageStatus;
	private Timestamp elkCreationDate;
	private Timestamp elkModifiedDate;
	private String fileName;
	private String orderType; //PO|POC|POCR|FA.
	private PocrTrxCtrl trxControl;

	public String getElasticSearchRef() {
		return elasticSearchRef;
	}

	public void setElasticSearchRef(String elasticSearchRef) {
		this.elasticSearchRef = elasticSearchRef;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public Timestamp getElkCreationDate() {
		return elkCreationDate;
	}

	public void setElkCreationDate(Timestamp elkCreationDate) {
		this.elkCreationDate = elkCreationDate;
	}

	public Timestamp getElkModifiedDate() {
		return elkModifiedDate;
	}

	public void setElkModifiedDate(Timestamp elkModifiedDate) {
		this.elkModifiedDate = elkModifiedDate;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public PocrTrxCtrl getTrxControl() {
		return trxControl;
	}

	public void setTrxControl(PocrTrxCtrl trxControl) {
		this.trxControl = trxControl;
	}
}
