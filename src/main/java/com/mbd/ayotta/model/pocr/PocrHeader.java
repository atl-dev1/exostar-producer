package com.mbd.ayotta.model.pocr;

import java.util.ArrayList;
import java.util.Date;

import com.mbd.ayotta.model.po.ExoPoLineDetails;

public class PocrHeader {

	private String recordId;
	private String purpose;
	private String ackType;
	private String poNumber;
	private Integer chgOrdSeqNo;

	private Date poDate;
	private String refId;
	private Date pocrIssueDate;
	private Date pocDate;
	private String poOrdTypeCode;

	private String transTypeCode;
	private String buyerMpid;
	private String sellerMpid;
	private String supplierCName;
	private String supplierCEmail;

	private String reserveHdr15;
	private String reserveHdr16;
	private String reserveHdr17;
	private String reserveHdr18;
	private String reserveHdr19;

	private String reserveHdr20;
	private String reserveHdr21;
	private String reserveHdr22;
	private String reserveHdr23;
	private String reserveHdr24;
	private String reserveHdr25;
	private ArrayList<PocrRefInfoHdr> pocrRefInfoHdr = new ArrayList<PocrRefInfoHdr>();
	private ArrayList<PocrDetail> pocrDetail = new ArrayList<PocrDetail>();

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getAckType() {
		return ackType;
	}

	public void setAckType(String ackType) {
		this.ackType = ackType;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public Integer getChgOrdSeqNo() {
		return chgOrdSeqNo;
	}

	public void setChgOrdSeqNo(Integer chgOrdSeqNo) {
		this.chgOrdSeqNo = chgOrdSeqNo;
	}

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public Date getPocrIssueDate() {
		return pocrIssueDate;
	}

	public void setPocrIssueDate(Date pocrIssueDate) {
		this.pocrIssueDate = pocrIssueDate;
	}

	public Date getPocDate() {
		return pocDate;
	}

	public void setPocDate(Date pocDate) {
		this.pocDate = pocDate;
	}

	public String getPoOrdTypeCode() {
		return poOrdTypeCode;
	}

	public void setPoOrdTypeCode(String poOrdTypeCode) {
		this.poOrdTypeCode = poOrdTypeCode;
	}

	public String getTransTypeCode() {
		return transTypeCode;
	}

	public void setTransTypeCode(String transTypeCode) {
		this.transTypeCode = transTypeCode;
	}

	public String getBuyerMpid() {
		return buyerMpid;
	}

	public void setBuyerMpid(String buyerMpid) {
		this.buyerMpid = buyerMpid;
	}

	public String getSellerMpid() {
		return sellerMpid;
	}

	public void setSellerMpid(String sellerMpid) {
		this.sellerMpid = sellerMpid;
	}

	public String getSupplierCName() {
		return supplierCName;
	}

	public void setSupplierCName(String supplierCName) {
		this.supplierCName = supplierCName;
	}

	public String getSupplierCEmail() {
		return supplierCEmail;
	}

	public void setSupplierCEmail(String supplierCEmail) {
		this.supplierCEmail = supplierCEmail;
	}

	public String getReserveHdr15() {
		return reserveHdr15;
	}

	public void setReserveHdr15(String reserveHdr15) {
		this.reserveHdr15 = reserveHdr15;
	}

	public String getReserveHdr16() {
		return reserveHdr16;
	}

	public void setReserveHdr16(String reserveHdr16) {
		this.reserveHdr16 = reserveHdr16;
	}

	public String getReserveHdr17() {
		return reserveHdr17;
	}

	public void setReserveHdr17(String reserveHdr17) {
		this.reserveHdr17 = reserveHdr17;
	}

	public String getReserveHdr18() {
		return reserveHdr18;
	}

	public void setReserveHdr18(String reserveHdr18) {
		this.reserveHdr18 = reserveHdr18;
	}

	public String getReserveHdr19() {
		return reserveHdr19;
	}

	public void setReserveHdr19(String reserveHdr19) {
		this.reserveHdr19 = reserveHdr19;
	}

	public String getReserveHdr20() {
		return reserveHdr20;
	}

	public void setReserveHdr20(String reserveHdr20) {
		this.reserveHdr20 = reserveHdr20;
	}

	public String getReserveHdr21() {
		return reserveHdr21;
	}

	public void setReserveHdr21(String reserveHdr21) {
		this.reserveHdr21 = reserveHdr21;
	}

	public String getReserveHdr22() {
		return reserveHdr22;
	}

	public void setReserveHdr22(String reserveHdr22) {
		this.reserveHdr22 = reserveHdr22;
	}

	public String getReserveHdr23() {
		return reserveHdr23;
	}

	public void setReserveHdr23(String reserveHdr23) {
		this.reserveHdr23 = reserveHdr23;
	}

	public String getReserveHdr24() {
		return reserveHdr24;
	}

	public void setReserveHdr24(String reserveHdr24) {
		this.reserveHdr24 = reserveHdr24;
	}

	public String getReserveHdr25() {
		return reserveHdr25;
	}

	public void setReserveHdr25(String reserveHdr25) {
		this.reserveHdr25 = reserveHdr25;
	}

	public ArrayList<PocrDetail> getPocrDetail() {
		return pocrDetail;
	}

	public void setPocrDetail(ArrayList<PocrDetail> pocrDetail) {
		this.pocrDetail = pocrDetail;
	}

	public ArrayList<PocrRefInfoHdr> getPocrRefInfoHdr() {
		return pocrRefInfoHdr;
	}

	public void setPocrRefInfoHdr(ArrayList<PocrRefInfoHdr> pocrRefInfoHdr) {
		this.pocrRefInfoHdr = pocrRefInfoHdr;
	}
}
