package com.mbd.ayotta.model.pocr;

import java.util.ArrayList;

public class PocrRefInfoHdr {

	private String recordId;
	private String refCode;
	private String refTitle;
	private String addRefTitle;
	private ArrayList<PocrRefDescHdr> pocrRefDescHdr = new ArrayList<PocrRefDescHdr>(); 

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefCode() {
		return refCode;
	}

	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}

	public String getRefTitle() {
		return refTitle;
	}

	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}

	public String getAddRefTitle() {
		return addRefTitle;
	}

	public void setAddRefTitle(String addRefTitle) {
		this.addRefTitle = addRefTitle;
	}

	public ArrayList<PocrRefDescHdr> getPocrRefDescHdr() {
		return pocrRefDescHdr;
	}

	public void setPocrRefDescHdr(ArrayList<PocrRefDescHdr> pocrRefDescHdr) {
		this.pocrRefDescHdr = pocrRefDescHdr;
	}
}
