package com.mbd.ayotta.model.pocr;

public class PocrDetail {

	private String recordId;
	private String pocLineId;
	private String lineItemStCode;
	private String scheduleId;
	private Double reqSchQty;
	private String scheduleUom;

	private String reserveDtl6;
	private String reserveDtl7;
	private String reserveDtl8;
	private String reserveDtl9;
	private String reserveDtl10;

	private String reserveDtl11;
	private String reserveDtl12;
	private String reserveDtl13;
	private String reserveDtl14;
	private String reserveDtl15;
	private String reserveDtl16;

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getPocLineId() {
		return pocLineId;
	}

	public void setPocLineId(String pocLineId) {
		this.pocLineId = pocLineId;
	}

	public String getLineItemStCode() {
		return lineItemStCode;
	}

	public void setLineItemStCode(String lineItemStCode) {
		this.lineItemStCode = lineItemStCode;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Double getReqSchQty() {
		return reqSchQty;
	}

	public void setReqSchQty(Double reqSchQty) {
		this.reqSchQty = reqSchQty;
	}

	public String getScheduleUom() {
		return scheduleUom;
	}

	public void setScheduleUom(String scheduleUom) {
		this.scheduleUom = scheduleUom;
	}

	public String getReserveDtl6() {
		return reserveDtl6;
	}

	public void setReserveDtl6(String reserveDtl6) {
		this.reserveDtl6 = reserveDtl6;
	}

	public String getReserveDtl7() {
		return reserveDtl7;
	}

	public void setReserveDtl7(String reserveDtl7) {
		this.reserveDtl7 = reserveDtl7;
	}

	public String getReserveDtl8() {
		return reserveDtl8;
	}

	public void setReserveDtl8(String reserveDtl8) {
		this.reserveDtl8 = reserveDtl8;
	}

	public String getReserveDtl9() {
		return reserveDtl9;
	}

	public void setReserveDtl9(String reserveDtl9) {
		this.reserveDtl9 = reserveDtl9;
	}

	public String getReserveDtl10() {
		return reserveDtl10;
	}

	public void setReserveDtl10(String reserveDtl10) {
		this.reserveDtl10 = reserveDtl10;
	}

	public String getReserveDtl11() {
		return reserveDtl11;
	}

	public void setReserveDtl11(String reserveDtl11) {
		this.reserveDtl11 = reserveDtl11;
	}

	public String getReserveDtl12() {
		return reserveDtl12;
	}

	public void setReserveDtl12(String reserveDtl12) {
		this.reserveDtl12 = reserveDtl12;
	}

	public String getReserveDtl13() {
		return reserveDtl13;
	}

	public void setReserveDtl13(String reserveDtl13) {
		this.reserveDtl13 = reserveDtl13;
	}

	public String getReserveDtl14() {
		return reserveDtl14;
	}

	public void setReserveDtl14(String reserveDtl14) {
		this.reserveDtl14 = reserveDtl14;
	}

	public String getReserveDtl15() {
		return reserveDtl15;
	}

	public void setReserveDtl15(String reserveDtl15) {
		this.reserveDtl15 = reserveDtl15;
	}

	public String getReserveDtl16() {
		return reserveDtl16;
	}

	public void setReserveDtl16(String reserveDtl16) {
		this.reserveDtl16 = reserveDtl16;
	}

}
