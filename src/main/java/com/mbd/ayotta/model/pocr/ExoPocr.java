package com.mbd.ayotta.model.pocr;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.model.fa.ExoFaHeader;
import com.mbd.ayotta.model.fa.ExoFaTrxControl;
import com.mbd.ayotta.model.po.ExoPoDetails;
import com.mbd.ayotta.model.po.ExoPoLineDetails;
import com.mbd.ayotta.utils.DateUtil;

public class ExoPocr extends AbstractAction {
	Logger logger = Logger.getLogger(ExoPocr.class);
	
	private String pocrFFPath;
	private String toDuns;
	private PocrTrxCtrl ctl;
	private PocrHeader hdr;
	private PocrRefDescHdr dsh;
	private PocrRefInfoHdr rfh;
	private PocrDetail dtl;
	private List<PocrDetail> pocrDetailList = new ArrayList<PocrDetail>();
	
	private static Hashtable<String, Integer> tblTypes ;
	static {
		tblTypes = new Hashtable<String, Integer>();
		tblTypes.put("CTL",new Integer(1));
		tblTypes.put("HDR",new Integer(2));
		tblTypes.put("RFH",new Integer(3));
		tblTypes.put("DSH",new Integer(4));
		tblTypes.put("DTL", new Integer(5));
	}
	
	//ADD TRANSACTION CONTROL DETAILS.
	public static PocrTrxCtrl getCtl(String str){
		PocrTrxCtrl ctl = new PocrTrxCtrl();
		ArrayList<String> ctlList = new ArrayList<String>();
		ctlList = splitString(str);
		int index = 0;
		for (String strVal : ctlList){
			switch (index){
			case 0:
				ctl.setRecordId(strVal);
				break;
			case 1:
				ctl.setFuncIdCode(strVal);
				break;
			case 2:
				ctl.setTrxCtrlNo(strVal);
				break;
			case 3:
				ctl.setSenderId(strVal);
				break;
			case 4:
				ctl.setRecieverId(strVal);
				break;
			case 5:
				ctl.setFfVersionCtrlNo(strVal);
				break;
			case 6:
				ctl.setBbsId(strVal);
				break;
			case 7:
				ctl.setTrxDate(DateUtil.stringToDate(strVal));
				break;
			case 8:
				ctl.setTrxTime(strVal);
				break;
			case 9:
				ctl.setReserveCtl9(strVal);
				break;
			case 10:
				ctl.setReserveCtl10(strVal);
				break;
			case 11:
				ctl.setReserveCtl11(strVal);
				break;
			case 12:
				ctl.setReserveCtl12(strVal);
				break;
			case 13:
				ctl.setReserveCtl13(strVal);
				break;
			}
			index++;
		}
		return ctl;
	}
	
    //ADD POCR HEADER DETAILS.
	public static PocrHeader getHdr(String str, PocrTrxCtrl pocrTrxCtrl){
		PocrHeader hdr = new PocrHeader();
		ArrayList<String> hdrList = new ArrayList<String>();
		hdrList = splitString(str);
		int index = 0;
		for (String strVal : hdrList){
			switch (index){
			case 0:
				hdr.setRecordId(strVal);
				break;
			case 1:
				hdr.setPurpose(strVal);
				break;
			case 2:
				hdr.setAckType(strVal);
				break;
			case 3:
				hdr.setPoNumber(strVal);
				break;
			case 4:
				hdr.setChgOrdSeqNo(parseStrToInteger(strVal));
				break;
			case 5:
				hdr.setPoDate(DateUtil.stringToDate(strVal));
				break;
			case 6:
				hdr.setRefId(strVal);
				break;
			case 7:
				hdr.setPocrIssueDate(DateUtil.stringToDate(strVal));
				break;
			case 8:
				hdr.setPocDate(DateUtil.stringToDate(strVal));
				break;
			case 9:
				hdr.setPoOrdTypeCode(strVal);
				break;
			case 10:
				hdr.setTransTypeCode(strVal);
				break;
			case 11:
				hdr.setBuyerMpid(strVal);
				break;
			case 12:
				hdr.setSellerMpid(strVal);
				break;
			case 13:
				hdr.setSupplierCName(strVal);
				break;
			case 14:
				hdr.setSupplierCEmail(strVal);
				break;
			case 15:
				hdr.setReserveHdr15(strVal);
				break;
			case 16:
				hdr.setReserveHdr16(strVal);
				break;
			case 17:
				hdr.setReserveHdr17(strVal);
				break;
			case 18:
				hdr.setReserveHdr18(strVal);
				break;
			case 19:
				hdr.setReserveHdr19(strVal);
				break;
			case 20:
				hdr.setReserveHdr20(strVal);
				break;
			case 21:
				hdr.setReserveHdr21(strVal);
				break;
			case 22:
				hdr.setReserveHdr22(strVal);
				break;
			case 23:
				hdr.setReserveHdr23(strVal);
				break;
			case 24:
				hdr.setReserveHdr24(strVal);
				break;
			case 25:
				hdr.setReserveHdr25(strVal);
				break;
			}
			index++;
		 }
		pocrTrxCtrl.getPocrHeader().add(hdr);
		return hdr;
	}
		
	//ADD REFERENCE INFORMATION HEADER DETAILS.
	public static PocrRefInfoHdr getRef(String str, PocrHeader pocrHeader){
		PocrRefInfoHdr refInfo = new PocrRefInfoHdr();
		ArrayList<String> refInfoList = new ArrayList<String>();
		refInfoList = splitString(str);
		int index = 0;
		for (String strVal : refInfoList){
			switch (index){
			case 0:
				refInfo.setRecordId(strVal);
				break;
			case 1:
				refInfo.setRefCode(strVal);
				break;
			case 2:
				refInfo.setRefTitle(strVal);
				break;
			case 3:
				refInfo.setAddRefTitle(strVal);
				break;
			}
			index++;
		 }
		pocrHeader.getPocrRefInfoHdr().add(refInfo);
		return refInfo;
	}

	//ADD REFERENCE DESCRIPTION HEADER DETAILS.
	public static PocrRefDescHdr getDsh(String str, PocrRefInfoHdr pocrRefInfoHdr) {
		PocrRefDescHdr refDescInfo = new PocrRefDescHdr();
		ArrayList<String> refDescList = new ArrayList<String>();
		refDescList = splitString(str);
		int index = 0;
		for (String strVal : refDescList) {
			switch (index) {
			case 0:
				refDescInfo.setRecordId(strVal);
				break;
			case 1:
				refDescInfo.setRefDesc(strVal);
				break;
			}
			index++;
		}
		pocrRefInfoHdr.getPocrRefDescHdr().add(refDescInfo);
		return refDescInfo;
	}

	//ADD POCR DETAIL.
	public static PocrDetail getDtl(String str, PocrHeader pocrHeader){
		PocrDetail dtl = new PocrDetail();
		ArrayList<String> dtlList = new ArrayList<String>();
		dtlList = splitString(str);
		int index = 0;
		for (String strVal : dtlList){
			switch (index){
			case 0:
				dtl.setRecordId(strVal);
				break;
			case 1:
				dtl.setPocLineId(strVal);
				break;
			case 2:
				dtl.setLineItemStCode(strVal);
				break;
			case 3:
				dtl.setScheduleId(strVal);
				break;
			case 4:
				dtl.setReqSchQty(parseStrToDouble(strVal));
				break;
			case 5:
				dtl.setScheduleUom(strVal);
				break;
			case 6:
				dtl.setReserveDtl6(strVal);
				break;
			case 7:
				dtl.setReserveDtl7(strVal);
				break;
			case 8:
				dtl.setReserveDtl8(strVal);
				break;
			case 9:
				dtl.setReserveDtl9(strVal);
				break;
			case 10:
				dtl.setReserveDtl10(strVal);
				break;
			case 11:
				dtl.setReserveDtl11(strVal);
				break;
			case 12:
				dtl.setReserveDtl12(strVal);
				break;
			case 13:
				dtl.setReserveDtl13(strVal);
				break;
			}
			index++;
		}
		pocrHeader.getPocrDetail().add(dtl);
		return dtl;
	}
	
	public String getPocrFFPath() {
		return pocrFFPath;
	}

	public void setPocrFFPath(String pocrFFPath) {
		this.pocrFFPath = pocrFFPath;
	}

	public String getToDuns() {
		return toDuns;
	}

	public void setToDuns(String toDuns) {
		this.toDuns = toDuns;
	}

	public PocrTrxCtrl getCtl() {
		return ctl;
	}

	public void setCtl(PocrTrxCtrl ctl) {
		this.ctl = ctl;
	}

	public PocrHeader getHdr() {
		return hdr;
	}

	public void setHdr(PocrHeader hdr) {
		this.hdr = hdr;
	}

	public PocrRefDescHdr getDsh() {
		return dsh;
	}

	public void setDsh(PocrRefDescHdr dsh) {
		this.dsh = dsh;
	}

	public PocrRefInfoHdr getRfh() {
		return rfh;
	}

	public void setRfh(PocrRefInfoHdr rfh) {
		this.rfh = rfh;
	}

	public List<PocrDetail> getPocrDetailList() {
		return pocrDetailList;
	}

	public void setPocrDetailList(List<PocrDetail> pocrDetailList) {
		this.pocrDetailList = pocrDetailList;
	}

	public PocrDetail getDtl() {
		return dtl;
	}

	public void setDtl(PocrDetail dtl) {
		this.dtl = dtl;
	}
}
