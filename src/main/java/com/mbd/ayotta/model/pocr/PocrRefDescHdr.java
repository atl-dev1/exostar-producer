package com.mbd.ayotta.model.pocr;

public class PocrRefDescHdr {

	private String recordId;
	private String refDesc;

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getRefDesc() {
		return refDesc;
	}

	public void setRefDesc(String refDesc) {
		this.refDesc = refDesc;
	}
}
