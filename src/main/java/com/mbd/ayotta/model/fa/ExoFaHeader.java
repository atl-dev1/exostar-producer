package com.mbd.ayotta.model.fa;

import java.sql.Timestamp;

public class ExoFaHeader {
	private Integer faId;
	private String recordId;
	private String source;
	private String faType;
	private String errorSeverity;
	private String errorCode;
	private String errorMsg;
	private String msgId;
	private String orgDocSender;
	private String orgDocReceiver;
	private String docType;
	private String generalNote;
	private Timestamp createDate;

	public Integer getFaId() {
		return faId;
	}

	public void setFaId(Integer faId) {
		this.faId = faId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getFaType() {
		return faType;
	}

	public void setFaType(String faType) {
		this.faType = faType;
	}

	public String getErrorSeverity() {
		return errorSeverity;
	}

	public void setErrorSeverity(String errorSeverity) {
		this.errorSeverity = errorSeverity;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getOrgDocSender() {
		return orgDocSender;
	}

	public void setOrgDocSender(String orgDocSender) {
		this.orgDocSender = orgDocSender;
	}

	public String getOrgDocReceiver() {
		return orgDocReceiver;
	}

	public void setOrgDocReceiver(String orgDocReceiver) {
		this.orgDocReceiver = orgDocReceiver;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getGeneralNote() {
		return generalNote;
	}

	public void setGeneralNote(String generalNote) {
		this.generalNote = generalNote;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
}
