package com.mbd.ayotta.model.fa;

import java.sql.Timestamp;

import org.springframework.data.annotation.Id;

import com.mbd.ayotta.model.po.ExoPayLoad;

public class FaMessages {

	@Id
	private String elasticSearchRef;
	private String messageStatus;
	private Timestamp elkCreationDate;
	private Timestamp elkModifiedDate;
	private String fileName;
	private String orderType; //FA
	private ExoFaTrxControl trxControl;
	private String toDunsName;
	
	public String getElasticSearchRef() {
		return elasticSearchRef;
	}

	public void setElasticSearchRef(String elasticSearchRef) {
		this.elasticSearchRef = elasticSearchRef;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public Timestamp getElkCreationDate() {
		return elkCreationDate;
	}

	public void setElkCreationDate(Timestamp elkCreationDate) {
		this.elkCreationDate = elkCreationDate;
	}

	public Timestamp getElkModifiedDate() {
		return elkModifiedDate;
	}

	public void setElkModifiedDate(Timestamp elkModifiedDate) {
		this.elkModifiedDate = elkModifiedDate;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public ExoFaTrxControl getTrxControl() {
		return trxControl;
	}

	public void setTrxControl(ExoFaTrxControl trxControl) {
		this.trxControl = trxControl;
	}

	public String getToDunsName() {
		return toDunsName;
	}

	public void setToDunsName(String toDunsName) {
		this.toDunsName = toDunsName;
	}
}
