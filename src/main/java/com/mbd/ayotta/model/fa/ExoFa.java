package com.mbd.ayotta.model.fa;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Hashtable;
import org.apache.log4j.Logger;
import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.utils.DateUtil;

public class ExoFa extends AbstractAction {
	
	Logger logger = Logger.getLogger(ExoFa.class);	
	private String ffPath;
	private String toDuns;
	private ExoFaTrxControl ctl;
	private ExoFaHeader hdr;
	
	private static Hashtable<String, Integer> tblTypes ;
	static {
		tblTypes = new Hashtable<String, Integer>();
		tblTypes.put("CTL",new Integer(1));
		tblTypes.put("HDR",new Integer(2));
	}
	
	
	//ADD TRANSACTION CONTROL DETAILS.
	public static ExoFaTrxControl getCtl(String str) {
		ExoFaTrxControl ctl = new ExoFaTrxControl();
		ArrayList<String> ctlList = new ArrayList<String>();
		ctlList = splitString(str);
		int index = 0;
		for (String strVal : ctlList) {
			switch (index) {
			case 0:
				ctl.setRecordId(strVal);
				break;
			case 1:
				ctl.setFuncIdCode(strVal);
				break;
			case 2:
				ctl.setTrxCtrlNo(strVal);
				break;
			case 3:
				ctl.setSenderId(strVal);
				break;
			case 4:
				ctl.setRecieverId(strVal);
				break;
			case 5:
				ctl.setFfVersionCtrlNo(strVal);
				break;
			case 6:
				ctl.setBbsId(strVal);
				break;
			case 7:
				ctl.setTrxDate(DateUtil.stringToDate(strVal));
				break;
			case 8:
				ctl.setTrxTime(strVal);
				break;
			case 9:
				ctl.setReserveCtl9(strVal);
				break;
			case 10:
				ctl.setReserveCtl10(strVal);
				break;
			case 11:
				ctl.setReserveCtl11(strVal);
				break;
			case 12:
				ctl.setReserveCtl12(strVal);
				break;
			case 13:
				ctl.setReserveCtl13(strVal);
				break;
			case 14:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				ctl.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		return ctl;
	}
	
	//ADD ACKNOWLEDGMENT HEADER DETAILS.
	public static ExoFaHeader getHdr(String str,ExoFaTrxControl exoFaTrxControl) {
		ExoFaHeader ctl = new ExoFaHeader();
		ArrayList<String> hdrList = new ArrayList<String>();
		hdrList = splitString(str);
		//ctl.setFaTrxControl(exoFaTrxControl);
		int index = 0;
		for (String strVal : hdrList) {
			switch (index) {
			case 0:
				ctl.setRecordId(strVal);
				break;
			case 1:
				ctl.setSource(strVal);
				break;
			case 2:
				ctl.setFaType(strVal);
				break;
			case 3:
				ctl.setErrorSeverity(strVal);
				break;
			case 4:
				ctl.setErrorCode(strVal);
				break;
			case 5:
				ctl.setErrorMsg(strVal);
				break;
			case 6:
				ctl.setMsgId(strVal);
				break;
			case 7:
				ctl.setOrgDocSender(strVal);
				break;
			case 8:
				ctl.setOrgDocReceiver(strVal);
				break;
			case 9:
				ctl.setDocType(strVal);
				break;
			case 10:
				ctl.setGeneralNote(strVal);
				break;
			case 11:
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				ctl.setCreateDate(timestamp);
				break;
			}
			index++;
		}
		exoFaTrxControl.getExoFaHdrs().add(ctl);
		return ctl;
	}
		
	public String getFfPath() {
		return ffPath;
	}
	public void setFfPath(String ffPath) {
		this.ffPath = ffPath;
	}
	public String getToDuns() {
		return toDuns;
	}
	public void setToDuns(String toDuns) {
		this.toDuns = toDuns;
	}

	public ExoFaTrxControl getCtl() {
		return ctl;
	}

	public void setCtl(ExoFaTrxControl ctl) {
		this.ctl = ctl;
	}

	public ExoFaHeader getHdr() {
		return hdr;
	}

	public void setHdr(ExoFaHeader hdr) {
		this.hdr = hdr;
	}
}
