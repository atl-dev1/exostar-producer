package com.mbd.ayotta.model.por;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="EXO_POR_LINE_DETAIL",schema="XOR")
public class ExoPorDetail {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="seq_por_line")
    @SequenceGenerator(name="seq_por_line",sequenceName="EXO_POR_LINE_DETAIL_ID_S",allocationSize=1)
	@Column(name = "POR_LINE_DETAIL_ID")
	private Integer porDetailId;
	
	@Column(name = "PO_LINE_DETAIL_ID")
	private Integer poDetailId;
	
	@Column(name = "RECORD_ID")
	private String recordId;
	
	@Column(name = "POR_HEADER_ID")
	private Integer poHdrId;
	
	@Column(name = "LINE_ID")
	private String poLineId;
	
	@Column(name = "LINE_ITEM_STATUS_CODE")
	private String statusCode;
	
	@Column(name = "SCHEDULE_ID")
	private String scheduleId;
	
	@Column(name = "REQUESTED_SCHEDULE_QTY")
	private Double reqScheduleQty;
	
	@Column(name = "SCHEDULE_UOM")
	private String scheduleUOM;
	
	@Column(name = "RESERVE_DTL06")
	private String reserveHdr6;
	
	@Column(name = "RESERVE_DTL07")
	private String reserveHdr7;
	
	@Column(name = "RESERVE_DTL08")
	private String reserveHdr8;
	
	@Column(name = "RESERVE_DTL09")
	private String reserveHdr9;
	
	@Column(name = "RESERVE_DTL10")
	private String reserveHdr10;
	
	@Column(name = "RESERVE_DTL11")
	private String reserveHdr11;
	
	@Column(name = "RESERVE_DTL12")
	private String reserveHdr12;
	
	@Column(name = "RESERVE_DTL13")
	private String reserveHdr13;
	
	@Column(name = "RESERVE_DTL14")
	private String reserveHdr14;
	
	@Column(name = "RESERVE_DTL15")
	private String reserveHdr15;
	
	@Column(name = "CREATION_DATE")
	private Timestamp createDate;

	public Integer getPorDetailId() {
		return porDetailId;
	}
	public void setPorDetailId(Integer porDetailId) {
		this.porDetailId = porDetailId;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getPoLineId() {
		return poLineId;
	}
	public void setPoLineId(String poLineId) {
		this.poLineId = poLineId;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getScheduleId() {
		return scheduleId;
	}
	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}
	public Double getReqScheduleQty() {
		return reqScheduleQty;
	}
	public void setReqScheduleQty(Double reqScheduleQty) {
		this.reqScheduleQty = reqScheduleQty;
	}
	public Integer getPoHdrId() {
		return poHdrId;
	}
	public void setPoHdrId(Integer poHdrId) {
		this.poHdrId = poHdrId;
	}
	public String getScheduleUOM() {
		return scheduleUOM;
	}
	public void setScheduleUOM(String scheduleUOM) {
		this.scheduleUOM = scheduleUOM;
	}
	public String getReserveHdr6() {
		return reserveHdr6;
	}
	public void setReserveHdr6(String reserveHdr6) {
		this.reserveHdr6 = reserveHdr6;
	}
	public String getReserveHdr7() {
		return reserveHdr7;
	}
	public void setReserveHdr7(String reserveHdr7) {
		this.reserveHdr7 = reserveHdr7;
	}
	public String getReserveHdr8() {
		return reserveHdr8;
	}
	public void setReserveHdr8(String reserveHdr8) {
		this.reserveHdr8 = reserveHdr8;
	}
	public String getReserveHdr9() {
		return reserveHdr9;
	}
	public void setReserveHdr9(String reserveHdr9) {
		this.reserveHdr9 = reserveHdr9;
	}
	public String getReserveHdr10() {
		return reserveHdr10;
	}
	public void setReserveHdr10(String reserveHdr10) {
		this.reserveHdr10 = reserveHdr10;
	}
	public String getReserveHdr11() {
		return reserveHdr11;
	}
	public void setReserveHdr11(String reserveHdr11) {
		this.reserveHdr11 = reserveHdr11;
	}
	public String getReserveHdr12() {
		return reserveHdr12;
	}
	public void setReserveHdr12(String reserveHdr12) {
		this.reserveHdr12 = reserveHdr12;
	}
	public String getReserveHdr13() {
		return reserveHdr13;
	}
	public void setReserveHdr13(String reserveHdr13) {
		this.reserveHdr13 = reserveHdr13;
	}
	public String getReserveHdr14() {
		return reserveHdr14;
	}
	public void setReserveHdr14(String reserveHdr14) {
		this.reserveHdr14 = reserveHdr14;
	}
	public String getReserveHdr15() {
		return reserveHdr15;
	}
	public void setReserveHdr15(String reserveHdr15) {
		this.reserveHdr15 = reserveHdr15;
	}
	
	
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	public Integer getPoDetailId() {
		return poDetailId;
	}
	public void setPoDetailId(Integer poDetailId) {
		this.poDetailId = poDetailId;
	}

	public String toStringFlatFile() {
		return (recordId != null ? recordId : "") + "|"
				+ (poLineId != null ? poLineId : "") + "|"
				+ (statusCode != null ? statusCode : "") + "|"
				+ (scheduleId != null ? scheduleId : "") + "|"
				+ (reqScheduleQty != null ? reqScheduleQty : "") + "|"
				+ (scheduleUOM != null ? scheduleUOM : "") + "|"
				+ (reserveHdr6 != null ? reserveHdr6 : "") + "|"
				+ (reserveHdr7 != null ? reserveHdr7 : "") + "|"
				+ (reserveHdr8 != null ? reserveHdr8 : "") + "|"
				+ (reserveHdr9 != null ? reserveHdr9 : "") + "|"
				+ (reserveHdr10 != null ? reserveHdr10 : "") + "|"
				+ (reserveHdr11 != null ? reserveHdr11 : "") + "|"
				+ (reserveHdr12 != null ? reserveHdr12 : "") + "|"
				+ (reserveHdr13 != null ? reserveHdr13 : "") + "|"
				+ (reserveHdr14 != null ? reserveHdr14 : "") + "|"
				+ (reserveHdr15 != null ? reserveHdr15 : "") + "|";
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
