package com.mbd.ayotta.model.por;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="EXO_POR_HEADER_REF",schema="XOR")
public class ExoRefInfoHdr {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="seq_por_hdr_ref")
    @SequenceGenerator(name="seq_por_hdr_ref",sequenceName="EXO_POR_HEADER_REF_ID_S",allocationSize=1)
	@Column(name = "POR_REF_HEADER_ID")
	private Integer refInfoHdrId;
	
	@Column(name = "POR_HEADER_ID")
	private Integer porHdrId;
	
	@Column(name = "RECORD_ID")
	private String recordId;
	
	@Column(name = "REF_CODE")
	private String refCode;
	
	@Column(name = "REF_TITLE")
	private String refTitle;
	
	@Column(name = "ADDITIONAL_REF_TITLE")	
	private String addRefTitle;
	
	@Column(name = "CREATION_DATE")
	private Timestamp createDate;
	
	public Integer getRefInfoHdrId() {
		return refInfoHdrId;
	}
	public void setRefInfoHdrId(Integer refInfoHdrId) {
		this.refInfoHdrId = refInfoHdrId;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getRefCode() {
		return refCode;
	}
	public void setRefCode(String refCode) {
		this.refCode = refCode;
	}
	public String getRefTitle() {
		return refTitle;
	}
	public void setRefTitle(String refTitle) {
		this.refTitle = refTitle;
	}
	public String getAddRefTitle() {
		return addRefTitle;
	}
	public void setAddRefTitle(String addRefTitle) {
		this.addRefTitle = addRefTitle;
	}
	
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	
	public Integer getPorHdrId() {
		return porHdrId;
	}
	public void setPorHdrId(Integer porHdrId) {
		this.porHdrId = porHdrId;
	}
	public String toStringFlatFile() {
		return (recordId != null ? recordId : "") + "|"
				+ (refCode != null ? refCode : "") + "|"
				+ (refTitle != null ? refTitle : "") + "|"
				+ (addRefTitle != null ? addRefTitle : "") + "|";
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
}
