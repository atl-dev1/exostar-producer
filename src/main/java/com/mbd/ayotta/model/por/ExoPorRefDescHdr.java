package com.mbd.ayotta.model.por;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="EXO_POR_HEADER_REF_DESC",schema="XOR")
public class ExoPorRefDescHdr {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="seq_por_hdr_desc")
    @SequenceGenerator(name="seq_por_hdr_desc",sequenceName="EXO_POR_HDR_REF_DESC_ID_S",allocationSize=1)
	@Column(name = "POR_REF_DESC_ID")
	private Integer refDescHdrId;
	
	@Column(name = "POR_REF_HEADER_ID")
	private Integer porHdrId;
	
	@Column(name = "RECORD_ID")
	private String recordId;
	
	@Column(name = "REFERENCE_DESCRIPTION")
	private String refDesc;
	
	@Column(name = "CREATION_DATE")
	private Timestamp createDate;
	
	public Integer getRefDescHdrId() {
		return refDescHdrId;
	}
	public void setRefDescHdrId(Integer refDescHdrId) {
		this.refDescHdrId = refDescHdrId;
	}
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getRefDesc() {
		return refDesc;
	}
	public void setRefDesc(String refDesc) {
		this.refDesc = refDesc;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	
	public Integer getPorHdrId() {
		return porHdrId;
	}
	public void setPorHdrId(Integer porHdrId) {
		this.porHdrId = porHdrId;
	}
	public String toStringFlatFile() {
		  return  (recordId != null ? recordId:"")+"|"+(refDesc != null ? refDesc:"")+"|";
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
