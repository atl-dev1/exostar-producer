package com.mbd.ayotta.model.por;

import java.sql.Timestamp;

import org.springframework.data.annotation.Id;

import com.mbd.ayotta.model.ExoTrig;

public class PorMessages {

	@Id
	private String elasticSearchRef;
	private String messageStatus;
	private Timestamp elkCreationDate;
	private Timestamp elkModifiedDate;
	private String orderType; /*POR*/
	private String porFileContent;
	private String fileName;
	private ExoTrig exoTrig;

	public String getElasticSearchRef() {
		return elasticSearchRef;
	}

	public void setElasticSearchRef(String elasticSearchRef) {
		this.elasticSearchRef = elasticSearchRef;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public Timestamp getElkCreationDate() {
		return elkCreationDate;
	}

	public void setElkCreationDate(Timestamp elkCreationDate) {
		this.elkCreationDate = elkCreationDate;
	}

	public Timestamp getElkModifiedDate() {
		return elkModifiedDate;
	}

	public void setElkModifiedDate(Timestamp elkModifiedDate) {
		this.elkModifiedDate = elkModifiedDate;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getPorFileContent() {
		return porFileContent;
	}

	public void setPorFileContent(String porFileContent) {
		this.porFileContent = porFileContent;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public ExoTrig getExoTrig() {
		return exoTrig;
	}

	public void setExoTrig(ExoTrig exoTrig) {
		this.exoTrig = exoTrig;
	}
}
