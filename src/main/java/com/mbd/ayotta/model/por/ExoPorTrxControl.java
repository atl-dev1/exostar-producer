package com.mbd.ayotta.model.por;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import com.mbd.ayotta.utils.DateUtil;

@Entity
@Table(name="EXO_POR_TRX_CONTROL",schema="XOR")
public class ExoPorTrxControl {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="seq_por_trx")
    @SequenceGenerator(name="seq_por_trx",sequenceName="XOR_POR_TRX_CONTROL_ID_S",allocationSize=1)
	@Column(name = "POR_TRX_CONTROL_ID")
	private Integer trxControlId ;
	
	@Column(name = "RECORD_ID")
	private String recordId;
	
	@Column(name = "PO_TRX_CONTROL_ID")
	private Integer poTrxCtrlId;;
	
	@Column(name = "FUNCTIONAL_IDENTIFIER_CODE")
	private String funcIdCode;
	
	@Column(name = "TRX_CONTROL_NUMBER")
	private String trxCtrlNo;
	
	@Column(name = "SENDER_IDENTIFICATION")
	private String senderId;
	
	@Column(name = "RECEIVER_IDENTIFICATION")
	private String recieverId;
	
	@Column(name = "FF_PO_VERSION_CTL_NUMBER")
	private String ffVersionCtrlNo;
	
	@Column(name = "BBS_IDENTIFIER")
	private String bbsId;
	
	@Column(name = "TRX_DATE")
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(style = "S-")
	private Date trxDate;
	
	@Column(name = "TRX_TIME")
	private String trxTime;
	
	@Column(name = "RESERVE_CTL09")
	private String reserveCtl9;
	
	@Column(name = "RESERVE_CTL10")
	private String reserveCtl10;
	
	@Column(name = "RESERVE_CTL11")
	private String reserveCtl11;
	
	@Column(name = "RESERVE_CTL12")
	private String reserveCtl12;
	
	@Column(name = "RESERVE_CTL13")
	private String reserveCtl13;
	
	@Column(name = "CREATION_DATE")
	private Timestamp createDate;
	
	public Integer getTrxControlId() {
		return trxControlId;
	}

	public void setTrxControlId(Integer trxControlId) {
		this.trxControlId = trxControlId;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getFuncIdCode() {
		return funcIdCode;
	}

	public void setFuncIdCode(String funcIdCode) {
		this.funcIdCode = funcIdCode;
	}

	public String getTrxCtrlNo() {
		return trxCtrlNo;
	}

	public void setTrxCtrlNo(String trxCtrlNo) {
		this.trxCtrlNo = trxCtrlNo;
	}

	public String getSenderId() {
		return senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getRecieverId() {
		return recieverId;
	}

	public void setRecieverId(String recieverId) {
		this.recieverId = recieverId;
	}

	public String getFfVersionCtrlNo() {
		return ffVersionCtrlNo;
	}

	public void setFfVersionCtrlNo(String ffVersionCtrlNo) {
		this.ffVersionCtrlNo = ffVersionCtrlNo;
	}

	public String getBbsId() {
		return bbsId;
	}

	public void setBbsId(String bbsId) {
		this.bbsId = bbsId;
	}

	public Date getTrxDate() {
		return trxDate;
	}

	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}

	public String getReserveCtl9() {
		return reserveCtl9;
	}

	public void setReserveCtl9(String reserveCtl9) {
		this.reserveCtl9 = reserveCtl9;
	}

	public String getReserveCtl10() {
		return reserveCtl10;
	}

	public void setReserveCtl10(String reserveCtl10) {
		this.reserveCtl10 = reserveCtl10;
	}

	public String getReserveCtl11() {
		return reserveCtl11;
	}

	public void setReserveCtl11(String reserveCtl11) {
		this.reserveCtl11 = reserveCtl11;
	}

	public String getReserveCtl12() {
		return reserveCtl12;
	}

	public void setReserveCtl12(String reserveCtl12) {
		this.reserveCtl12 = reserveCtl12;
	}

	public String getReserveCtl13() {
		return reserveCtl13;
	}

	public void setReserveCtl13(String reserveCtl13) {
		this.reserveCtl13 = reserveCtl13;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Override
	public boolean equals(Object o) {
		return EqualsBuilder.reflectionEquals(this, o);		
	}

	public String getTrxTime() {
		return trxTime;
	}

	public void setTrxTime(String trxTime) {
		this.trxTime = trxTime;
	}

	public Integer getPoTrxCtrlId() {
		return poTrxCtrlId;
	}

	public void setPoTrxCtrlId(Integer poTrxCtrlId) {
		this.poTrxCtrlId = poTrxCtrlId;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	public String toStringFlatFile() {
		return (recordId != null ? recordId : "") + "|" + (funcIdCode != null ?  funcIdCode : "") + "|" 
				+ (trxCtrlNo != null ? trxCtrlNo : "") + "|"
				+ (senderId != null ? senderId : "") + "|" + (recieverId != null ? recieverId : "") + "|"
				+ (ffVersionCtrlNo != null ? ffVersionCtrlNo : "") + "|" + (bbsId != null ? bbsId : "") + "|"
				+ (trxDate != null ? DateUtil.dateToStr(trxDate) : "") + "|" + (trxTime != null ? trxTime : "") + "|"
				+ (reserveCtl9 != null ? reserveCtl9 : "") + "|" + (reserveCtl10 != null ? reserveCtl10 : "") + "|"
				+ (reserveCtl11 != null ? reserveCtl11 : "") + "|" + (reserveCtl12 != null ? reserveCtl12 : "") + "|"
				+ (reserveCtl13 != null ? reserveCtl13 : "") + "|";

	}
}
