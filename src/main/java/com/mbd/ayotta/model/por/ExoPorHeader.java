package com.mbd.ayotta.model.por;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.ToStringBuilder;

import com.mbd.ayotta.utils.DateUtil;



@Entity
@Table(name="EXO_POR_HEADER",schema="XOR")
public class ExoPorHeader {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="seq_por_hdr")
    @SequenceGenerator(name="seq_por_hdr",sequenceName="EXO_POR_HEADER_ID_S",allocationSize=1)
	@Column(name = "POR_HEADER_ID")
	private Integer porHdrId;
	
	@Column(name = "POR_TRX_CONTROL_ID")
	private Integer porTrxCtrlId;
	
	@Column(name = "PO_HEADER_ID")
	private Integer poHdrId;
	
	@Column(name = "RECORD_ID")
	private String recordId;
	
	@Column(name = "PURPOSE")
	private String Purpose;
	
	@Column(name = "ACKNOWLEDGMENT_TYPE")
	private String ackType;
	
	@Column(name = "PO_NUMBER")
	private String poNumber;
	
	@Column(name = "CHANGE_ORDER_SEQ_NUMBER")
	private String changeOrderSeqNo;
	
	@Column(name = "ORDER_DATE")
	private Date poDate;
	
	@Column(name = "REFERENCE_IDENTIFICATION")
	private String referenceId;
	
	@Column(name = "POR_ISSUE_DATE")
	private Date pocrIssueDate;
	
	@Column(name = "POC_DATE")
	private Date pocDate;
	
	@Column(name = "PO_TYPE_CODE")
	private String poTypeCode;
	
	@Column(name = "TRX_TYPE_CODE")
	private String trxTypeCode;
	
	@Column(name = "BUYER_MPID")
	private String buyerMPID;
	
	@Column(name = "SELLER_MPID")
	private String supplierMPID;
	
	@Column(name = "SUPPLIER_CONTACT_NAME")
	private String supplierName;
	
	@Column(name = "SUPPLIER_CONTACT_EMAIL")
	private String supplierEmail;
	
	@Column(name = "SUPPLIER_CODE")
	private String supplierCode;
	
	@Column(name = "RESERVE_HDR16")
	private String reserveHdr16;
	
	@Column(name = "RESERVE_HDR17")
	private String reserveHdr17;
	
	@Column(name = "RESERVE_HDR18")
	private String reserveHdr18;
	
	@Column(name = "RESERVE_HDR19")
	private String reserveHdr19;
	
	@Column(name = "RESERVE_HDR20")
	private String reserveHdr20;
	
	@Column(name = "RESERVE_HDR21")
	private String reserveHdr21;
	
	@Column(name = "RESERVE_HDR22")
	private String reserveHdr22;
	
	@Column(name = "RESERVE_HDR23")
	private String reserveHdr23;
	
	@Column(name = "RESERVE_HDR24")
	private String reserveHdr24;
	
	@Column(name = "RESERVE_HDR25")
	private String reserveHdr25;
	
	@Column(name = "CREATION_DATE")
	private Timestamp createDate;
	
	public Integer getPorHdrId() {
		return porHdrId;
	}
	public Integer getPorTrxCtrlId() {
		return porTrxCtrlId;
	}
	public void setPorTrxCtrlId(Integer porTrxCtrlId) {
		this.porTrxCtrlId = porTrxCtrlId;
	}
	public void setPorHdrId(Integer porHdrId) {
		this.porHdrId = porHdrId;
	}
	
	public String getRecordId() {
		return recordId;
	}
	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}
	public String getPurpose() {
		return Purpose;
	}
	public void setPurpose(String purpose) {
		Purpose = purpose;
	}
	public String getAckType() {
		return ackType;
	}
	public void setAckType(String ackType) {
		this.ackType = ackType;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getChangeOrderSeqNo() {
		return changeOrderSeqNo;
	}
	public void setChangeOrderSeqNo(String changeOrderSeqNo) {
		this.changeOrderSeqNo = changeOrderSeqNo;
	}
	public Date getPoDate() {
		return poDate;
	}
	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
	public Date getPocrIssueDate() {
		return pocrIssueDate;
	}
	public void setPocrIssueDate(Date pocrIssueDate) {
		this.pocrIssueDate = pocrIssueDate;
	}
	public Date getPocDate() {
		return pocDate;
	}
	public void setPocDate(Date pocDate) {
		this.pocDate = pocDate;
	}
	public String getPoTypeCode() {
		return poTypeCode;
	}
	public void setPoTypeCode(String poTypeCode) {
		this.poTypeCode = poTypeCode;
	}
	public String getTrxTypeCode() {
		return trxTypeCode;
	}
	public void setTrxTypeCode(String trxTypeCode) {
		this.trxTypeCode = trxTypeCode;
	}
	public String getBuyerMPID() {
		return buyerMPID;
	}
	public void setBuyerMPID(String buyerMPID) {
		this.buyerMPID = buyerMPID;
	}
	public String getSupplierMPID() {
		return supplierMPID;
	}
	public void setSupplierMPID(String supplierMPID) {
		this.supplierMPID = supplierMPID;
	}

	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierEmail() {
		return supplierEmail;
	}
	public void setSupplierEmail(String supplierEmail) {
		this.supplierEmail = supplierEmail;
	}
	public String getReserveHdr16() {
		return reserveHdr16;
	}
	public void setReserveHdr16(String reserveHdr16) {
		this.reserveHdr16 = reserveHdr16;
	}
	public String getReserveHdr17() {
		return reserveHdr17;
	}
	public void setReserveHdr17(String reserveHdr17) {
		this.reserveHdr17 = reserveHdr17;
	}
	public String getReserveHdr18() {
		return reserveHdr18;
	}
	public void setReserveHdr18(String reserveHdr18) {
		this.reserveHdr18 = reserveHdr18;
	}
	public String getReserveHdr19() {
		return reserveHdr19;
	}
	public void setReserveHdr19(String reserveHdr19) {
		this.reserveHdr19 = reserveHdr19;
	}
	public String getReserveHdr20() {
		return reserveHdr20;
	}
	public void setReserveHdr20(String reserveHdr20) {
		this.reserveHdr20 = reserveHdr20;
	}
	public String getReserveHdr21() {
		return reserveHdr21;
	}
	public void setReserveHdr21(String reserveHdr21) {
		this.reserveHdr21 = reserveHdr21;
	}
	public String getReserveHdr22() {
		return reserveHdr22;
	}
	public void setReserveHdr22(String reserveHdr22) {
		this.reserveHdr22 = reserveHdr22;
	}
	public String getReserveHdr23() {
		return reserveHdr23;
	}
	public void setReserveHdr23(String reserveHdr23) {
		this.reserveHdr23 = reserveHdr23;
	}
	public String getReserveHdr24() {
		return reserveHdr24;
	}
	public void setReserveHdr24(String reserveHdr24) {
		this.reserveHdr24 = reserveHdr24;
	}
	public String getReserveHdr25() {
		return reserveHdr25;
	}
	public void setReserveHdr25(String reserveHdr25) {
		this.reserveHdr25 = reserveHdr25;
	}
	
	public Integer getPoHdrId() {
		return poHdrId;
	}
	public void setPoHdrId(Integer poHdrId) {
		this.poHdrId = poHdrId;
	}
	public String getSupplierCode() {
		return supplierCode;
	}
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	public Timestamp getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	@Transient
	public String toStringFlateFile() {
		  return  (recordId != null ? recordId:"")+"|"+
				  (Purpose != null ? Purpose:"")+"|"+
				  (ackType != null ? ackType:"")+"|"+
				  (poNumber != null ? poNumber:"")+"|"+
				  (changeOrderSeqNo != null ? changeOrderSeqNo:"")+"|"+
		    (poDate != null ? DateUtil.dateToStr(poDate):"")+"|"+
		    (referenceId != null ? referenceId:"")+"|"+
		    (pocrIssueDate != null ? DateUtil.dateToStr(pocrIssueDate):"")+"|"+
		    (pocDate != null ? DateUtil.dateToStr(pocDate):"")+"|"+
		    (poTypeCode != null ? poTypeCode:"")+"|"+
		    (trxTypeCode != null ? trxTypeCode:"")+"|"+
		    (buyerMPID != null ? buyerMPID: "")+"|"+
		    (supplierMPID != null ? supplierMPID:"")+"|"+
		    (supplierName != null ? supplierName: "")+"|"+
		    (supplierEmail != null ? supplierEmail:"")+"|"+
		    (supplierCode != null ? supplierCode: "")+"|"+
		    (reserveHdr16 != null ? reserveHdr16:"")+"|"+
		    (reserveHdr17 != null ? reserveHdr17: "")+"|"+
		    (reserveHdr18 != null ? reserveHdr18:"")+"|"+
		    (reserveHdr19 != null ? reserveHdr19: "")+"|"+
		    (reserveHdr20 != null ? reserveHdr20:"")+"|"+
		    (reserveHdr21 != null ? reserveHdr21: "")+"|"+
		    (reserveHdr22 != null ? reserveHdr22:"")+"|"+
		    (reserveHdr23 != null ? reserveHdr23: "")+"|"+
		    (reserveHdr24 != null ? reserveHdr24:"")+"|"+
		    (reserveHdr25 != null ? reserveHdr25:"")+"|";
		}
}
