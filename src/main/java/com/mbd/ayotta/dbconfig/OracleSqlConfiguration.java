package com.mbd.ayotta.dbconfig;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "oraclesqlEntityManager",
        transactionManagerRef = "oraclesqlTransactionManager",
        basePackages = "com.mbd.ayotta.repositories"
)
public class OracleSqlConfiguration {

	 @Bean
	    public DataSource oraclesqlDataSource() {
		 DriverManagerDataSource dataSource = new DriverManagerDataSource();
		    dataSource.setDriverClassName("oracle.jdbc.OracleDriver");
		   // dataSource.setUrl("jdbc:oracle:thin:@10.2.23.87:1521:etatst");
		    dataSource.setUrl("jdbc:oracle:thin:@ayotta-db:1645:etadev");
		    dataSource.setUsername("eta");
		    dataSource.setPassword("eta");

		    return dataSource;
	    }
	 
	 
	 @Primary
	    @Bean(name = "oraclesqlEntityManager")
	    public LocalContainerEntityManagerFactoryBean oraclesqlEntityManagerFactory(EntityManagerFactoryBuilder builder) {
	        return builder
	                    .dataSource(oraclesqlDataSource())
	                    .packages("com.mbd.ayotta.model.por") // you can also give the package where the Entities are given rather than giving Entity class
	                    .persistenceUnit("etadev")
	                    .build();
	    }
	 
	 @Primary
	    @Bean(name = "oraclesqlTransactionManager")
	    public PlatformTransactionManager oraclesqlTransactionManager(@Qualifier("oraclesqlEntityManager") EntityManagerFactory entityManagerFactory) {
	        return new JpaTransactionManager(entityManagerFactory);
	    }
}
