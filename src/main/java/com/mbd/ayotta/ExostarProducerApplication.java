package com.mbd.ayotta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mbd.ayotta.config.MqProperties;
import com.mbd.ayotta.config.OracleAQConfiguration;
import com.sun.messaging.ConnectionConfiguration;
import com.sun.messaging.ConnectionFactory;

@SpringBootApplication
@Configuration
public class ExostarProducerApplication {

	@Autowired
	OracleAQConfiguration oracleProperties;
	
	@Autowired
	MqProperties mqProps;
	
	public static void main(String[] args) {
		SpringApplication.run(ExostarProducerApplication.class, args);
	}
	
	@Bean
	public ConnectionFactory connection() {
		ConnectionFactory remoteJms = new ConnectionFactory();
	try {
			remoteJms.setProperty(ConnectionConfiguration.imqAddressList, mqProps.getMqHost());
			remoteJms.setProperty(ConnectionConfiguration.imqReconnectEnabled, "true");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return remoteJms;
	}
}
