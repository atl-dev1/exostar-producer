package com.mbd.ayotta;

import java.io.InputStream;
import java.io.OutputStream;

import org.apache.camel.Exchange;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbd.ayotta.services.PoService;
import com.mbd.ayotta.utils.ExostarUtils;

public class ExostarReader extends ExostarUtils implements DataFormat{
	
	Logger logger = LoggerFactory.getLogger(ExostarReader.class);
	ObjectMapper jacksonMapper = new ObjectMapper();
	@Autowired
	PoService service;
	
	@Override
	public void marshal(Exchange exchange, Object graph, OutputStream stream)
			throws Exception {
		
	}
	@Override
	public Object unmarshal(Exchange exchange, InputStream stream) throws Exception {
		logger.info("start unmarshalling exostar file");
		String msg = exchange.getIn().getBody(String.class);
		logger.info("exit unmarshalling exostar file");
		return msg;
	}
}
