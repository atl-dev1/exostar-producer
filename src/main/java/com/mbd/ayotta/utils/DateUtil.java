package com.mbd.ayotta.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
	
	public static String dateToStr(Date date) {
		try {
		    return new SimpleDateFormat("yyyyMMdd").format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Date strToDate(String obj) {
		Locale locale = Locale.UK;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", locale);
		try {
				sdf = new SimpleDateFormat("MM/dd/yyyy");
			return sdf.parse(obj);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String strToTime(){
		Locale locale= null;
		Date date = new Date();
		if (locale == null)
			locale = Locale.UK;
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmss", locale);
		try {
			 long now = System.currentTimeMillis();
			 int milliSec = (int) ((now / 10) % 100);
			return sdf.format(date)+milliSec;
		} catch (Exception e) {
			 e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String args[]){
		System.out.println(strToLongTime("08093333"));
	}
	public static String strToLongTime(String str){
		
		SimpleDateFormat sdf = new SimpleDateFormat("HHmmssSS");
		try {
			 
			
			return sdf.format(str);
		} catch (Exception e) {
			 e.printStackTrace();
		}
		return null;
	}
	
	public static String getCurrentDate() {
		try {
			Calendar currentDate = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmssSS");
			return formatter.format(currentDate.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Date stringToDate(String str){
		Locale locale= null;
		if (locale == null)
			locale = Locale.UK;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", locale);
		try {
			if (str != null && !str.equals("")){
				return new Date(sdf.parse(str).getTime());
			}
		} catch (ParseException e) {
			 e.printStackTrace();
		}
		return null;
	}

	private static final SimpleDateFormat DD_MMM_YYYY_HH_MM_SS = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	
	public static Timestamp getCurrentDateTS() {
		String str = DD_MMM_YYYY_HH_MM_SS.format((java.util.Date) new Timestamp(System.currentTimeMillis())); 
		return stringToDateTimeStamp(str);
	}
	
	public static Timestamp stringToDateTimeStamp(String str){
		Timestamp timestamp=null;
		if(str!=null && !str.trim().equals("")){
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
			Date date;
			try {
				date = sdf.parse(str);
				timestamp = new java.sql.Timestamp(date.getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return timestamp; 
	}
}
