package com.mbd.ayotta;

import java.util.ArrayList;

public class AbstractAction {
	
	private static String ORDER_INDEX = "exostar";
	private static String FILE_INDEX = "exostar_files";
	private static String TYPE = "exostar";
	private static String FILE_TYPE = "file";

	public static ArrayList<String> splitString(String str){
		ArrayList<String> splitList = new ArrayList<String>();
		for(String retVal : str.split("\\|", -1)){
			splitList.add(retVal);
		}
		return splitList;
	}
	
	public static Double parseStrToDouble(String str){
		if(!str.equals("")){
			return Double.parseDouble(str);
		}else{
			return null;
		}
	}
	
	public static Integer parseStrToInteger(String str){
		if(!str.equals("")){
			return Integer.parseInt(str);
		}else{
			return null;
		}
	}

	public static String getORDER_INDEX() {
		return ORDER_INDEX;
	}

	public static void setORDER_INDEX(String oRDER_INDEX) {
		ORDER_INDEX = oRDER_INDEX;
	}

	public static String getFILE_INDEX() {
		return FILE_INDEX;
	}

	public static void setFILE_INDEX(String fILE_INDEX) {
		FILE_INDEX = fILE_INDEX;
	}

	public static String getTYPE() {
		return TYPE;
	}

	public static void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public static String getFILE_TYPE() {
		return FILE_TYPE;
	}

	public static void setFILE_TYPE(String fILE_TYPE) {
		FILE_TYPE = fILE_TYPE;
	}
}
