package com.mbd.ayotta.repositories.por;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.mbd.ayotta.model.por.ExoPorTrxControl;

@Repository("porTrxControlRepos")
public interface PorTrxControlRepos extends JpaRepository<ExoPorTrxControl,Integer>{

}

