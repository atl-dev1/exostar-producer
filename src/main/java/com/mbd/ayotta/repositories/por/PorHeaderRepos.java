package com.mbd.ayotta.repositories.por;

import java.util.Iterator;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mbd.ayotta.model.por.ExoPorHeader;

@Repository("porHeaderRepos")
public interface PorHeaderRepos extends JpaRepository<ExoPorHeader,Integer>{
	
	List<ExoPorHeader> findByPorTrxCtrlId(Integer porTrxCtrlId);
	
}
