package com.mbd.ayotta.repositories.por;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mbd.ayotta.model.por.ExoRefInfoHdr;

@Repository("refInfoHdrRepos")
public interface RefInfoHdrRepos extends JpaRepository<ExoRefInfoHdr,Integer>{

	List<ExoRefInfoHdr> findByPorHdrId(Integer porHdrId);
}
