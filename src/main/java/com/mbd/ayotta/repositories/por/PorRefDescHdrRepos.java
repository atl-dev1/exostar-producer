package com.mbd.ayotta.repositories.por;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mbd.ayotta.model.por.ExoPorRefDescHdr;

@Repository("porRefDescHdrRepos")
public interface PorRefDescHdrRepos extends JpaRepository<ExoPorRefDescHdr,Integer>{

	List<ExoPorRefDescHdr> findByPorHdrId(Integer porHdrId);
}
