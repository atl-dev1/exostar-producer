package com.mbd.ayotta.repositories.por;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mbd.ayotta.model.por.ExoPorDetail;

@Repository("porDetailRepos")
public interface PorDetailRepos extends JpaRepository<ExoPorDetail,Integer>{

	List<ExoPorDetail> findByPoHdrId(Integer poHdrId);
}
