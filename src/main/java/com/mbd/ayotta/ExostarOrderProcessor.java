package com.mbd.ayotta;

import java.io.BufferedReader;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbd.ayotta.config.GenericProperties;
import com.mbd.ayotta.kafka.Producer;
import com.mbd.ayotta.model.fa.FaMessages;
import com.mbd.ayotta.model.po.PoMessages;
import com.mbd.ayotta.model.poc.PocMessages;
import com.mbd.ayotta.model.pocr.PocrMessages;
import com.mbd.ayotta.services.FaService;
import com.mbd.ayotta.services.PoService;
import com.mbd.ayotta.services.PocService;
import com.mbd.ayotta.services.PocrService;

public class ExostarOrderProcessor implements Processor{
	Logger logger = LoggerFactory.getLogger(ExostarOrderProcessor.class);

	Producer producer;
	String secret ="SLSISEDIDMZTOETA";
	ObjectMapper jacksonMapper = new ObjectMapper();
	private PoService poService;
	private PocService pocService;
	private FaService faService;
	private PocrService pocrService;
	private GenericProperties genProperty;
	
	public ExostarOrderProcessor (PoService poService, PocService pocService, FaService faService, PocrService pocrService, Producer producer, GenericProperties genProperty) {
		this.poService = poService;
		this.pocService = pocService;
		this.faService = faService;
		this.pocrService = pocrService;
		this.producer = producer;
		this.genProperty = genProperty;
	}
	
	@Override
	public void process(Exchange exchange) throws Exception {
		String msg = exchange.getIn().getBody(String.class);
		logger.info("EXOSTAR FILE: "+ msg + " process start");
		String orderTypeStr = getType(msg,"ORDER_TYPE");
		if(orderTypeStr != null && !orderTypeStr.equals("")){
			if(orderTypeStr.equalsIgnoreCase("PO")){
				/*PO*/
				PoMessages poMsg = new PoMessages();
				poMsg = poService.mappingPo(msg,poMsg);
				poService.indexOrderPoFile(poMsg);
				
				if(poMsg != null){
					logger.info("EXOSTAR PO FILE: "+ poMsg.getElasticSearchRef() + " process start");
					poElk(msg,poMsg,exchange,orderTypeStr);
					logger.info("EXOSTAR PO FILE: "+ poMsg.getElasticSearchRef() + " process exit");
				}else{
					logger.info("PO msg is null");
				}
			}else if(orderTypeStr.equalsIgnoreCase("POC")){
				/*POC*/
				PocMessages pocMsg = new PocMessages();
				pocMsg = pocService.mappingPoc(msg,pocMsg);
				pocService.indexOrderPocFile(pocMsg);
				
				if(pocMsg != null){
					logger.info("EXOSTAR POC FILE: "+ pocMsg.getElasticSearchRef() + " process start");
					pocElk(msg,pocMsg,exchange,orderTypeStr);
					logger.info("EXOSTAR POC FILE: "+ pocMsg.getElasticSearchRef() + " process exit");
				}else{
					logger.info("POC msg is null");
				}
			}else if(orderTypeStr.equalsIgnoreCase("FA")){
				/*FA*/
				FaMessages faMsg = new FaMessages();
				faMsg = faService.mappingFa(msg,faMsg);
				faService.indexOrderFaFile(faMsg);
				
				if(faMsg != null){
					logger.info("EXOSTAR FA FILE: "+ faMsg.getElasticSearchRef() + " process start");
					faElk(faMsg,exchange,orderTypeStr);
					logger.info("EXOSTAR FA FILE: "+ faMsg.getElasticSearchRef() + " process exit");
				}else{
					logger.info("FA msg is null");
				}
			}else if(orderTypeStr.equalsIgnoreCase("POCR")){
				/*POCR*/
				PocrMessages pocrMsg = new PocrMessages();
				pocrMsg = pocrService.mappingPocr(msg,pocrMsg);
				pocrService.indexOrderPocrFile(pocrMsg);
				
				if(pocrMsg != null){
					logger.info("EXOSTAR POCR FILE: "+ pocrMsg.getElasticSearchRef() + " process start");
					pocrElk(pocrMsg,exchange,orderTypeStr);
					logger.info("EXOSTAR POCR FILE: "+ pocrMsg.getElasticSearchRef() + " process exit");
				}else{
					logger.info("POCR msg is null");
				}
				
			}
		}
	}
	
	private boolean poElk(String msg, PoMessages poMsg, Exchange exchange, String orderTypeStr){
		poMsg.setElkCreationDate(new Timestamp(System.currentTimeMillis()));
		poMsg.setMessageStatus("READ_FROM_FILE");
		poMsg.setFileName(exchange.getIn().getHeader("CamelFileName").toString());
		poMsg.setOrderType(orderTypeStr);
		poMsg.setVendorCode(getType(msg,"VENDOR_CODE").substring(0,6));
		poMsg.setToDunsName(genProperty.getToDunsName());
		poMsg.setPoType(getType(msg,"PO_TYPE"));
		try {
			poService.createOrder(poMsg);
			logger.info("EXO PO FILE  "+poMsg.getFileName()+ " ORDER: "+ poMsg.getElasticSearchRef() + " saved in elastic search");
			String objectString = jacksonMapper.writeValueAsString(poMsg);
			producer.sendMessages("exostar_po",objectString);
			logger.info("EXO PO FILE  "+poMsg.getFileName()+ " ORDER: "+ poMsg.getElasticSearchRef() + " sent to message queue");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private boolean pocElk(String msg,PocMessages pocMsg, Exchange exchange, String orderTypeStr) {
		pocMsg.setElkCreationDate(new Timestamp(System.currentTimeMillis()));
		pocMsg.setMessageStatus("READ_FROM_FILE");
		pocMsg.setFileName(exchange.getIn().getHeader("CamelFileName").toString());
		pocMsg.setOrderType(orderTypeStr);
		pocMsg.setVendorCode(getType(msg,"VENDOR_CODE").substring(0,6));
		pocMsg.setToDunsName(genProperty.getToDunsName());
		pocMsg.setPoType(getType(msg,"PO_TYPE"));
		try {
			pocService.createOrder(pocMsg);
			logger.info("EXO POC FILE  "+pocMsg.getFileName()+ " ORDER: "+ pocMsg.getElasticSearchRef() + " saved in elastic search");
			String objectString = jacksonMapper.writeValueAsString(pocMsg);
			producer.sendMessage("exostar_poc",objectString);
			logger.info("EXO POC FILE  "+pocMsg.getFileName()+ " ORDER: "+ pocMsg.getElasticSearchRef() + " sent to message queue");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private boolean faElk(FaMessages faMsg, Exchange exchange, String orderTypeStr) {
		faMsg.setElkCreationDate(new Timestamp(System.currentTimeMillis()));
		faMsg.setMessageStatus("READ_FROM_FILE");
		faMsg.setFileName(exchange.getIn().getHeader("CamelFileName").toString());
		faMsg.setOrderType(orderTypeStr);
		faMsg.setToDunsName(genProperty.getToDunsName());
		try {
			faService.createOrder(faMsg);
			logger.info("EXO FA FILE  "+faMsg.getFileName()+ " ORDER: "+ faMsg.getElasticSearchRef() + " saved in elastic search");
			String objectString = jacksonMapper.writeValueAsString(faMsg);
			producer.sendMessage("exostar_fa",objectString);
			logger.info("EXO FA FILE  "+faMsg.getFileName()+ " ORDER: "+ faMsg.getElasticSearchRef() + " sent to message queue");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private boolean pocrElk(PocrMessages pocrMsg, Exchange exchange, String orderTypeStr){
		pocrMsg.setElkCreationDate(new Timestamp(System.currentTimeMillis()));
		pocrMsg.setMessageStatus("READ_FROM_FILE");
		pocrMsg.setFileName(exchange.getIn().getHeader("CamelFileName").toString());
		pocrMsg.setOrderType(orderTypeStr);
		try {
			pocrService.createOrder(pocrMsg);
			logger.info("EXO POCR FILE  "+pocrMsg.getFileName()+ " ORDER: "+ pocrMsg.getElasticSearchRef() + " saved in elastic search");
			String objectString = jacksonMapper.writeValueAsString(pocrMsg);
			producer.sendMessage("exostar_pocr",objectString);
			logger.info("EXO POCR FILE  "+pocrMsg.getFileName()+ " ORDER: "+ pocrMsg.getElasticSearchRef() + " sent to message queue");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	//GET ORDER FILE TYPE
	private String getType(String msg, String paramName){
		StringReader strReader= null;
		String line = null;
		strReader = new StringReader(msg);
		BufferedReader br = new BufferedReader(strReader);
		int index = 0;
		String paramValue = "";
		try {
			while ((line=br.readLine()) != null) {
				if(index==0){
					int counter = 0;
					for(String retVal: line.split("\\|", -1)){
						if(paramName.equals("ORDER_TYPE")){
							if(counter == 1){
								paramValue = retVal;
								logger.debug("orderType :"+ paramValue);
								break;
							}
						}else if(paramName.equals("PO_TYPE")){
							if(counter == 3){
								paramValue = retVal;
								logger.debug("poType :"+ paramValue);
								break;
							}
						}else if(paramName.equals("VENDOR_CODE")){
							if(counter == 2){
								paramValue = retVal;
								logger.debug("vendorCode :"+ paramValue);
								break;
							}
						}
						counter++;
					}
					return paramValue;
				}
				index++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("paramValue :"+ paramValue);
		return paramValue;
	}
}
