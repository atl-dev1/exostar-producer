package com.mbd.ayotta.web;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mbd.ayotta.ExostarOrderProcessor;
import com.mbd.ayotta.ExostarReader;
import com.mbd.ayotta.config.GenericProperties;
import com.mbd.ayotta.kafka.Producer;
import com.mbd.ayotta.services.FaService;
import com.mbd.ayotta.services.PoService;
import com.mbd.ayotta.services.PocService;
import com.mbd.ayotta.services.PocrService;


@Component
public class ExostarRouteBuilder extends SpringRouteBuilder{
	
	@Autowired 
	Producer producer;
	
	@Autowired
	GenericProperties generic;
	
	@Autowired 
	PoService poService;
	@Autowired 
	PocService pocService;
	@Autowired
	FaService faService;
	@Autowired
	PocrService pocrService;
	
	/*READ EXOSTAR PO ORDER FILE*/ 
	@Override
	public void configure() throws Exception {
		from(generic.getInputFilePath()).log("Incoming File: ${file:onlyname}")
		.doTry()
		.unmarshal(new ExostarReader()).process(new ExostarOrderProcessor(poService,pocService,faService,pocrService, producer,generic))
        .doCatch(Exception.class).process(new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                  Exception cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
                  log.error("Processing error "+ cause.getMessage());
                  cause.printStackTrace();
            }
          }).endDoTry().
        log("Loaded File: ${file:onlyname}");
	}
}
