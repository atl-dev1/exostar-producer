package com.mbd.ayotta.web;

import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.mbd.ayotta.model.XxmSpParams;


@Configuration
public class MailHelper {

	private Logger logger = LoggerFactory.getLogger(MailHelper.class);
	
	@Value("${spring.mail.from}")
	private String MAIL_FROM;
	
	@Value("${spring.mail.aog.to}")
	private String MAIL_AOG_TO;
	
	@Value("${spring.schema.mail.to}")
	private String SCHEMA_MAIL_TO;
	
	@Value("${spring.mail.host}")
	private String MAIL_HOST;
	
	private List<String> MAIL_TOS;
	private JavaMailSenderImpl sender;
	private String provider;
	private MimeMessageHelper helper;
	private MimeMessage message;
	private String SUBJECT_FAILURE;
	private String SUBJECT_SUCCESS;
	private String[] MAIL_TO;
	
	@Autowired private JavaMailSender mailSender;
	public static Hashtable parameters = new Hashtable();

	public static String getParam(String name) {
		return (String) (parameters.get(name) == null ? "" : parameters.get(name));
	}
	
	@SuppressWarnings("unchecked")
	public static void setParams(List params) {
		if (params.size() > 0) {
			for (int i = 0; i < params.size(); i++) {
				XxmSpParams param = (XxmSpParams) params.get(i);
				parameters.put(param.getParameterName(), param.getParameterValue());
				System.out.println("initializing parameter " + parameters.get(param.getParameterName()));
			}
		}
	}

	protected void sendMail(String strMessage, String Subject, String mailTo) {
		try {
			logger.debug("Enter sending mail");
			helper = new MimeMessageHelper(message = mailSender.createMimeMessage(), false);
			helper.setFrom(MAIL_FROM);
			helper.setTo(mailTo);
			helper.setSubject(Subject);
			helper.setText(strMessage + "", false);
			if (mailSender == null) {
				logger.error("cannot send mail as mail utility is not configured.");
			}
			mailSender.send(message);
			logger.debug("Exit sending mail");
		} catch (MessagingException e) {
			logger.error("Error while mailing  " + e);
		} catch (Exception e) {
			logger.error("Generic Error while mailing  " + e);
		}
	}

	protected void sendMail(String strMessage, String Subject, String[] mailTo) {
		try {
			logger.debug("Enter sending mail");
			helper = new MimeMessageHelper(message = mailSender.createMimeMessage(), true);
			helper.setFrom(MAIL_FROM);
			helper.setTo(mailTo);
			helper.setSubject(Subject);
			helper.setText(strMessage + "", false);
			if (mailSender == null) {
				logger.error("cannot send mail as mail utility is not configured.");
			}
			mailSender.send(message);
			logger.debug("mail sent successfully...");
		} catch (MessagingException e) {
			logger.error("Error while mailing  " + e);
		} catch (Exception e) {
			logger.error("Generic Error while mailing  " + e);
		}
	}

	public String getMAIL_FROM() {
		return MAIL_FROM;
	}

	public List<String> getMAIL_TOS() {
		return MAIL_TOS;
	}

	public String getSUBJECT_FAILURE() {
		return SUBJECT_FAILURE;
	}

	public String getSUBJECT_SUCCESS() {
		return SUBJECT_SUCCESS;
	}
	
	public void setMAIL_FROM(String mAIL_FROM) {
		MAIL_FROM = mAIL_FROM;
	}

	public void setMAIL_TOS(List<String> mAIL_TOS) {
		MAIL_TOS = mAIL_TOS;
	}

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}

	public void setSUBJECT_FAILURE(String sUBJECT_FAILURE) {
		SUBJECT_FAILURE = sUBJECT_FAILURE;
	}

	public void setSUBJECT_SUCCESS(String sUBJECT_SUCCESS) {
		SUBJECT_SUCCESS = sUBJECT_SUCCESS;
	}

	public String[] getMAIL_TO() {
		return MAIL_TO;
	}

	public void setMAIL_TO(String[] mAIL_TO) {
		MAIL_TO = mAIL_TO;
	}

	public String getMAIL_AOG_TO() {
		return MAIL_AOG_TO;
	}

	public void setMAIL_AOG_TO(String mAIL_AOG_TO) {
		MAIL_AOG_TO = mAIL_AOG_TO;
	}

	public String getSCHEMA_MAIL_TO() {
		return SCHEMA_MAIL_TO;
	}

	public void setSCHEMA_MAIL_TO(String sCHEMA_MAIL_TO) {
		SCHEMA_MAIL_TO = sCHEMA_MAIL_TO;
	}

	public JavaMailSenderImpl getSender() {
		return sender;
	}

	public void setSender(JavaMailSenderImpl sender) {
		this.sender = sender;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getMAIL_HOST() {
		return MAIL_HOST;
	}

	public void setMAIL_HOST(String mAIL_HOST) {
		MAIL_HOST = mAIL_HOST;
	}
}
