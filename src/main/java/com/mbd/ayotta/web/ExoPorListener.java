package com.mbd.ayotta.web;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.Session;

import oracle.jms.AQjmsSession;
import oracle.jms.AdtMessage;
import oracle.sql.ORAData;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.exolab.castor.xml.Unmarshaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.oxm.castor.CastorMarshaller;
import org.springframework.stereotype.Component;

import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.config.GenericProperties;
import com.mbd.ayotta.config.OracleAQConfiguration;
import com.mbd.ayotta.model.ExoTrig;
import com.mbd.ayotta.services.PorService;
import com.md.spice.model.XXMCLOB;

@Component
public class ExoPorListener extends AbstractAction implements MessageListener{
	
	Logger logger = Logger.getLogger(ExoPorListener.class);
	
	@Autowired
	private QueueConnectionFactory cf;
	protected QueueConnection conn;
	protected MessageConsumer consumer;
	private BasicDataSource datasource;
	protected Queue destinationQueue;
	public static long errorId = 0;
	private String queueName;
	private String hostName;
	private String origString;
	protected Session session;
	private AQjmsSession aqJmsSession;
	private boolean started = false, isConOpen = false;
	private String toDunsName;
	private NamedParameterJdbcTemplate jdbc;
	private HashMap<String,Integer> dbMap = new HashMap<String,Integer>();
	private CastorMarshaller unmarshaller;
	private CastorMarshaller marshaller;
	
	@Autowired GenericProperties genProps;
	@Autowired PorService porService;
	@Autowired OracleAQConfiguration oracleAQConfig;
	
	public void destroy() {
		logger.debug("destroy entered");
		try {
			logger.info("ROLLING BACK on mq");
			if (consumer != null)
				consumer.close();
			if (session != null)
				session.close();
			if (conn != null)
				conn.close();
			started = false;
		} catch (JMSException e) {
			logger.error("JMSException while destroy" + e);
		}
		consumer = null;
		session = null;
		conn = null;
		logger.debug("destroy exited");
	}
	
	@PostConstruct
	public void initialize() {
		logger.info("Enter in to Eta Trigger Listener");
		try {
			conn = cf.createQueueConnection();
			aqJmsSession = (AQjmsSession) conn.createQueueSession(false, AQjmsSession.AUTO_ACKNOWLEDGE);
			destinationQueue =  aqJmsSession.getQueue(oracleAQConfig.getAqUser(), oracleAQConfig.getAqName());
			QueueReceiver receiver = aqJmsSession.createReceiver(destinationQueue, XXMCLOB.getORADataFactory());
			receiver.setMessageListener(this);
			conn.start();
			logger.info("started listening on ");
		} catch (Exception e) {
			logger.error(e);
		}
	}
	
	public void onMessage(Message message) {
		logger.debug("Enter onMessage ");
		String exoPorResult = null;
		AdtMessage thisAdtMessage = (AdtMessage) message;
		boolean error = false;
		String msg = null;
		if (message instanceof AdtMessage) {
			try {
				ORAData mydata;
				mydata = (ORAData) thisAdtMessage.getAdtPayload();
				XXMCLOB xxmCLOB = (XXMCLOB) mydata;
				BufferedReader reader;
				reader = new BufferedReader(xxmCLOB.getXclob()
						.getCharacterStream());
				String line;
				StringBuffer clob = new StringBuffer();
				StringBuffer fullString = new StringBuffer();
				while (reader != null && (line = reader.readLine()) != null) {
					fullString.append(line);
				}
				logger.debug("FullString: "+fullString);
				msg = fullString.toString();
				msg = msg.replace("TrigFeed", "trig-feed");
				msg = msg.replace("TrigPKey", "trig-pKey");
				msg = msg.replace("TrigDate", "trig-date");
				ExoTrig trig = (ExoTrig) Unmarshaller.unmarshal(ExoTrig.class,new StringReader(msg));
				logger.debug("exo_por_trigger :"+ trig);
				

				HashMap<String,Object> params = new HashMap<String,Object>();
				params.put("FEED", trig.getTrigFeed());
				params.put("PKEY", trig.getTrigPKey());
				
				int retVal =0;
				logger.info("FEED="+trig.getTrigFeed()+ ", PKEY="+trig.getTrigPKey());
				if (!dbMap.containsKey(trig.getTrigFeed())){
					dbMap.put(trig.getTrigFeed(),1);
				} else {
					dbMap.put(trig.getTrigFeed(), dbMap.get(trig.getTrigFeed())+1);
				}
				
				if(trig.getTrigFeed().equals("EXO_POR")){
					try {
						porService.createPor(trig,toDunsName,genProps);
					} catch (Exception e) {
						//saveExoError(e,null);
						logger.error("ERROR POR MSG", e);
					}
				}
				
			}catch(Exception e){
				logger.debug("ExoPorListenerError :"+ e);
				e.printStackTrace();
			}
		logger.debug("onMessage exit");
		}
	}
	
	/*private String saveExoError(Exception e,String exoPorResult) {
		logger.error(e);
		String errorMsg = e.getMessage();
		Integer techId = 0;
		Integer failedId = 0;
		try {
			ExoTechErrors tecError = new ExoTechErrors();
			ExoFailedMessages failedMessages = new ExoFailedMessages();
			
			tecError.setErrorType("QUEUE_ERROR_EXOSTAR_V2");
			tecError.setError(e.getMessage());
			if((exoPorResult != null && !exoPorResult.equals("")) && !exoPorResult.equals("SUCCESS")){
				tecError.setError("Invalid Pipe, Error for "+ exoPorResult);
			}
			ExoTechErrors techErrorObj = genProps.getTechErrorsRepos().save(tecError);
			techId = (techErrorObj == null ? 0 : techErrorObj.getId());
			logger.debug("tech_error_id :"+ techId);
			
			if(techId>0){
				failedMessages.setTechErrorId(techId);
				failedMessages.setHostName(hostName);
				failedMessages.setQueueName(queueName);
				failedMessages.setActualMsg(origString);
				ExoFailedMessages faildMsg = genProps.getFailedMsgRepos().save(failedMessages);
				failedId = (faildMsg == null ? 0 : faildMsg.getId());
				if(failedId>0)
				logger.debug("Failed meassages saved ID: "+failedId);
			}else{
				logger.debug("Tech Error Not Saved.");
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return errorMsg;
	}*/
	
	public QueueConnectionFactory getCf() {
		return cf;
	}
	public void setCf(QueueConnectionFactory cf) {
		this.cf = cf;
	}
	public QueueConnection getConn() {
		return conn;
	}
	public void setConn(QueueConnection conn) {
		this.conn = conn;
	}
	public MessageConsumer getConsumer() {
		return consumer;
	}
	public void setConsumer(MessageConsumer consumer) {
		this.consumer = consumer;
	}
	public BasicDataSource getDatasource() {
		return datasource;
	}
	public void setDatasource(BasicDataSource datasource) {
		this.datasource = datasource;
	}
	public Queue getDestinationQueue() {
		return destinationQueue;
	}
	public void setDestinationQueue(Queue destinationQueue) {
		this.destinationQueue = destinationQueue;
	}
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getOrigString() {
		return origString;
	}
	public void setOrigString(String origString) {
		this.origString = origString;
	}
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	public boolean isStarted() {
		return started;
	}
	public void setStarted(boolean started) {
		this.started = started;
	}
	public boolean isConOpen() {
		return isConOpen;
	}
	public void setConOpen(boolean isConOpen) {
		this.isConOpen = isConOpen;
	}

	public String getToDunsName() {
		return toDunsName;
	}


	public void setToDunsName(String toDunsName) {
		this.toDunsName = toDunsName;
	}


	public NamedParameterJdbcTemplate getJdbc() {
		return jdbc;
	}


	public void setJdbc(NamedParameterJdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}


	public CastorMarshaller getUnmarshaller() {
		return unmarshaller;
	}


	public void setUnmarshaller(CastorMarshaller unmarshaller) {
		this.unmarshaller = unmarshaller;
	}


	public CastorMarshaller getMarshaller() {
		return marshaller;
	}


	public void setMarshaller(CastorMarshaller marshaller) {
		this.marshaller = marshaller;
	}


	public AQjmsSession getAqJmsSession() {
		return aqJmsSession;
	}


	public void setAqJmsSession(AQjmsSession aqJmsSession) {
		this.aqJmsSession = aqJmsSession;
	}

}
