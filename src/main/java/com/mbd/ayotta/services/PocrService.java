package com.mbd.ayotta.services;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.model.fa.ExoFa;
import com.mbd.ayotta.model.po.ExoPo;
import com.mbd.ayotta.model.po.ExoPoDetails;
import com.mbd.ayotta.model.po.PoMessages;
import com.mbd.ayotta.model.pocr.ExoPocr;
import com.mbd.ayotta.model.pocr.PocrDetail;
import com.mbd.ayotta.model.pocr.PocrHeader;
import com.mbd.ayotta.model.pocr.PocrMessages;
import com.mbd.ayotta.model.pocr.PocrRefDescHdr;
import com.mbd.ayotta.model.pocr.PocrRefInfoHdr;
import com.mbd.ayotta.model.pocr.PocrTrxCtrl;

@Service
public class PocrService extends AbstractAction {
	
	Logger logger = LoggerFactory.getLogger(FaService.class);
	private RestHighLevelClient client;
	private ObjectMapper objectMapper;
	private static Hashtable<String, Integer> tblTypes;
	static {
		tblTypes = new Hashtable<String, Integer>();
		tblTypes.put("CTL",new Integer(1));
		tblTypes.put("HDR",new Integer(2));
		tblTypes.put("RFH",new Integer(3));
		tblTypes.put("DSH",new Integer(4));
		tblTypes.put("DTL", new Integer(5));
	}
	
	@Autowired
	public PocrService(RestHighLevelClient client, ObjectMapper objectMapper) {
		this.client = client;
		this.objectMapper = objectMapper;
	}
	
	//MAPPING POCR DATA.	
	public PocrMessages mappingPocr(String msg,PocrMessages pocrMsg){
		logger.debug("Pocr msg : "+ msg);
		String line = null;
		String lineType= null;
		StringReader strReader = new StringReader(msg);
		BufferedReader br = new BufferedReader(strReader);
		PocrDetail pocrDetail = new PocrDetail();
		PocrTrxCtrl pocrTrxCtrl = new PocrTrxCtrl();
		ExoPocr exoPocr = new ExoPocr(); 
		try {
			while ((line= br.readLine()) != null) {
				line = line.trim();
				lineType = line.substring(0,3);
				int value = tblTypes.get(lineType);
				switch (value) {
				case 1:
					//CTL
					exoPocr.setCtl(ExoPocr.getCtl(line));
					pocrTrxCtrl = exoPocr.getCtl();
					break;
				case 2:
					//HDR
					exoPocr.setHdr(ExoPocr.getHdr(line, exoPocr.getCtl()));
					break;
				case 3:
					//RFH
					exoPocr.setRfh(ExoPocr.getRef(line, exoPocr.getHdr()));
					break;
				case 4:
					//DSH
					exoPocr.setDsh(ExoPocr.getDsh(line, exoPocr.getRfh()));
					break;
				case 5:
					//DTL
					pocrDetail = new PocrDetail();
					exoPocr.setDtl(ExoPocr.getDtl(line, exoPocr.getHdr()));
					exoPocr.getPocrDetailList().add(pocrDetail);
					break;
				default:				
					break;
				}
			}
			logger.debug("pocrTrxCtrl : "+ pocrTrxCtrl);
			if(pocrTrxCtrl != null){
				pocrMsg.setTrxControl(pocrTrxCtrl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pocrMsg;
	}
	
	/*ELK POCR START*/
	public String indexOrderPocrFile(PocrMessages pocr) throws Exception {
		logger.info(pocr.getFileName()+"");
		logger.info("client : "+ client);
		IndexRequest indexRequest = new IndexRequest(getFILE_INDEX(), getFILE_TYPE(), pocr.getFileName()).source(convertPocrFileToMap(pocr));
		logger.info("indexRequest : "+ indexRequest);
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		logger.info("indexed file "+ indexResponse.getIndex());
		return indexResponse.getResult().name();
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> convertPocrFileToMap(PocrMessages pocrFile) {
		return objectMapper.convertValue(pocrFile, Map.class);
	}
	
	public String createOrder(PocrMessages pocr) throws Exception {
		UUID uuid = UUID.randomUUID();
		pocr.setElasticSearchRef(uuid.toString());
		IndexRequest indexRequest = new IndexRequest(getORDER_INDEX(), getTYPE(), pocr.getElasticSearchRef()).source(convertPocrFileToMap(pocr));
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		return indexResponse.getResult().name();
	}
	
	@KafkaListener(topics = "inbound.exostar_pocr", groupId = "exo_pocr")
	public void listen(String message) {
	    System.out.println("Received Messasge in group foo: " + message);
	    logger.info("Received Messasge in group foo:");
	}
	/*ELK POCR END*/
}
