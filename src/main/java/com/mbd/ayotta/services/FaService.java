package com.mbd.ayotta.services;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.model.fa.ExoFa;
import com.mbd.ayotta.model.fa.ExoFaTrxControl;
import com.mbd.ayotta.model.fa.FaMessages;
import com.mbd.ayotta.model.poc.PocMessages;

@Service
public class FaService extends AbstractAction {
	Logger logger = LoggerFactory.getLogger(FaService.class);
	private RestHighLevelClient client;
	private ObjectMapper objectMapper;
	private static Hashtable<String, Integer> tblTypes;
	static {
		tblTypes = new Hashtable<String, Integer>();
		tblTypes.put("CTL", new Integer(1));
		tblTypes.put("HDR", new Integer(2));
	}

	@Autowired
	public FaService(RestHighLevelClient client, ObjectMapper objectMapper) {
		this.client = client;
		this.objectMapper = objectMapper;
	}
	
	/*MAPPING FA DATA.*/	
	public FaMessages mappingFa(String msg,FaMessages faMessages) {
		logger.debug("Fa msg : " + msg);
		String line = null;
		String lineType = null;
		StringReader strReader = new StringReader(msg);
		BufferedReader br = new BufferedReader(strReader);
		ExoFa exoFa = new ExoFa(); 
		ExoFaTrxControl faTrxControl = new ExoFaTrxControl();
		try {
			while ((line = br.readLine()) != null) {
				line = line.trim();
				lineType = line.substring(0, 3);
				int value = tblTypes.get(lineType);
				switch (value) {
				case 1:
					//CTL
					exoFa.setCtl(ExoFa.getCtl(line));
					faTrxControl= exoFa.getCtl();
					break;
				case 2:
					//HDR
					ExoFa.getHdr(line, exoFa.getCtl());
					break;
				default:
					break;
				}
			}
			logger.debug("faTrxControl : "+ faTrxControl);
			if(faTrxControl != null){
				faMessages.setTrxControl(faTrxControl);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return faMessages;
	}
	
	/*ELK FA START*/
	public String indexOrderFaFile(FaMessages faMsg) throws Exception {
		logger.info(faMsg.getFileName()+"");
		logger.info("client : "+ client);
		IndexRequest indexRequest = new IndexRequest(getFILE_INDEX(), getFILE_TYPE(), faMsg.getFileName()).source(convertFaFileToMap(faMsg));
		logger.info("indexRequest : "+ indexRequest);
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		logger.info("indexed file "+ indexResponse.getIndex());
		return indexResponse.getResult().name();
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> convertFaFileToMap(FaMessages faFile) {
		return objectMapper.convertValue(faFile, Map.class);
	}
	
	public String createOrder(FaMessages fa) throws Exception {
		UUID uuid = UUID.randomUUID();
		fa.setElasticSearchRef(uuid.toString());
		IndexRequest indexRequest = new IndexRequest(getORDER_INDEX(), getTYPE(), fa.getElasticSearchRef()).source(convertFaFileToMap(fa));
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		return indexResponse.getResult().name();
	}
	
	@KafkaListener(topics = "inbound.exostar_fa", groupId = "exo_fa")
	public void listen(String message) {
	    System.out.println("Received Messasge in group foo: " + message);
	    logger.info("Received Messasge in group foo:");
	}
	/*ELK FA END*/
}
