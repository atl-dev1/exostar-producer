package com.mbd.ayotta.services;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.model.po.ExoPayLoad;
import com.mbd.ayotta.model.po.ExoPo;
import com.mbd.ayotta.model.po.ExoPoDetails;
import com.mbd.ayotta.model.po.ExoPoTrxControl;
import com.mbd.ayotta.model.po.PoMessages;

@Service
public class PoService extends AbstractAction {
	Logger logger = LoggerFactory.getLogger(PoService.class);
	
	private RestHighLevelClient client;
	private ObjectMapper objectMapper;
	private String toDuns;
	private static Hashtable<String, Integer> tblTypes ;
	static {
		tblTypes = new Hashtable<String, Integer>();
		tblTypes.put("CTL",new Integer(1));
		tblTypes.put("HDR",new Integer(2));
		tblTypes.put("ATT",new Integer(3));
		tblTypes.put("RFH",new Integer(4));
		tblTypes.put("DSH",new Integer(5));
		tblTypes.put("NMH",new Integer(6));
		tblTypes.put("DTL",new Integer(7));
		tblTypes.put("API",new Integer(8));
		tblTypes.put("RFD",new Integer(9));
		tblTypes.put("DSD",new Integer(10));
		tblTypes.put("EVT",new Integer(11));
		tblTypes.put("NMD",new Integer(12));
		tblTypes.put("SCH",new Integer(13));
	}
	
	@Autowired
	public PoService( RestHighLevelClient client, ObjectMapper objectMapper) {
		this.client = client;
		this.objectMapper = objectMapper;
	}
	
	//MAPPING PO DATA.	
	public PoMessages mappingPo(String msg,PoMessages poMsg) throws Exception{
		logger.debug("Entered mappingPo...");
		int lineCount = 0;
		StringReader strReader= null;
		String line = null;
		String lineType= null;
		ExoPo exoPo = new ExoPo();
		ExoPoDetails exoPoDetails = new ExoPoDetails();
		ExoPoTrxControl exoPoTrxControl = new ExoPoTrxControl();
		strReader = new StringReader(msg);
		BufferedReader br = new BufferedReader(strReader);
		while ((line=br.readLine()) != null) {
			line = line.trim();
			lineType = line.substring(0,3);
			int value = tblTypes.get(lineType);
			switch (value) {
			case 1:
				//CTL
				exoPo.setCtl(ExoPo.getCtl(line));
				exoPoTrxControl = exoPo.getCtl();
				if(splitDataCheck(line,15)){
					break;
				}
			case 2:
				//HDR
				exoPo.setHdr(ExoPo.getHdr(line, exoPo.getCtl()));
				exoPo.getHdr().getPoType();
				if(splitDataCheck(line,46)){
					break;
				}
			case 3:
				//ATT
				exoPo.setAtt(ExoPo.getAtt(line, exoPo.getHdr()));
				if(splitDataCheck(line,4)){
					break;
				}
			case 4:
				//RFH
				exoPo.setRfh(ExoPo.getRfh(line, exoPo.getHdr()));
				if(splitDataCheck(line,5)){
					break;
				}
			case 5:
				//DSH
				exoPo.setDsh(ExoPo.getDsh(line, exoPo.getRfh()));
				if(splitDataCheck(line,3)){
					break;
				}
			case 6:
				//NMH
				exoPo.setNmh(ExoPo.getNmh(line, exoPo.getHdr()));
				if(splitDataCheck(line,25)){
					break;
				}
			case 7:
				//DTL
				exoPoDetails = new ExoPoDetails();
				exoPoDetails.setDtl(ExoPoDetails.getDtl(line, exoPo.getHdr()));
				exoPo.getExoPoDetailsList().add(exoPoDetails);
				lineCount++;
				if(splitDataCheck(line,55)){
					break;
				}
			case 8:
				//API
				exoPoDetails = exoPo.getExoPoDetailsList().get(lineCount-1);
				exoPoDetails.setApi(ExoPoDetails.getApi(line, exoPoDetails.getDtl()));
				if(splitDataCheck(line,3)){
					break;
				}
		    case 9:
				//RFD
		    	exoPoDetails = exoPo.getExoPoDetailsList().get(lineCount-1);
		    	exoPoDetails.setRfd(ExoPoDetails.getRfd(line,exoPoDetails.getDtl()));
		    	if(splitDataCheck(line,5)){
					break;
				}
			case 10:
				//DSD
				exoPoDetails = exoPo.getExoPoDetailsList().get(lineCount-1);
				exoPoDetails.setDsd(ExoPoDetails.getDsd(line,exoPoDetails.getDtl()));
				if(splitDataCheck(line,3)){
					break;
				}
			case 11:
				//EVT
				exoPoDetails = exoPo.getExoPoDetailsList().get(lineCount-1);
				exoPoDetails.setEvt(ExoPoDetails.getEvt(line,exoPoDetails.getDtl()));
				if(splitDataCheck(line,6)){
					break;
				}
			case 12:
				//NMD
				exoPoDetails = exoPo.getExoPoDetailsList().get(lineCount-1);
				exoPoDetails.setNmd(ExoPoDetails.getNmd(line,exoPoDetails.getDtl()));
				if(splitDataCheck(line,25)){
					break;
				}
			case 13:
				//SCH
				exoPoDetails = exoPo.getExoPoDetailsList().get(lineCount-1);
				exoPoDetails.setSch(ExoPoDetails.getSch(line,exoPoDetails.getDtl()));
				if(splitDataCheck(line,16)){
					break;
				}
			default:				
				break;
			}
		}
		
		logger.debug("exoPoTrxControl : "+ exoPoTrxControl);
		ExoPayLoad exoPayLoad = new ExoPayLoad();
		exoPayLoad.setCpo(exoPo.getHdr().getPoNo());
		exoPayLoad.setPoHdrId(exoPo.getHdr().getPoOrderHdrId());
		exoPayLoad.setOrderType(exoPoTrxControl.getFuncIdCode());
		exoPayLoad.setOrderData(msg);
		if(exoPoTrxControl != null){
			poMsg.setTrxControl(exoPoTrxControl);
			poMsg.setExoPayLoad(exoPayLoad);
		}
		return poMsg;
	}
	
	public Boolean splitDataCheck(String lineStr, int totalCount){
    	String[] line = lineStr.split("\\|",-1);
		if (line.length == totalCount) {
			return true;
		}
		logger.info("lineCount :" +line.length +", totalCount "+ totalCount);
		return false;
	}
	
	/*ELK START*/
	public String indexOrderPoFile(PoMessages po) throws Exception {
		logger.info(po.getFileName()+"");
		logger.info("client : "+ client);
		IndexRequest indexRequest = new IndexRequest(getFILE_INDEX(), getFILE_TYPE(), po.getFileName()).source(convertPoFileToMap(po));
		logger.info("indexRequest : "+ indexRequest);
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		logger.info("indexed file "+ indexResponse.getIndex());
		return indexResponse.getResult().name();
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> convertPoFileToMap(PoMessages poFile) {
		return objectMapper.convertValue(poFile, Map.class);
	}
	
	public String createOrder(PoMessages po) throws Exception {
		UUID uuid = UUID.randomUUID();
		po.setElasticSearchRef(uuid.toString());
		IndexRequest indexRequest = new IndexRequest(getORDER_INDEX(), getTYPE(), po.getElasticSearchRef()).source(convertPoFileToMap(po));
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		return indexResponse.getResult().name();
	}
	
	@KafkaListener(topics = "inbound.exostar_po", groupId = "exo_po")
	public void listen(String message) {
	    System.out.println("Received Messasge in group foo: " + message);
	    logger.info("Received Messasge in group foo:");
	}
	/*ELK END*/

	public String getToDuns() {
		return toDuns;
	}

	public void setToDuns(String toDuns) {
		this.toDuns = toDuns;
	}

	public static Hashtable<String, Integer> getTblTypes() {
		return tblTypes;
	}

	public static void setTblTypes(Hashtable<String, Integer> tblTypes) {
		PoService.tblTypes = tblTypes;
	}
}
