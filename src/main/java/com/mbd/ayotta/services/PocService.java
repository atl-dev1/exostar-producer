package com.mbd.ayotta.services;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.model.po.ExoPayLoad;
import com.mbd.ayotta.model.poc.ExoPoc;
import com.mbd.ayotta.model.poc.ExoPocDetails;
import com.mbd.ayotta.model.poc.ExoPocTrxControl;
import com.mbd.ayotta.model.poc.PocMessages;

@Service
public class PocService extends AbstractAction {
	Logger logger = LoggerFactory.getLogger(PoService.class);
	
	private RestHighLevelClient client;
	private ObjectMapper objectMapper;
	private static Hashtable<String, Integer> tblTypes ;
	static {
		tblTypes = new Hashtable<String, Integer>();
		tblTypes.put("CTL",new Integer(1));
		tblTypes.put("HDR",new Integer(2));
		tblTypes.put("ATT",new Integer(3));
		tblTypes.put("RFH",new Integer(4));
		tblTypes.put("DSH",new Integer(5));
		tblTypes.put("NMH",new Integer(6));
		tblTypes.put("DTL",new Integer(7));
		tblTypes.put("API",new Integer(8));
		tblTypes.put("RFD",new Integer(9));
		tblTypes.put("DSD",new Integer(10));
		tblTypes.put("EVT",new Integer(11));
		tblTypes.put("NMD",new Integer(12));
		tblTypes.put("SCH",new Integer(13));
	}
	
	@Autowired
	public PocService( RestHighLevelClient client, ObjectMapper objectMapper) {
		this.client = client;
		this.objectMapper = objectMapper;
	}
	
	//MAPPING POC DATA.	
	public PocMessages mappingPoc(String msg, PocMessages pocMsg) throws Exception{
		logger.debug("Entered mappingPoc...");
		int lineCount = 0;
		StringReader strReader= null;
		String line = null;
		String lineType= null;
		ExoPoc exoPoc = new ExoPoc();
		ExoPocDetails exoPocDetails = new ExoPocDetails();
		ExoPocTrxControl exoPocTrxControl = new ExoPocTrxControl();
		strReader = new StringReader(msg);
		BufferedReader br = new BufferedReader(strReader);
		while ((line=br.readLine()) != null) {
			line = line.trim();
			lineType = line.substring(0,3);
			int value = tblTypes.get(lineType);
			switch (value) {
			case 1:
				//CTL
				exoPoc.setCtl(ExoPoc.getCtl(line));
				exoPocTrxControl = exoPoc.getCtl();
				if(splitDataCheck(line,15)){
					break;
				}
			case 2:
				//HDR
				exoPoc.setHdr(ExoPoc.getHdr(line, exoPoc.getCtl()));
				if(splitDataCheck(line,52)){
					break;
				}
			case 3:
				//ATT
				exoPoc.setAtt(ExoPoc.getAtt(line, exoPoc.getHdr()));
				if(splitDataCheck(line,4)){
					break;
				}
			case 4:
				//RFH
				exoPoc.setRfh(ExoPoc.getRfh(line, exoPoc.getHdr()));
				if(splitDataCheck(line,5)){
					break;
				}
			case 5:
				//DSH
				exoPoc.setDsh(ExoPoc.getDsh(line, exoPoc.getRfh()));
				if(splitDataCheck(line,3)){
					break;
				}
			case 6:
				//NMH
				exoPoc.setNmh(ExoPoc.getNmh(line, exoPoc.getHdr()));
				if(splitDataCheck(line,25)){
					break;
				}
			case 7:
				//DTL
				exoPocDetails = new ExoPocDetails();
				exoPocDetails.setDtl(ExoPocDetails.getDtl(line, exoPoc.getHdr()));
				exoPoc.getExoPocDetailsList().add(exoPocDetails);
				lineCount++;
				if(splitDataCheck(line,59)){
					break;
				}
			case 8:
				//API
				exoPocDetails = exoPoc.getExoPocDetailsList().get(lineCount-1);
				exoPocDetails.setApi(ExoPocDetails.getApi(line, exoPocDetails.getDtl()));
				if(splitDataCheck(line,3)){
					break;
				}
		    case 9:
				//RFD
		    	exoPocDetails = exoPoc.getExoPocDetailsList().get(lineCount-1);
		    	exoPocDetails.setRfd(ExoPocDetails.getRfd(line,exoPocDetails.getDtl()));
		    	if(splitDataCheck(line,5)){
					break;
				}
			case 10:
				//DSD
				exoPocDetails = exoPoc.getExoPocDetailsList().get(lineCount-1);
				exoPocDetails.setDsd(ExoPocDetails.getDsd(line,exoPocDetails.getDtl()));
				if(splitDataCheck(line,3)){
					break;
				}
			case 11:
				//EVT
				exoPocDetails = exoPoc.getExoPocDetailsList().get(lineCount-1);
				exoPocDetails.setEvt(ExoPocDetails.getEvt(line,exoPocDetails.getDtl()));
				if(splitDataCheck(line,6)){
					break;
				}
			case 12:
				//NMD
				exoPocDetails = exoPoc.getExoPocDetailsList().get(lineCount-1);
				exoPocDetails.setNmd(ExoPocDetails.getNmd(line,exoPocDetails.getDtl()));
				if(splitDataCheck(line,25)){
					break;
				}
			case 13:
				//SCH
				exoPocDetails = exoPoc.getExoPocDetailsList().get(lineCount-1);
				exoPocDetails.setSch(ExoPocDetails.getSch(line,exoPocDetails.getDtl()));
				if(splitDataCheck(line,16)){
					break;
				}
			default:				
				break;
			}
		}
		logger.debug("exoPocTrxControl : "+ exoPocTrxControl);
		ExoPayLoad exoPayLoad = new ExoPayLoad();
		exoPayLoad.setPoHdrId(exoPoc.getHdr().getPocOrderHdrId());
		exoPayLoad.setCpo(exoPoc.getHdr().getPoNo());
		exoPayLoad.setOrderType(exoPocTrxControl.getFuncIdCode());
		exoPayLoad.setOrderData(msg);
		logger.debug("POC ExoPayLoad." + exoPayLoad);
		
		if(exoPocTrxControl != null){
			pocMsg.setTrxControl(exoPocTrxControl);
			pocMsg.setExoPayLoad(exoPayLoad);
		}
		return pocMsg;
	}
	
	public Boolean splitDataCheck(String lineStr, int totalCount){
    	String[] line = lineStr.split("\\|",-1);
		if (line.length == totalCount) {
			return true;
		}
		logger.info("lineCount :" +line.length +", totalCount "+ totalCount);
		return false;
	}

	/*ELK POC START*/
	public String indexOrderPocFile(PocMessages poc) throws Exception {
		logger.info(poc.getFileName()+"");
		logger.info("client : "+ client);
		IndexRequest indexRequest = new IndexRequest(getFILE_INDEX(), getFILE_TYPE(), poc.getFileName()).source(convertPocFileToMap(poc));
		logger.info("indexRequest : "+ indexRequest);
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		logger.info("indexed file "+ indexResponse.getIndex());
		return indexResponse.getResult().name();
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> convertPocFileToMap(PocMessages pocFile) {
		return objectMapper.convertValue(pocFile, Map.class);
	}
	
	public String createOrder(PocMessages poc) throws Exception {
		UUID uuid = UUID.randomUUID();
		poc.setElasticSearchRef(uuid.toString());
		IndexRequest indexRequest = new IndexRequest(getORDER_INDEX(), getTYPE(), poc.getElasticSearchRef()).source(convertPocFileToMap(poc));
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		return indexResponse.getResult().name();
	}
	
	@KafkaListener(topics = "inbound.exostar_poc", groupId = "exo_poc")
	public void listen(String message) {
	    System.out.println("Received Messasge in group foo: " + message);
	    logger.info("Received Messasge in group foo:");
	}
	/*ELK POC END*/
}
