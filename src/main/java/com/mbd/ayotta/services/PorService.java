package com.mbd.ayotta.services;

import java.io.File;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mbd.ayotta.AbstractAction;
import com.mbd.ayotta.config.GenericProperties;
import com.mbd.ayotta.model.ExoMPIDS;
import com.mbd.ayotta.model.ExoTrig;
import com.mbd.ayotta.model.por.ExoPorDetail;
import com.mbd.ayotta.model.por.ExoPorHeader;
import com.mbd.ayotta.model.por.ExoPorRefDescHdr;
import com.mbd.ayotta.model.por.ExoPorTrxControl;
import com.mbd.ayotta.model.por.ExoRefInfoHdr;
import com.mbd.ayotta.model.por.PorMessages;

@Service
public class PorService extends AbstractAction{

	Logger logger = Logger.getLogger(PorService.class);
	private String toDuns;
	private String porFFPath;
	private GenericProperties genProperty;
	private RestHighLevelClient client;
	private ObjectMapper objectMapper;
	private PorMessages porMsssageObj;
	
	@Autowired
	public PorService( RestHighLevelClient client, ObjectMapper objectMapper) {
		this.client = client;
		this.objectMapper = objectMapper;
	}
	
	public String createPor(ExoTrig trig,String toDunsName, GenericProperties props){
		try {
			porMsssageObj = new PorMessages();
			setGenProperty(props);
			setToDuns(genProperty.getToDunsName());
			setPorFFPath(genProperty.getPorFfPath());
			StringBuffer fileContent = new StringBuffer();
			ExoPorTrxControl porTrx = new ExoPorTrxControl();
			String fnIdCode = "POR";
			if(trig.getTrigPKey() != null && !trig.getTrigPKey().equals("")){
				Optional<ExoPorTrxControl> porTrxList = genProperty.getPorTrxControlRepos().findById(Integer.valueOf(trig.getTrigPKey())); 
				porTrx = porTrxList.get();
				if (porTrx == null ){
					return null;
				}
				fnIdCode = porTrx.getFuncIdCode();
				logger.debug("porTrx : "+ porTrx);
				fileContent.append(porTrx.toStringFlatFile() + "\r\n");
				String supplierMpid = "";
				
				Iterator<ExoPorHeader> porHdr = null;
				porMessageMap(trig,"READ_FROM_DB_QUEUE",null,""); /*SAVED-POR ELK*/
				if(porTrx != null){
					List<ExoPorHeader> list = genProperty.getPorHeaderRepos().findByPorTrxCtrlId(porTrx.getPoTrxCtrlId());
					porHdr = list.iterator();
					logger.debug("List : "+ list);
				}
				while (porHdr != null && porHdr.hasNext()) {
					ExoPorHeader hdr = (ExoPorHeader) porHdr.next();
					supplierMpid = hdr.getSupplierMPID();
					String changeOrderNo = getChangeOrdSeq(hdr.getPoNumber());
					if(changeOrderNo !=null && !changeOrderNo.equals("")){
						hdr.setChangeOrderSeqNo(changeOrderNo);
					}
					fileContent.append(hdr.toStringFlateFile() + "\r\n");
					
					List<ExoPorDetail> lineList = genProperty.getPorDetailRepos().findByPoHdrId(hdr.getPoHdrId());
					Iterator<ExoPorDetail> porDtl = lineList.iterator();
					while (porDtl != null && porDtl.hasNext()) {
						ExoPorDetail dtl = (ExoPorDetail) porDtl.next();
						fileContent.append(dtl.toStringFlatFile() + "\r\n");
					}
					
					List<ExoRefInfoHdr> infoHdrList = genProperty.getRefInfoHdrRepos().findByPorHdrId(hdr.getPoHdrId());
					Iterator<ExoRefInfoHdr> porRef = infoHdrList.iterator();
					while (porRef != null && porRef.hasNext()) {
						ExoRefInfoHdr ref = (ExoRefInfoHdr) porRef.next();
						fileContent.append(ref.toStringFlatFile() + "\r\n");
						
						List<ExoPorRefDescHdr> refDescHdrList = genProperty.getPorRefDescHdrRepos().findByPorHdrId(hdr.getPoHdrId());
						Iterator<ExoPorRefDescHdr> porRefDesc = refDescHdrList.iterator();
						while (porRefDesc != null && porRefDesc.hasNext()) {
							ExoPorRefDescHdr desc = (ExoPorRefDescHdr) porRefDesc.next();
							fileContent.append(desc.toStringFlatFile() + "\r\n");
						}
					}
				}
				logger.debug(fileContent);
				porFlatFile(fileContent,supplierMpid,fnIdCode);
			}else{
				logger.debug("por trx id is : "+ trig.getTrigPKey());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "SUCCESS";
	}
	
	public void porFlatFile(StringBuffer fileContent,String supplierMpid, String fnIdCode) throws Exception{
		logger.info("fileContent : "+ fileContent);
		if(getPorFFPath() != null && getPorFFPath() != ""){
			
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String date = df.format(new Date());
			String fileName = genProperty.getSupplierMpid()+"_"+getToDuns()+"_"+fnIdCode+"_BSCP"+fnIdCode+"FF1_"+date+".txt";
			String ffPath = getPorFFPath()+fileName;
			File file = new File(ffPath);
			
			FileUtils.writeStringToFile(file, fileContent.toString());
			logger.info(fnIdCode+" generated and sent to queue "+ fileName);
			logger.info(fnIdCode+" Flatfile Path : "+ ffPath);
			sendPorMessage(fileName,fileContent.toString());
			porMessageMap(null,"PROCESSED",fileContent.toString(),fileName); /*UPDATED(PROCESSED-POR) ELK*/
		}
	}
	
	/*SEND MQ FOR POR*/
	private boolean sendPorMessage(final String fileName, final String exoPor) {
		logger.info("sendPorMsg start");
		JmsTemplate mqTemplate =  new JmsTemplate(genProperty.getRemoteJms());
		logger.debug("sendPorMsg host : "+ genProperty.getMqProps().getMqHost());
		mqTemplate.send(genProperty.getMqProps().getExoPorOutQ(), new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				MapMessage txtMessage = session.createMapMessage();
				txtMessage.setString("filename",fileName);
				txtMessage.setString("content",exoPor);
				return txtMessage;
			}
		});
		return true;
	}
	
	public String getChangeOrdSeq(String poNumber){
		if(poNumber != null && !poNumber.equals("")){
			String sql = "SELECT MAX(CHANGE_ORDER_SEQUENCE) FROM EXO_POC_HEADER WHERE PO_NUMBER ="+poNumber;
			List ordSeqList = genProperty.getEm().createQuery(sql).getResultList();
			if(ordSeqList != null) {
				return ordSeqList.get(0)+"";
			}
		}
		return null;
	}
	
	public void porMessageMap(ExoTrig exoTrig,String messageStatus,String flatFileContent, String fileName) throws Exception{
		if(exoTrig != null && !exoTrig.equals("")){
			PorMessages por = new PorMessages();
			por.setMessageStatus(messageStatus);
			por.setOrderType("POR");
			por.setPorFileContent(flatFileContent);
			por.setFileName(fileName);
			por.setElkCreationDate(new Timestamp(System.currentTimeMillis()));
			por.setExoTrig(exoTrig);
			createOrder(por);
		}else{
			porMsssageObj.setElkModifiedDate(new Timestamp(System.currentTimeMillis()));
			porMsssageObj.setPorFileContent(flatFileContent);
			porMsssageObj.setFileName(fileName);
			porMsssageObj.setMessageStatus(messageStatus);
			updateOrder(porMsssageObj);
		}
	}
	
	/*START ELK CODE*/
	public String indexOrderPor(PorMessages por) throws Exception {
		logger.info(por.getFileName()+"");
		logger.info("client : "+ client);
		IndexRequest indexRequest = new IndexRequest(getFILE_INDEX(), getFILE_TYPE(), por.getExoTrig().getTrigPKey()).source(convertOrderToMap(por));
		logger.info("indexRequest : "+ indexRequest);
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		logger.info("indexed file "+ indexResponse.getIndex());
		return indexResponse.getResult().name();
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, Object> convertOrderToMap(PorMessages porMsg) {
		return objectMapper.convertValue(porMsg, Map.class);
	}
	
	public String createOrder(PorMessages order) throws Exception {
		UUID uuid = UUID.randomUUID();
		order.setElasticSearchRef(uuid.toString());
		porMsssageObj = order;
		IndexRequest indexRequest = new IndexRequest(getORDER_INDEX(), getTYPE(), order.getElasticSearchRef()).source(convertOrderToMap(order));
		IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
		return indexResponse.getResult().name();
	}
	
	public void updateElk(PorMessages porMsg, String status) throws Exception{
		porMsg.setMessageStatus(status);
		porMsg.setElkModifiedDate(new Timestamp(System.currentTimeMillis()));
		updateOrder(porMsg);
	}
	
	public String updateOrder(PorMessages msg) throws Exception {
		logger.info("Enter elasticsearch update..."+ msg.getElasticSearchRef());
		PorMessages resultDocument = findById(msg.getElasticSearchRef());
		UpdateRequest updateRequest = new UpdateRequest(getORDER_INDEX(), getTYPE(), resultDocument.getElasticSearchRef());
		updateRequest.doc(convertOrderToMap(msg));
		UpdateResponse updateResponse = client.update(updateRequest, RequestOptions.DEFAULT);
		logger.info("Exit elasticsearch update..."+ msg.getElasticSearchRef());
		return updateResponse.getResult().name();
	}
	
	public PorMessages findById(String id) throws Exception {
		GetRequest getRequest = new GetRequest(getORDER_INDEX(), getTYPE(), id);
		GetResponse getResponse = client.get(getRequest, RequestOptions.DEFAULT);
		Map<String, Object> resultMap = getResponse.getSource();
		return convertMapToOrder(resultMap);
	}
	
	public PorMessages convertMapToOrder(Map<String, Object> map) {
		return objectMapper.convertValue(map, PorMessages.class);
	}
	/*END ELK CODE*/

	public String getToDuns() {
		return toDuns;
	}

	public void setToDuns(String toDuns) {
		this.toDuns = toDuns;
	}

	public String getPorFFPath() {
		return porFFPath;
	}

	public void setPorFFPath(String porFFPath) {
		this.porFFPath = porFFPath;
	}

	public GenericProperties getGenProperty() {
		return genProperty;
	}

	public void setGenProperty(GenericProperties genProperty) {
		this.genProperty = genProperty;
	}

	public ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public PorMessages getPorMsssageObj() {
		return porMsssageObj;
	}

	public void setPorMsssageObj(PorMessages porMsssageObj) {
		this.porMsssageObj = porMsssageObj;
	}

}
