package com.mbd.ayotta.config;

import java.sql.SQLException;

import javax.jms.JMSException;
import javax.jms.QueueConnectionFactory;
import javax.sql.DataSource;
import javax.validation.constraints.NotNull;

import oracle.jdbc.pool.OracleDataSource;
import oracle.jms.AQjmsFactory;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@ConfigurationProperties("spring.oracle")
public class OracleAQConfiguration {

	    @NotNull
	    private String url;

	    @NotNull
	    private String aqName;

	    @NotNull
	    private String aqUser;

	    @NotNull
	    private String aqPassword;

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

		public String getAqName() {
			return aqName;
		}

		public void setAqName(String aqName) {
			this.aqName = aqName;
		}

		public String getAqUser() {
			return aqUser;
		}

		public void setAqUser(String aqUser) {
			this.aqUser = aqUser;
		}
		
		public String getAqPassword() {
			return aqPassword;
		}

		public void setAqPassword(String aqPassword) {
			this.aqPassword = aqPassword;
		}
		
		 @Bean
		    /**
		     * Spring bean with the configuration details of where the Oracle database is containing the QUEUES
		     */
		 @Primary
		    public DataSource dataSource() throws SQLException {
		        OracleDataSource ds = new OracleDataSource();
		        ds.setUser(aqUser);
		        ds.setPassword(aqPassword);
		        ds.setURL(url);
		        ds.setImplicitCachingEnabled(true);
		        ds.setFastConnectionFailoverEnabled(true);
		        return ds;
		    }

		    @Bean
		    /**
		     * The KEY component effectively connecting to the Oracle AQ system using the datasource input
		     */
		    public QueueConnectionFactory connectionFactory(DataSource dataSource) throws JMSException {
		        return AQjmsFactory.getQueueConnectionFactory(dataSource);
		    }

}
