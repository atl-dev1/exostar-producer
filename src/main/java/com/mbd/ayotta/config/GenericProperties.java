package com.mbd.ayotta.config;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.mbd.ayotta.repositories.por.PorDetailRepos;
import com.mbd.ayotta.repositories.por.PorHeaderRepos;
import com.mbd.ayotta.repositories.por.PorRefDescHdrRepos;
import com.mbd.ayotta.repositories.por.PorTrxControlRepos;
import com.mbd.ayotta.repositories.por.RefInfoHdrRepos;
import com.sun.messaging.ConnectionFactory;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class GenericProperties {
	
	private String inputFilePath;
	private String processedFilePath;
	private String mailfrom;
	private String readFilePath;
	private String toDunsName;
	private String porFfPath;
	private String exostarMpid;
	private String buyerMpid;
	private String supplierMpid;
	
	@Autowired PorTrxControlRepos porTrxControlRepos;
	@Autowired PorHeaderRepos porHeaderRepos;
	@Autowired PorRefDescHdrRepos porRefDescHdrRepos;
	@Autowired RefInfoHdrRepos refInfoHdrRepos;
	@Autowired PorDetailRepos porDetailRepos;
	@Autowired EntityManager em;
	@Autowired MqProperties mqProps;
	@Autowired ConnectionFactory remoteJms;
	
	public String getMailfrom() {
		return mailfrom;
	}

	public void setMailfrom(String mailfrom) {
		this.mailfrom = mailfrom;
	}

	public String getInputFilePath() {
		return inputFilePath;
	}

	public void setInputFilePath(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}

	public String getProcessedFilePath() {
		return processedFilePath;
	}

	public void setProcessedFilePath(String processedFilePath) {
		this.processedFilePath = processedFilePath;
	}

	public String getReadFilePath() {
		return readFilePath;
	}

	public void setReadFilePath(String readFilePath) {
		this.readFilePath = readFilePath;
	}

	public String getToDunsName() {
		return toDunsName;
	}

	public void setToDunsName(String toDunsName) {
		this.toDunsName = toDunsName;
	}

	public PorTrxControlRepos getPorTrxControlRepos() {
		return porTrxControlRepos;
	}

	public void setPorTrxControlRepos(PorTrxControlRepos porTrxControlRepos) {
		this.porTrxControlRepos = porTrxControlRepos;
	}

	public PorHeaderRepos getPorHeaderRepos() {
		return porHeaderRepos;
	}

	public void setPorHeaderRepos(PorHeaderRepos porHeaderRepos) {
		this.porHeaderRepos = porHeaderRepos;
	}

	public PorRefDescHdrRepos getPorRefDescHdrRepos() {
		return porRefDescHdrRepos;
	}

	public void setPorRefDescHdrRepos(PorRefDescHdrRepos porRefDescHdrRepos) {
		this.porRefDescHdrRepos = porRefDescHdrRepos;
	}

	public RefInfoHdrRepos getRefInfoHdrRepos() {
		return refInfoHdrRepos;
	}

	public void setRefInfoHdrRepos(RefInfoHdrRepos refInfoHdrRepos) {
		this.refInfoHdrRepos = refInfoHdrRepos;
	}

	public PorDetailRepos getPorDetailRepos() {
		return porDetailRepos;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public void setPorDetailRepos(PorDetailRepos porDetailRepos) {
		this.porDetailRepos = porDetailRepos;
	}

	public MqProperties getMqProps() {
		return mqProps;
	}

	public void setMqProps(MqProperties mqProps) {
		this.mqProps = mqProps;
	}

	public ConnectionFactory getRemoteJms() {
		return remoteJms;
	}

	public void setRemoteJms(ConnectionFactory remoteJms) {
		this.remoteJms = remoteJms;
	}

	public String getPorFfPath() {
		return porFfPath;
	}

	public void setPorFfPath(String porFfPath) {
		this.porFfPath = porFfPath;
	}

	public String getExostarMpid() {
		return exostarMpid;
	}

	public void setExostarMpid(String exostarMpid) {
		this.exostarMpid = exostarMpid;
	}

	public String getBuyerMpid() {
		return buyerMpid;
	}

	public void setBuyerMpid(String buyerMpid) {
		this.buyerMpid = buyerMpid;
	}

	public String getSupplierMpid() {
		return supplierMpid;
	}

	public void setSupplierMpid(String supplierMpid) {
		this.supplierMpid = supplierMpid;
	}
}
