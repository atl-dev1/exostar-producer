package com.mbd.ayotta.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("spring.mq")
public class MqProperties {
	
	private String faInQ;
	private String exoPorOutQ;
	private String mqHost;
	
	public String getFaInQ() {
		return faInQ;
	}
	public void setFaInQ(String faInQ) {
		this.faInQ = faInQ;
	}
	public String getExoPorOutQ() {
		return exoPorOutQ;
	}
	public void setExoPorOutQ(String exoPorOutQ) {
		this.exoPorOutQ = exoPorOutQ;
	}
	public String getMqHost() {
		return mqHost;
	}
	public void setMqHost(String mqHost) {
		this.mqHost = mqHost;
	}
}
